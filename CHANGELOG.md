# Change Log

All notable changes to this project will be documented in this file.
See [Conventional Commits](https://conventionalcommits.org) for commit guidelines.

## [10.21.2](https://gitlab.com/emerald-works/corvus/compare/v10.21.1...v10.21.2) (2023-12-12)

**Note:** Version bump only for package @emerald-works/serverless-boilerplate





## [10.21.1](https://gitlab.com/emerald-works/corvus/compare/v10.21.0...v10.21.1) (2023-12-11)

**Note:** Version bump only for package @emerald-works/serverless-boilerplate





# [10.21.0](https://gitlab.com/emerald-works/corvus/compare/v10.20.1...v10.21.0) (2023-12-11)


### Bug Fixes

* adjust CLI commands ([a749519](https://gitlab.com/emerald-works/corvus/commit/a7495190753281d17f0f3b5b826888f750d07eae))
* lambda multi region streams ([6465efe](https://gitlab.com/emerald-works/corvus/commit/6465efe4a83b4e52fd900607f6cda538d4b1baf9))
* package-lock ([819a78d](https://gitlab.com/emerald-works/corvus/commit/819a78d3a0c1ca5764c5953c64fb6da9469f5be4))


### Features

* **PENG-475:** lambda filter for dynamo expressions ([3d0f77a](https://gitlab.com/emerald-works/corvus/commit/3d0f77aeadbdc472b3ff7f5f355a2b9e018ad90d))
* **PENG-475:** multi-region (WIP) ([e20c9fd](https://gitlab.com/emerald-works/corvus/commit/e20c9fdbd8838843c6efac9311fb387829f87968))





## [10.20.1](https://gitlab.com/emerald-works/corvus/compare/v10.20.0...v10.20.1) (2023-11-22)

**Note:** Version bump only for package @emerald-works/serverless-boilerplate





# [10.20.0](https://gitlab.com/emerald-works/corvus/compare/v10.19.2...v10.20.0) (2023-11-02)

**Note:** Version bump only for package @emerald-works/serverless-boilerplate





## [10.19.2](https://gitlab.com/emerald-works/corvus/compare/v10.19.1...v10.19.2) (2023-10-13)

**Note:** Version bump only for package @emerald-works/serverless-boilerplate





## [10.19.1](https://gitlab.com/emerald-works/corvus/compare/v10.19.0...v10.19.1) (2023-09-25)

**Note:** Version bump only for package @emerald-works/serverless-boilerplate





# [10.19.0](https://gitlab.com/emerald-works/corvus/compare/v10.18.0...v10.19.0) (2023-09-24)


### Bug Fixes

* add cdk dependency ([f813a51](https://gitlab.com/emerald-works/corvus/commit/f813a511a127739dd41777e28ca96c792d5b5890))
* add custom serverless package ([bf3925f](https://gitlab.com/emerald-works/corvus/commit/bf3925f5283eb59e648a5104a11fa77954dbd078))
* cdk ([c470f9e](https://gitlab.com/emerald-works/corvus/commit/c470f9edf92e8d7914f5a57e3f84cd52ce7f0786))
* package-lock ([828b4f1](https://gitlab.com/emerald-works/corvus/commit/828b4f1969cf42144a0f0a200de57326e4d8c51e))
* package-lock ([07e9c5c](https://gitlab.com/emerald-works/corvus/commit/07e9c5cf8ce391e6f6edc41a3830625acc5b6b54))
* package-lock ([e645dcf](https://gitlab.com/emerald-works/corvus/commit/e645dcf1078b5452117a04db0dab3203cf1e780b))
* package-lock ([bf20879](https://gitlab.com/emerald-works/corvus/commit/bf20879220c47c713bad097de2d117be1c19062c))
* package-lock ([5ce66df](https://gitlab.com/emerald-works/corvus/commit/5ce66df4796259d7417aca0618c266a2a2615f14))
* package-lock ([dcf77c9](https://gitlab.com/emerald-works/corvus/commit/dcf77c926b420256762483652d2366c0e19fa53b))
* package-lock ([a6808b1](https://gitlab.com/emerald-works/corvus/commit/a6808b10da766fa179abcc4ac327b5eaaf2df9ef))
* package-lock ([2f8d2da](https://gitlab.com/emerald-works/corvus/commit/2f8d2dac915c1f4d4ba90bf16343816b49d062a1))
* package-lock ([ff13683](https://gitlab.com/emerald-works/corvus/commit/ff136835aa2a3eca36ef4da065074b47b4efe52a))
* package-lock ([583ee75](https://gitlab.com/emerald-works/corvus/commit/583ee753487bc1f03d439567f9e999edb10f98a4))
* remove mongoose ([c0c117b](https://gitlab.com/emerald-works/corvus/commit/c0c117b8006cdd90b392b0c09e86a56466d923c5))
* removing mongoose ([1cc6808](https://gitlab.com/emerald-works/corvus/commit/1cc6808ab98f610be0473e98e6d0c98411222a4e))
* removing serverless ([c2d7fc3](https://gitlab.com/emerald-works/corvus/commit/c2d7fc376e9ba546a97d0182619bf1d022f649ca))
* using mt serverless ([c8913c4](https://gitlab.com/emerald-works/corvus/commit/c8913c4d4125e814d2c746bfef52858fbdc22c7b))
* using newer corvus version ([ef56708](https://gitlab.com/emerald-works/corvus/commit/ef5670826166b632f242984f1a73e18df7c53d22))
* using newer corvus version ([f5d4d7e](https://gitlab.com/emerald-works/corvus/commit/f5d4d7e3023d8963005b49409eec6004734f154f))
* using newer corvus version ([8412ebc](https://gitlab.com/emerald-works/corvus/commit/8412ebcec8d85d7756d9d6f00856b02de6083474))


### Reverts

* updating template serverless version ([a7242d7](https://gitlab.com/emerald-works/corvus/commit/a7242d7b9dccf6edbdebc39754f6f9af136aaad6))



## [10.9.8](https://gitlab.com/emerald-works/corvus/compare/v10.9.7...v10.9.8) (2022-12-10)



## [10.9.7](https://gitlab.com/emerald-works/corvus/compare/v10.9.6...v10.9.7) (2022-12-09)



## [10.9.6](https://gitlab.com/emerald-works/corvus/compare/v10.9.5...v10.9.6) (2022-12-09)



## [10.9.5](https://gitlab.com/emerald-works/corvus/compare/v10.9.4...v10.9.5) (2022-11-30)



## [10.9.4](https://gitlab.com/emerald-works/corvus/compare/v10.9.3...v10.9.4) (2022-11-22)



## [10.9.3](https://gitlab.com/emerald-works/corvus/compare/v10.9.2...v10.9.3) (2022-11-03)



## [10.9.2](https://gitlab.com/emerald-works/corvus/compare/v10.9.1...v10.9.2) (2022-11-02)


### Bug Fixes

* using on demand ([1d177cb](https://gitlab.com/emerald-works/corvus/commit/1d177cbaf11450c32fdfdb380d71f3877009a548))



## [10.9.1](https://gitlab.com/emerald-works/corvus/compare/v10.9.0...v10.9.1) (2022-10-25)



# [10.9.0](https://gitlab.com/emerald-works/corvus/compare/v10.8.0...v10.9.0) (2022-10-24)


### Features

* use http for core events ([c0b44f1](https://gitlab.com/emerald-works/corvus/commit/c0b44f1d75ff7d27a8491c5bad7ade642621f146))



# [10.8.0](https://gitlab.com/emerald-works/corvus/compare/v10.7.0...v10.8.0) (2022-10-19)



# [10.7.0](https://gitlab.com/emerald-works/corvus/compare/v10.6.1...v10.7.0) (2022-10-10)


### Features

* adding cloudformation outputs ([cfae735](https://gitlab.com/emerald-works/corvus/commit/cfae73554c3629c21795abfc74c193a5c0434d17))



## [10.6.1](https://gitlab.com/emerald-works/corvus/compare/v10.6.0...v10.6.1) (2022-10-07)



# [10.6.0](https://gitlab.com/emerald-works/corvus/compare/v10.5.0...v10.6.0) (2022-10-06)



# [10.5.0](https://gitlab.com/emerald-works/corvus/compare/v10.4.1...v10.5.0) (2022-10-05)


### Features

* include, exclude and per function env vars ([0cde4d2](https://gitlab.com/emerald-works/corvus/commit/0cde4d26c9022a77e369e528b67c9bde8cdd3c32))



## [10.4.1](https://gitlab.com/emerald-works/corvus/compare/v10.4.0...v10.4.1) (2022-09-27)



# [10.4.0](https://gitlab.com/emerald-works/corvus/compare/v10.3.0...v10.4.0) (2022-09-27)



# [10.3.0](https://gitlab.com/emerald-works/corvus/compare/v10.2.5...v10.3.0) (2022-09-22)



## [10.2.5](https://gitlab.com/emerald-works/corvus/compare/v10.2.4...v10.2.5) (2022-08-18)



## [10.2.4](https://gitlab.com/emerald-works/corvus/compare/v10.2.3...v10.2.4) (2022-08-18)



## [10.2.3](https://gitlab.com/emerald-works/corvus/compare/v10.2.2...v10.2.3) (2022-08-18)



## [10.2.2](https://gitlab.com/emerald-works/corvus/compare/v10.2.1...v10.2.2) (2022-08-18)



## [10.2.1](https://gitlab.com/emerald-works/corvus/compare/v10.2.0...v10.2.1) (2022-08-17)



# [10.2.0](https://gitlab.com/emerald-works/corvus/compare/v9.2.0...v10.2.0) (2022-08-17)


### Bug Fixes

* 9.x dependencies ([d3ee748](https://gitlab.com/emerald-works/corvus/commit/d3ee7489dc7ad47134569f373ddecc2452592fc6))
* merge ([ade3566](https://gitlab.com/emerald-works/corvus/commit/ade3566f69673585e81c954f74776876e3585142))
* package-locks ([3551369](https://gitlab.com/emerald-works/corvus/commit/35513691d5edc615a83e40a668ec2979b88bda2b))



# [9.2.0](https://gitlab.com/emerald-works/corvus/compare/v9.1.10...v9.2.0) (2022-08-11)



## [9.1.10](https://gitlab.com/emerald-works/corvus/compare/v9.1.9...v9.1.10) (2022-08-01)



## [9.1.9](https://gitlab.com/emerald-works/corvus/compare/v9.1.8...v9.1.9) (2022-07-31)





# [10.18.0](https://gitlab.com/emerald-works/corvus/compare/v10.17.0...v10.18.0) (2023-08-30)

**Note:** Version bump only for package @emerald-works/serverless-boilerplate





# [10.17.0](https://gitlab.com/emerald-works/corvus/compare/v10.16.2...v10.17.0) (2023-08-29)

**Note:** Version bump only for package @emerald-works/serverless-boilerplate





## [10.16.2](https://gitlab.com/emerald-works/corvus/compare/v10.16.1...v10.16.2) (2023-08-14)

**Note:** Version bump only for package @emerald-works/serverless-boilerplate





## [10.16.1](https://gitlab.com/emerald-works/corvus/compare/v10.16.0...v10.16.1) (2023-08-09)

**Note:** Version bump only for package @emerald-works/serverless-boilerplate





# [10.16.0](https://gitlab.com/emerald-works/corvus/compare/v10.15.0...v10.16.0) (2023-08-08)

**Note:** Version bump only for package @emerald-works/serverless-boilerplate





# [10.15.0](https://gitlab.com/emerald-works/corvus/compare/v10.14.4...v10.15.0) (2023-08-07)

**Note:** Version bump only for package @emerald-works/serverless-boilerplate





## [10.14.4](https://gitlab.com/emerald-works/corvus/compare/v10.14.3...v10.14.4) (2023-08-05)

**Note:** Version bump only for package @emerald-works/serverless-boilerplate





## [10.14.3](https://gitlab.com/emerald-works/corvus/compare/v10.14.2...v10.14.3) (2023-08-02)


### Bug Fixes

* using node 16 ([ab3a2da](https://gitlab.com/emerald-works/corvus/commit/ab3a2da68807b3c35672235bc2eaee2ddc09d7b3))





## [10.14.2](https://gitlab.com/emerald-works/corvus/compare/v10.14.1...v10.14.2) (2023-08-01)

**Note:** Version bump only for package @emerald-works/serverless-boilerplate





## [10.14.1](https://gitlab.com/emerald-works/corvus/compare/v10.14.0...v10.14.1) (2023-07-27)

**Note:** Version bump only for package @emerald-works/serverless-boilerplate





# [10.14.0](https://gitlab.com/emerald-works/corvus/compare/v10.13.0...v10.14.0) (2023-07-13)


### Features

* adding the ability of deploying modules ([6dc3994](https://gitlab.com/emerald-works/corvus/commit/6dc399426af191dc297ebb13407d7aed2021f0e2))





# [10.13.0](https://gitlab.com/emerald-works/corvus/compare/v10.12.1...v10.13.0) (2023-06-21)

**Note:** Version bump only for package @emerald-works/serverless-boilerplate





## [10.12.1](https://gitlab.com/emerald-works/corvus/compare/v10.12.0...v10.12.1) (2023-06-21)

**Note:** Version bump only for package @emerald-works/serverless-boilerplate





# [10.12.0](https://gitlab.com/emerald-works/corvus/compare/v10.11.1...v10.12.0) (2023-06-21)

**Note:** Version bump only for package @emerald-works/serverless-boilerplate





## [10.11.1](https://gitlab.com/emerald-works/corvus/compare/v10.11.0...v10.11.1) (2023-06-07)

**Note:** Version bump only for package @emerald-works/serverless-boilerplate





# [10.11.0](https://gitlab.com/emerald-works/corvus/compare/v10.10.0...v10.11.0) (2023-06-07)

**Note:** Version bump only for package @emerald-works/serverless-boilerplate





# [10.10.0](https://gitlab.com/emerald-works/corvus/compare/v10.9.9...v10.10.0) (2023-05-10)


### Bug Fixes

* package-locks ([b513ff8](https://gitlab.com/emerald-works/corvus/commit/b513ff8f267a8900ffc1c3ac7f8e03de289ceb6c))


### Features

* adding socket.io server and client ([dc0baa1](https://gitlab.com/emerald-works/corvus/commit/dc0baa1bfe19cda79420fa114e014deb2888ade1))





## [10.9.9](https://gitlab.com/emerald-works/corvus/compare/v10.9.8...v10.9.9) (2023-04-12)

**Note:** Version bump only for package @emerald-works/serverless-boilerplate





## [10.9.8](https://gitlab.com/emerald-works/corvus/compare/v10.9.7...v10.9.8) (2022-12-10)

**Note:** Version bump only for package @emerald-works/serverless-boilerplate





## [10.9.7](https://gitlab.com/emerald-works/corvus/compare/v10.9.6...v10.9.7) (2022-12-09)

**Note:** Version bump only for package @emerald-works/serverless-boilerplate





## [10.9.6](https://gitlab.com/emerald-works/corvus/compare/v10.9.5...v10.9.6) (2022-12-09)

**Note:** Version bump only for package @emerald-works/serverless-boilerplate





## [10.9.5](https://gitlab.com/emerald-works/corvus/compare/v10.9.4...v10.9.5) (2022-11-30)

**Note:** Version bump only for package @emerald-works/serverless-boilerplate





## [10.9.4](https://gitlab.com/emerald-works/corvus/compare/v10.9.3...v10.9.4) (2022-11-22)

**Note:** Version bump only for package @emerald-works/serverless-boilerplate





## [10.9.3](https://gitlab.com/emerald-works/corvus/compare/v10.9.2...v10.9.3) (2022-11-03)


### Bug Fixes

* using on demand ([1d177cb](https://gitlab.com/emerald-works/corvus/commit/1d177cbaf11450c32fdfdb380d71f3877009a548))





## [10.9.2](https://gitlab.com/emerald-works/corvus/compare/v10.9.1...v10.9.2) (2022-11-02)

**Note:** Version bump only for package @emerald-works/serverless-boilerplate





## [10.9.1](https://gitlab.com/emerald-works/corvus/compare/v10.9.0...v10.9.1) (2022-10-25)

**Note:** Version bump only for package @emerald-works/serverless-boilerplate





# [10.9.0](https://gitlab.com/emerald-works/corvus/compare/v10.8.0...v10.9.0) (2022-10-24)


### Features

* use http for core events ([c0b44f1](https://gitlab.com/emerald-works/corvus/commit/c0b44f1d75ff7d27a8491c5bad7ade642621f146))





# [10.8.0](https://gitlab.com/emerald-works/corvus/compare/v10.7.0...v10.8.0) (2022-10-19)


### Features

* adding cloudformation outputs ([cfae735](https://gitlab.com/emerald-works/corvus/commit/cfae73554c3629c21795abfc74c193a5c0434d17))





# [10.7.0](https://gitlab.com/emerald-works/corvus/compare/v10.6.1...v10.7.0) (2022-10-10)

**Note:** Version bump only for package @emerald-works/serverless-boilerplate





## [10.6.1](https://gitlab.com/emerald-works/corvus/compare/v10.6.0...v10.6.1) (2022-10-07)

**Note:** Version bump only for package @emerald-works/serverless-boilerplate





# [10.6.0](https://gitlab.com/emerald-works/corvus/compare/v10.5.0...v10.6.0) (2022-10-06)

**Note:** Version bump only for package @emerald-works/serverless-boilerplate





# [10.5.0](https://gitlab.com/emerald-works/corvus/compare/v10.4.1...v10.5.0) (2022-10-05)


### Features

* include, exclude and per function env vars ([0cde4d2](https://gitlab.com/emerald-works/corvus/commit/0cde4d26c9022a77e369e528b67c9bde8cdd3c32))





## [10.4.1](https://gitlab.com/emerald-works/corvus/compare/v10.4.0...v10.4.1) (2022-09-27)

**Note:** Version bump only for package @emerald-works/serverless-boilerplate





# [10.4.0](https://gitlab.com/emerald-works/corvus/compare/v10.3.0...v10.4.0) (2022-09-27)

**Note:** Version bump only for package @emerald-works/serverless-boilerplate





# [10.3.0](https://gitlab.com/emerald-works/corvus/compare/v10.2.5...v10.3.0) (2022-09-22)

**Note:** Version bump only for package @emerald-works/serverless-boilerplate





## [10.2.5](https://gitlab.com/emerald-works/corvus/compare/v10.2.4...v10.2.5) (2022-08-18)

**Note:** Version bump only for package @emerald-works/serverless-boilerplate





## [10.2.4](https://gitlab.com/emerald-works/corvus/compare/v10.2.3...v10.2.4) (2022-08-18)

**Note:** Version bump only for package @emerald-works/serverless-boilerplate





## [10.2.3](https://gitlab.com/emerald-works/corvus/compare/v10.2.2...v10.2.3) (2022-08-18)

**Note:** Version bump only for package @emerald-works/serverless-boilerplate





## [10.2.2](https://gitlab.com/emerald-works/corvus/compare/v10.2.1...v10.2.2) (2022-08-18)

**Note:** Version bump only for package @emerald-works/serverless-boilerplate





## [10.2.1](https://gitlab.com/emerald-works/corvus/compare/v10.2.0...v10.2.1) (2022-08-17)

**Note:** Version bump only for package @emerald-works/serverless-boilerplate





# [10.2.0](https://gitlab.com/emerald-works/corvus/compare/v9.2.0...v10.2.0) (2022-08-17)


### Bug Fixes

* 9.x dependencies ([d3ee748](https://gitlab.com/emerald-works/corvus/commit/d3ee7489dc7ad47134569f373ddecc2452592fc6))
* merge ([ade3566](https://gitlab.com/emerald-works/corvus/commit/ade3566f69673585e81c954f74776876e3585142))
* package-locks ([3551369](https://gitlab.com/emerald-works/corvus/commit/35513691d5edc615a83e40a668ec2979b88bda2b))





# [9.2.0](https://gitlab.com/emerald-works/corvus/compare/v9.1.10...v9.2.0) (2022-08-11)

**Note:** Version bump only for package @emerald-works/serverless-boilerplate





## [9.1.10](https://gitlab.com/emerald-works/corvus/compare/v9.1.9...v9.1.10) (2022-08-01)

**Note:** Version bump only for package @emerald-works/serverless-boilerplate





## [9.1.9](https://gitlab.com/emerald-works/corvus/compare/v9.1.8...v9.1.9) (2022-07-31)

**Note:** Version bump only for package @emerald-works/serverless-boilerplate





## [9.1.8](https://gitlab.com/emerald-works/corvus/compare/v9.1.7...v9.1.8) (2022-06-28)

**Note:** Version bump only for package @emerald-works/serverless-boilerplate





## [9.1.7](https://gitlab.com/emerald-works/corvus/compare/v9.1.6...v9.1.7) (2022-06-14)

**Note:** Version bump only for package @emerald-works/serverless-boilerplate





## [9.1.6](https://gitlab.com/emerald-works/corvus/compare/v9.1.5...v9.1.6) (2022-06-08)

**Note:** Version bump only for package @emerald-works/serverless-boilerplate





## [9.1.5](https://gitlab.com/emerald-works/corvus/compare/v9.1.4...v9.1.5) (2022-06-08)


### Bug Fixes

* package-locks ([272bc38](https://gitlab.com/emerald-works/corvus/commit/272bc383244a8f58b062637496735326e6bec92c))





## [9.1.4](https://gitlab.com/emerald-works/corvus/compare/v9.1.3...v9.1.4) (2022-06-07)

**Note:** Version bump only for package @emerald-works/serverless-boilerplate





## [9.1.3](https://gitlab.com/emerald-works/corvus/compare/v9.1.2...v9.1.3) (2022-06-07)

**Note:** Version bump only for package @emerald-works/serverless-boilerplate





## [9.1.2](https://gitlab.com/emerald-works/corvus/compare/v9.1.1...v9.1.2) (2022-06-02)


### Bug Fixes

* package locks ([746f3f2](https://gitlab.com/emerald-works/corvus/commit/746f3f2b557e1e196de4bc3260e704132e482ee8))





## [9.1.1](https://gitlab.com/emerald-works/corvus/compare/v9.1.0...v9.1.1) (2022-06-01)

**Note:** Version bump only for package @emerald-works/serverless-boilerplate





# [9.1.0](https://gitlab.com/emerald-works/corvus/compare/v8.11.0...v9.1.0) (2022-05-26)


### Bug Fixes

* package-locks ([506d0b3](https://gitlab.com/emerald-works/corvus/commit/506d0b3ad55d4b224f36a04f8e480d9891cd7e5a))
* package-locks ([e0d1050](https://gitlab.com/emerald-works/corvus/commit/e0d105041ad4d9e960778b6e53a135ef52780cf5))
* package-locks ([0bcc247](https://gitlab.com/emerald-works/corvus/commit/0bcc2472ad56b6672ff25908d0a7cdbfeb5ac018))
* peerDependencies ([f4d57bc](https://gitlab.com/emerald-works/corvus/commit/f4d57bc66c2f934905929b60dc5124a67191a3f9))





# [8.11.0](https://gitlab.com/emerald-works/corvus/compare/v8.10.8...v8.11.0) (2022-05-02)

**Note:** Version bump only for package @emerald-works/serverless-boilerplate





## [8.10.8](https://gitlab.com/emerald-works/corvus/compare/v8.10.7...v8.10.8) (2022-04-28)

**Note:** Version bump only for package @emerald-works/serverless-boilerplate





## [8.10.7](https://gitlab.com/emerald-works/corvus/compare/v8.10.6...v8.10.7) (2022-03-18)

**Note:** Version bump only for package @emerald-works/serverless-boilerplate





## [8.10.6](https://gitlab.com/emerald-works/corvus/compare/v8.10.5...v8.10.6) (2022-03-17)

**Note:** Version bump only for package @emerald-works/serverless-boilerplate





## [8.10.5](https://gitlab.com/emerald-works/corvus/compare/v8.10.4...v8.10.5) (2022-03-17)


### Bug Fixes

* cognito user pool name env var ([ba293f0](https://gitlab.com/emerald-works/corvus/commit/ba293f0c1afbf7d8f0c14d9a8b726cd142783636))





## [8.10.4](https://gitlab.com/emerald-works/corvus/compare/v8.10.3...v8.10.4) (2022-03-16)

**Note:** Version bump only for package @emerald-works/serverless-boilerplate





## [8.10.3](https://gitlab.com/emerald-works/corvus/compare/v8.10.2...v8.10.3) (2022-02-02)

**Note:** Version bump only for package @emerald-works/serverless-boilerplate





## [8.10.2](https://gitlab.com/emerald-works/corvus/compare/v8.10.1...v8.10.2) (2022-02-01)

**Note:** Version bump only for package @emerald-works/serverless-boilerplate





## [8.10.1](https://gitlab.com/emerald-works/corvus/compare/v8.10.0...v8.10.1) (2022-01-31)

**Note:** Version bump only for package @emerald-works/serverless-boilerplate





# [8.10.0](https://gitlab.com/emerald-works/corvus/compare/v8.9.0...v8.10.0) (2022-01-28)

**Note:** Version bump only for package @emerald-works/serverless-boilerplate





# [8.9.0](https://gitlab.com/emerald-works/corvus/compare/v8.8.1...v8.9.0) (2022-01-28)

**Note:** Version bump only for package @emerald-works/serverless-boilerplate





## [8.8.1](https://gitlab.com/emerald-works/corvus/compare/v8.8.0...v8.8.1) (2022-01-27)

**Note:** Version bump only for package @emerald-works/serverless-boilerplate





# [8.8.0](https://gitlab.com/emerald-works/corvus/compare/v8.6.1...v8.8.0) (2022-01-27)


### Bug Fixes

* chat broadcast destination ([7cdcf7c](https://gitlab.com/emerald-works/corvus/commit/7cdcf7c06e9b38d7ea4ab5cde355a244eeb14086))
* dynamoose dependencies ([37151a7](https://gitlab.com/emerald-works/corvus/commit/37151a7fe1ef83c98df96549d1a44df64f055797))
* generating secure params ([04851e5](https://gitlab.com/emerald-works/corvus/commit/04851e5de2c38aa11f1133e7ce0c2a1b9fd996ac))
* nwe version ([9c3fd3f](https://gitlab.com/emerald-works/corvus/commit/9c3fd3ff70b1b2fe62fc2c5026647375db413b47))
* post deploy on debug stack ([0204e26](https://gitlab.com/emerald-works/corvus/commit/0204e269378a394c4395d73411000d8204efb77b))
* using dependencies instead of dev dependencies ([65099a4](https://gitlab.com/emerald-works/corvus/commit/65099a464d203f3a63fc1bc1924d9a593fd9ae0f))
* using micro packages ([b52fc09](https://gitlab.com/emerald-works/corvus/commit/b52fc09c469505b60be7d27f7d89f6514b8dece9))
* using new handlers ([459d47b](https://gitlab.com/emerald-works/corvus/commit/459d47b4d111dd70f5b2142b04ab81c2a8ca281e))
* webpack ([e34f7a7](https://gitlab.com/emerald-works/corvus/commit/e34f7a7384338381bcb5008347eca80ccbfc3fa9))
* webpack deps ([91e0d05](https://gitlab.com/emerald-works/corvus/commit/91e0d05ac30a7937c50b6c4ba82dda3d602b5de4))
* xray for v3 sdk ([3ee0483](https://gitlab.com/emerald-works/corvus/commit/3ee04830b4d8b7d02fd85a3cfa49f6cf572f626b))


### Features

* adding dynamoose packages ([8418882](https://gitlab.com/emerald-works/corvus/commit/84188821c883505b5202fbbda290367e35748a62))
* adding new packages ([f296158](https://gitlab.com/emerald-works/corvus/commit/f2961581d2d265242377218d6854870f9a8319fe))
* mongoose middleware ([992f0c6](https://gitlab.com/emerald-works/corvus/commit/992f0c6a635bf306894d96bd1cf7af64230bb4d1))
* using micro packages on websocket functions ([3ae17ee](https://gitlab.com/emerald-works/corvus/commit/3ae17eef8c9bc2677af29667f22320c27a0f017f))





# [8.7.0](https://gitlab.com/emerald-works/corvus/compare/v8.6.1...v8.7.0) (2022-01-27)


### Bug Fixes

* chat broadcast destination ([7cdcf7c](https://gitlab.com/emerald-works/corvus/commit/7cdcf7c06e9b38d7ea4ab5cde355a244eeb14086))
* dynamoose dependencies ([37151a7](https://gitlab.com/emerald-works/corvus/commit/37151a7fe1ef83c98df96549d1a44df64f055797))
* generating secure params ([04851e5](https://gitlab.com/emerald-works/corvus/commit/04851e5de2c38aa11f1133e7ce0c2a1b9fd996ac))
* nwe version ([9c3fd3f](https://gitlab.com/emerald-works/corvus/commit/9c3fd3ff70b1b2fe62fc2c5026647375db413b47))
* post deploy on debug stack ([0204e26](https://gitlab.com/emerald-works/corvus/commit/0204e269378a394c4395d73411000d8204efb77b))
* using micro packages ([b52fc09](https://gitlab.com/emerald-works/corvus/commit/b52fc09c469505b60be7d27f7d89f6514b8dece9))
* using new handlers ([459d47b](https://gitlab.com/emerald-works/corvus/commit/459d47b4d111dd70f5b2142b04ab81c2a8ca281e))
* webpack ([e34f7a7](https://gitlab.com/emerald-works/corvus/commit/e34f7a7384338381bcb5008347eca80ccbfc3fa9))
* webpack deps ([91e0d05](https://gitlab.com/emerald-works/corvus/commit/91e0d05ac30a7937c50b6c4ba82dda3d602b5de4))
* xray for v3 sdk ([3ee0483](https://gitlab.com/emerald-works/corvus/commit/3ee04830b4d8b7d02fd85a3cfa49f6cf572f626b))


### Features

* adding dynamoose packages ([8418882](https://gitlab.com/emerald-works/corvus/commit/84188821c883505b5202fbbda290367e35748a62))
* adding new packages ([f296158](https://gitlab.com/emerald-works/corvus/commit/f2961581d2d265242377218d6854870f9a8319fe))
* mongoose middleware ([992f0c6](https://gitlab.com/emerald-works/corvus/commit/992f0c6a635bf306894d96bd1cf7af64230bb4d1))
* using micro packages on websocket functions ([3ae17ee](https://gitlab.com/emerald-works/corvus/commit/3ae17eef8c9bc2677af29667f22320c27a0f017f))





## [8.6.1](https://gitlab.com/emerald-works/corvus/compare/v8.6.0...v8.6.1) (2022-01-25)

**Note:** Version bump only for package @emerald-works/serverless-boilerplate





# [8.6.0](https://gitlab.com/emerald-works/corvus/compare/v8.5.3...v8.6.0) (2022-01-22)

**Note:** Version bump only for package @emerald-works/serverless-boilerplate





## [8.5.3](https://gitlab.com/emerald-works/corvus/compare/v8.5.2...v8.5.3) (2022-01-17)


### Bug Fixes

* package-lock.json ([acad9bf](https://gitlab.com/emerald-works/corvus/commit/acad9bf40567c0024408192984dd5188823407ad))
* recent ci ([e8692aa](https://gitlab.com/emerald-works/corvus/commit/e8692aa65656b8b3cec73a031d3e9fc66f727dc2))
* remove yarn mentions ([591a829](https://gitlab.com/emerald-works/corvus/commit/591a829c3e95d4065c2cb3760561e6c50562de5e))





## [8.5.2](https://gitlab.com/emerald-works/corvus/compare/v8.5.1...v8.5.2) (2022-01-14)

**Note:** Version bump only for package @emerald-works/serverless-boilerplate





## [8.5.1](https://gitlab.com/emerald-works/corvus/compare/v8.5.0...v8.5.1) (2022-01-14)

**Note:** Version bump only for package @emerald-works/serverless-boilerplate





# [8.5.0](https://gitlab.com/emerald-works/corvus/compare/v8.4.1...v8.5.0) (2021-12-10)


### Bug Fixes

* ack on integration tests ([c1511b0](https://gitlab.com/emerald-works/corvus/commit/c1511b0d562da7c98919c182164d29152457bf7f))
* deleting all objects ([1f8168b](https://gitlab.com/emerald-works/corvus/commit/1f8168bcf83d2cd75f8695d1d02fdbcf9c7c408f))
* hash ([83a29e2](https://gitlab.com/emerald-works/corvus/commit/83a29e29e42e76440ee7224bbb7bcb49b9773daa))
* lint ([43c6a06](https://gitlab.com/emerald-works/corvus/commit/43c6a06ce5e8586e94e7b433a4a58c75fd07418d))
* package-lock ([84090bf](https://gitlab.com/emerald-works/corvus/commit/84090bf7dcafcade06a624f0bee4a7560aaab683))
* package-lock ([4e3c514](https://gitlab.com/emerald-works/corvus/commit/4e3c514e69bad3161aaf46e4b7dbca26b7fd9739))
* package-lock ([4d8133f](https://gitlab.com/emerald-works/corvus/commit/4d8133ff78a011adcb642ca0428aa917471ada93))
* package-lock ([a7e2d73](https://gitlab.com/emerald-works/corvus/commit/a7e2d733189b537b05cfeb0f23978c4ea7dc12b6))
* package-lock ([8b5653c](https://gitlab.com/emerald-works/corvus/commit/8b5653cfc1e690a128799180662198160df30f74))
* package-lock ([be84f89](https://gitlab.com/emerald-works/corvus/commit/be84f89a940e7c85068fcb01d1145f5e203bd12a))
* package-lock ([3902d3b](https://gitlab.com/emerald-works/corvus/commit/3902d3b5966b94d00e340ce3037267adcbf82a89))
* package-lock ([4bbb3bc](https://gitlab.com/emerald-works/corvus/commit/4bbb3bc5e0de8d2b68ed5652c9f324bec9e862b1))
* package-lock ([143f28d](https://gitlab.com/emerald-works/corvus/commit/143f28d67e43cdb0fecf3cb3223298cf28ccdd08))
* package-lock ([5dd3270](https://gitlab.com/emerald-works/corvus/commit/5dd3270bb0898a19c813b63f7e49ea5b4d031059))
* removed console.log in corvusPostDeployBackend ([9e27eb5](https://gitlab.com/emerald-works/corvus/commit/9e27eb51605ac1a506d6019b4e63d2173cc9096c))
* using success instead of ack ([b8ece79](https://gitlab.com/emerald-works/corvus/commit/b8ece790cd1a6a4433ac2c2d4e16580caebf2bed))


### Features

* adding bypassDebugStack option ([ee076a7](https://gitlab.com/emerald-works/corvus/commit/ee076a774c9f7f3958775273be4a05106763073b))
* cli tests ([7855e69](https://gitlab.com/emerald-works/corvus/commit/7855e69b1d90b8d9377f7e4966c2b53932217956))
* demoing event handling and disconnection issues ([4b8a6d4](https://gitlab.com/emerald-works/corvus/commit/4b8a6d47c34db5b9a9f8bbae0244c9542beb49c6))
* improving debug stack deploy time ([1259d7f](https://gitlab.com/emerald-works/corvus/commit/1259d7ffda05a88ab86f84c8e35371a7047298e3))





## [8.4.1](https://gitlab.com/emerald-works/corvus/compare/v8.4.0...v8.4.1) (2021-12-03)

**Note:** Version bump only for package @emerald-works/serverless-boilerplate





# [8.4.0](https://gitlab.com/emerald-works/corvus/compare/v8.3.0...v8.4.0) (2021-12-03)


### Bug Fixes

* package-lock ([7e0ff48](https://gitlab.com/emerald-works/corvus/commit/7e0ff48687573db76a28d201ee0a85e0116552c7))


### Features

* upgrade serverless and install lambda insights ([bc67c24](https://gitlab.com/emerald-works/corvus/commit/bc67c24bdd2b4ba55285d60e0467d4d2721e78be))





# [8.3.0](https://gitlab.com/emerald-works/corvus/compare/v8.2.10...v8.3.0) (2021-12-02)


### Bug Fixes

* package-lock ([6dc3a9b](https://gitlab.com/emerald-works/corvus/commit/6dc3a9b89724bb760abf1750aa0db995b58cb355))





## [8.2.10](https://gitlab.com/emerald-works/corvus/compare/v8.2.9...v8.2.10) (2021-12-02)

**Note:** Version bump only for package @emerald-works/serverless-boilerplate





## [8.2.9](https://gitlab.com/emerald-works/corvus/compare/v8.2.8...v8.2.9) (2021-12-02)

**Note:** Version bump only for package @emerald-works/serverless-boilerplate





## [8.2.8](https://gitlab.com/emerald-works/corvus/compare/v8.2.7...v8.2.8) (2021-11-24)

**Note:** Version bump only for package @emerald-works/serverless-boilerplate





## [8.2.7](https://gitlab.com/emerald-works/corvus/compare/v8.2.6...v8.2.7) (2021-10-28)

**Note:** Version bump only for package @emerald-works/serverless-boilerplate





## [8.2.6](https://gitlab.com/emerald-works/corvus/compare/v8.2.5...v8.2.6) (2021-10-28)


### Bug Fixes

* subscribing from secure params tenantKey ([91a1132](https://gitlab.com/emerald-works/corvus/commit/91a1132d1834eb552bf630d507cee1a7935735d5))





## [8.2.5](https://gitlab.com/emerald-works/corvus/compare/v8.2.4...v8.2.5) (2021-10-20)

**Note:** Version bump only for package @emerald-works/serverless-boilerplate





## [8.2.4](https://gitlab.com/emerald-works/corvus/compare/v8.2.3...v8.2.4) (2021-10-20)

**Note:** Version bump only for package @emerald-works/serverless-boilerplate





## [8.2.3](https://gitlab.com/emerald-works/corvus/compare/v8.2.2...v8.2.3) (2021-10-20)

**Note:** Version bump only for package @emerald-works/serverless-boilerplate





## [8.2.2](https://gitlab.com/emerald-works/corvus/compare/v8.2.1...v8.2.2) (2021-10-15)

**Note:** Version bump only for package @emerald-works/serverless-boilerplate





## [8.2.1](https://gitlab.com/emerald-works/corvus/compare/v8.2.0...v8.2.1) (2021-09-16)

**Note:** Version bump only for package @emerald-works/serverless-boilerplate





# [8.2.0](https://gitlab.com/emerald-works/corvus/compare/v8.1.0...v8.2.0) (2021-09-14)

**Note:** Version bump only for package @emerald-works/serverless-boilerplate





# [8.1.0](https://gitlab.com/emerald-works/corvus/compare/v8.0.0...v8.1.0) (2021-09-14)


### Bug Fixes

* corvus package-locks ([f611ff7](https://gitlab.com/emerald-works/corvus/commit/f611ff77546e5b493ccb9960295201b9544e4987))


### Features

* using new version of sst ([5003c1e](https://gitlab.com/emerald-works/corvus/commit/5003c1ee0b3a6e678694331dc0e191e95b5bfa43))



# [7.4.0](https://gitlab.com/emerald-works/corvus/compare/v7.3.0...v7.4.0) (2021-08-16)


### Features

* new version ([973bd83](https://gitlab.com/emerald-works/corvus/commit/973bd83a9708e3418f4305645d34c634c047e5e9))



# [7.3.0](https://gitlab.com/emerald-works/corvus/compare/v7.2.1...v7.3.0) (2021-08-09)





# [7.4.0](https://gitlab.com/emerald-works/corvus/compare/v7.3.0...v7.4.0) (2021-08-16)


### Features

* new version ([973bd83](https://gitlab.com/emerald-works/corvus/commit/973bd83a9708e3418f4305645d34c634c047e5e9))





# [7.3.0](https://gitlab.com/emerald-works/corvus/compare/v7.2.1...v7.3.0) (2021-08-09)

**Note:** Version bump only for package @emerald-works/serverless-boilerplate





## [7.2.1](https://gitlab.com/emerald-works/corvus/compare/v7.2.0...v7.2.1) (2021-07-29)

**Note:** Version bump only for package @emerald-works/serverless-boilerplate





# [7.2.0](https://gitlab.com/emerald-works/corvus/compare/v7.1.2...v7.2.0) (2021-07-29)

**Note:** Version bump only for package @emerald-works/serverless-boilerplate





## [7.1.2](https://gitlab.com/emerald-works/corvus/compare/v7.1.1...v7.1.2) (2021-07-28)

**Note:** Version bump only for package @emerald-works/serverless-boilerplate





## [7.1.1](https://gitlab.com/emerald-works/corvus/compare/v7.1.0...v7.1.1) (2021-07-26)

**Note:** Version bump only for package @emerald-works/serverless-boilerplate





# [7.1.0](https://gitlab.com/emerald-works/corvus/compare/v7.0.3...v7.1.0) (2021-07-20)

**Note:** Version bump only for package @emerald-works/serverless-boilerplate





## [7.0.3](https://gitlab.com/emerald-works/corvus/compare/v7.0.2...v7.0.3) (2021-07-15)

**Note:** Version bump only for package @emerald-works/serverless-boilerplate





## [7.0.2](https://gitlab.com/emerald-works/corvus/compare/v7.0.1...v7.0.2) (2021-07-15)

**Note:** Version bump only for package @emerald-works/serverless-boilerplate





## [7.0.1](https://gitlab.com/emerald-works/corvus/compare/v7.0.0...v7.0.1) (2021-07-15)

**Note:** Version bump only for package @emerald-works/serverless-boilerplate





## [6.2.2](https://gitlab.com/emerald-works/corvus/compare/v6.2.1...v6.2.2) (2021-07-14)

**Note:** Version bump only for package @emerald-works/serverless-boilerplate





## [6.2.1](https://gitlab.com/emerald-works/corvus/compare/v6.2.0...v6.2.1) (2021-07-14)

**Note:** Version bump only for package @emerald-works/serverless-boilerplate





# [6.2.0](https://gitlab.com/emerald-works/corvus/compare/v6.1.0...v6.2.0) (2021-07-09)


### Features

* moving event validator to base handler ([1fe4705](https://gitlab.com/emerald-works/corvus/commit/1fe4705399143f8c6a64044222d67dee004344fa))





# [6.1.0](https://gitlab.com/emerald-works/corvus/compare/v6.0.3...v6.1.0) (2021-07-08)


### Features

* using npm as lerna client ([6c2b27b](https://gitlab.com/emerald-works/corvus/commit/6c2b27b7af6b7ef6993745981efaa6e662308555))





## [6.0.3](https://gitlab.com/emerald-works/corvus/compare/v6.0.2...v6.0.3) (2021-07-05)

**Note:** Version bump only for package @emerald-works/serverless-boilerplate





## [6.0.2](https://gitlab.com/emerald-works/corvus/compare/v6.0.1...v6.0.2) (2021-07-05)

**Note:** Version bump only for package @emerald-works/serverless-boilerplate





## [6.0.1](https://gitlab.com/emerald-works/corvus/compare/v6.0.0...v6.0.1) (2021-07-02)

**Note:** Version bump only for package @emerald-works/serverless-boilerplate





# [6.0.0](https://gitlab.com/emerald-works/corvus/compare/v5.11.0...v6.0.0) (2021-07-02)

**Note:** Version bump only for package @emerald-works/serverless-boilerplate





# [5.11.0](https://gitlab.com/emerald-works/corvus/compare/v5.10.0...v5.11.0) (2021-06-26)

**Note:** Version bump only for package @emerald-works/serverless-boilerplate





# [5.10.0](https://gitlab.com/emerald-works/corvus/compare/v5.9.8...v5.10.0) (2021-06-17)

**Note:** Version bump only for package @emerald-works/serverless-boilerplate





## [5.9.8](https://gitlab.com/emerald-works/corvus/compare/v5.9.7...v5.9.8) (2021-06-15)

**Note:** Version bump only for package @emerald-works/serverless-boilerplate





## [5.9.7](https://gitlab.com/emerald-works/corvus/compare/v5.9.6...v5.9.7) (2021-06-04)

**Note:** Version bump only for package @emerald-works/serverless-boilerplate





## [5.9.6](https://gitlab.com/emerald-works/corvus/compare/v5.9.5...v5.9.6) (2021-06-04)

**Note:** Version bump only for package @emerald-works/serverless-boilerplate





## [5.9.5](https://gitlab.com/emerald-works/corvus/compare/v5.9.4...v5.9.5) (2021-06-04)

**Note:** Version bump only for package @emerald-works/serverless-boilerplate





## [5.9.4](https://gitlab.com/emerald-works/corvus/compare/v5.9.3...v5.9.4) (2021-06-03)

**Note:** Version bump only for package @emerald-works/serverless-boilerplate





## [5.9.3](https://gitlab.com/emerald-works/corvus/compare/v5.9.2...v5.9.3) (2021-06-02)

**Note:** Version bump only for package @emerald-works/serverless-boilerplate





## [5.9.2](https://gitlab.com/emerald-works/corvus/compare/v5.9.1...v5.9.2) (2021-05-27)

**Note:** Version bump only for package @emerald-works/serverless-boilerplate





## [5.9.1](https://gitlab.com/emerald-works/corvus/compare/v5.9.0...v5.9.1) (2021-05-26)

**Note:** Version bump only for package @emerald-works/serverless-boilerplate





# [5.9.0](https://gitlab.com/emerald-works/corvus/compare/v5.8.4...v5.9.0) (2021-05-25)

**Note:** Version bump only for package @emerald-works/serverless-boilerplate





## [5.8.4](https://gitlab.com/emerald-works/corvus/compare/v5.8.3...v5.8.4) (2021-05-24)

**Note:** Version bump only for package @emerald-works/serverless-boilerplate





## [5.8.3](https://gitlab.com/emerald-works/corvus/compare/v5.8.2...v5.8.3) (2021-05-24)

**Note:** Version bump only for package @emerald-works/serverless-boilerplate





## [5.8.2](https://gitlab.com/emerald-works/corvus/compare/v5.8.1...v5.8.2) (2021-05-21)

**Note:** Version bump only for package @emerald-works/serverless-boilerplate





## [5.8.1](https://gitlab.com/emerald-works/corvus/compare/v5.8.0...v5.8.1) (2021-05-21)

**Note:** Version bump only for package @emerald-works/serverless-boilerplate





# [5.8.0](https://gitlab.com/emerald-works/corvus/compare/v5.7.0...v5.8.0) (2021-05-21)


### Features

* adding prerender lambdas for SEO ([67a67f3](https://gitlab.com/emerald-works/corvus/commit/67a67f34c5f2d4c6c642bc3862a6da19f002cc9c))





# [5.7.0](https://gitlab.com/emerald-works/corvus/compare/v5.6.1...v5.7.0) (2021-05-20)

**Note:** Version bump only for package @emerald-works/serverless-boilerplate





## [5.6.1](https://gitlab.com/emerald-works/corvus/compare/v5.6.0...v5.6.1) (2021-05-18)

**Note:** Version bump only for package @emerald-works/serverless-boilerplate





# [5.6.0](https://gitlab.com/emerald-works/corvus/compare/v5.5.6...v5.6.0) (2021-05-17)

**Note:** Version bump only for package @emerald-works/serverless-boilerplate





## [5.5.6](https://gitlab.com/emerald-works/corvus/compare/v5.5.5...v5.5.6) (2021-05-17)

**Note:** Version bump only for package @emerald-works/serverless-boilerplate





## [5.5.5](https://gitlab.com/emerald-works/corvus/compare/v5.5.4...v5.5.5) (2021-05-13)

**Note:** Version bump only for package @emerald-works/serverless-boilerplate





## [5.5.4](https://gitlab.com/emerald-works/corvus/compare/v5.5.3...v5.5.4) (2021-05-13)

**Note:** Version bump only for package @emerald-works/serverless-boilerplate





## [5.5.3](https://gitlab.com/emerald-works/corvus/compare/v5.5.2...v5.5.3) (2021-05-13)

**Note:** Version bump only for package @emerald-works/serverless-boilerplate





## [5.5.2](https://gitlab.com/emerald-works/corvus/compare/v5.5.1...v5.5.2) (2021-05-13)

**Note:** Version bump only for package @emerald-works/serverless-boilerplate





## [5.5.1](https://gitlab.com/emerald-works/corvus/compare/v5.5.0...v5.5.1) (2021-05-12)

**Note:** Version bump only for package @emerald-works/serverless-boilerplate





# [5.5.0](https://gitlab.com/emerald-works/corvus/compare/v5.3.1...v5.5.0) (2021-05-12)


### Features

* **frontend:** new ui ([6bfc124](https://gitlab.com/emerald-works/corvus/commit/6bfc124f1c4c2c95b948cadd36baab78cee3962e))





# [5.4.0](https://gitlab.com/emerald-works/corvus/compare/v5.3.1...v5.4.0) (2021-05-11)


### Features

* **frontend:** new ui ([6bfc124](https://gitlab.com/emerald-works/corvus/commit/6bfc124f1c4c2c95b948cadd36baab78cee3962e))





## [5.3.1](https://gitlab.com/emerald-works/corvus/compare/v5.3.0...v5.3.1) (2021-05-10)

**Note:** Version bump only for package @emerald-works/serverless-boilerplate





# [5.3.0](https://gitlab.com/emerald-works/corvus/compare/v5.2.1...v5.3.0) (2021-05-07)

**Note:** Version bump only for package @emerald-works/serverless-boilerplate





## [5.2.1](https://gitlab.com/emerald-works/corvus/compare/v5.2.0...v5.2.1) (2021-05-07)

**Note:** Version bump only for package @emerald-works/serverless-boilerplate





# [5.2.0](https://gitlab.com/emerald-works/corvus/compare/v5.1.1...v5.2.0) (2021-05-07)

**Note:** Version bump only for package @emerald-works/serverless-boilerplate





## [5.1.1](https://gitlab.com/emerald-works/corvus/compare/v5.1.0...v5.1.1) (2021-05-05)

**Note:** Version bump only for package @emerald-works/serverless-boilerplate





# [5.1.0](https://gitlab.com/emerald-works/corvus/compare/v5.0.2...v5.1.0) (2021-05-01)

**Note:** Version bump only for package @emerald-works/serverless-boilerplate





## [5.0.2](https://gitlab.com/emerald-works/corvus/compare/v5.0.1...v5.0.2) (2021-04-30)

**Note:** Version bump only for package @emerald-works/serverless-boilerplate





## [5.0.1](https://gitlab.com/emerald-works/corvus/compare/v5.0.0...v5.0.1) (2021-04-30)

**Note:** Version bump only for package @emerald-works/serverless-boilerplate





# [5.0.0](https://gitlab.com/emerald-works/corvus/compare/v4.2.1...v5.0.0) (2021-04-30)


### Bug Fixes

* adding new dependencies ([c0eac02](https://gitlab.com/emerald-works/corvus/commit/c0eac02c57b54eb457b757a90609015d79a121a6))


### Features

* adding the ability to keep lambdas warmed ([d37ad09](https://gitlab.com/emerald-works/corvus/commit/d37ad092b1028fe992ef5890bb32aeb02e02bf21))
* enabling x-ray by default ([76fdc1e](https://gitlab.com/emerald-works/corvus/commit/76fdc1e6d19cbebbb4bb19349897288599f849ac))


### BREAKING CHANGES

* serverless-plugin-warmup is now required. Run 'npm i -D serverless-plugin-warmup' on your backend project to use this version.
* middy-middleware-warmup is now required. Run 'npm i middy-middleware-warmup' on your backend to use this version.
* aws-xray-sdk-core is now required. Run 'npm i -D aws-xray-sdk-core' on your backend project to use this version.





## [4.2.1](https://gitlab.com/emerald-works/corvus/compare/v4.2.0...v4.2.1) (2021-04-30)

**Note:** Version bump only for package @emerald-works/serverless-boilerplate





# [4.2.0](https://gitlab.com/emerald-works/corvus/compare/v4.0.0-beta...v4.2.0) (2021-04-29)


### Bug Fixes

* bootstrap and local dot-env instabilities ([35cc9b1](https://gitlab.com/emerald-works/corvus/commit/35cc9b1ee107eb8161998e993fb59c81cdda7a0c))
* counter realtime with effects model ([d9ab8e3](https://gitlab.com/emerald-works/corvus/commit/d9ab8e3d13a0e0bf78993ff615d17144e2db408e))
* missing licenses ([e32083d](https://gitlab.com/emerald-works/corvus/commit/e32083d2d463a6b9b3b22e70934b6b49330e21a9))
* removing dotenv/config ([f53b784](https://gitlab.com/emerald-works/corvus/commit/f53b7845a02cde68112382aedc20f69abc5f86e4))
* serverless and split stack compatibility ([330cb2a](https://gitlab.com/emerald-works/corvus/commit/330cb2a23f426716b2eb862df90a0d7b4f257b74))
* trying wildcard permissions ([3dba3bf](https://gitlab.com/emerald-works/corvus/commit/3dba3bf4ff10271a58328840fa0cd4224c19e985))
* using ondemand for counter dynamodb table ([c305400](https://gitlab.com/emerald-works/corvus/commit/c305400ba5713edd5b79ead6e16129f5032ea3f3))


### Features

* dynamoose model builder and making a true offline experience ([8109081](https://gitlab.com/emerald-works/corvus/commit/81090818b1d82ea6f61c33b7f3830dd0424ad7fe))
* fixing serverless-dotenv-plugin and normalising serverless yml variables ([d94d9de](https://gitlab.com/emerald-works/corvus/commit/d94d9def8553d5fbdbc43ce8137c3cdd0e211b4f))
* using npm instead of yarn ([6a9525c](https://gitlab.com/emerald-works/corvus/commit/6a9525cd4ebe6a0573cc57fc90c2cd07469cd229))





# [4.1.0](https://gitlab.com/emerald-works/corvus/compare/v4.0.0-beta...v4.1.0) (2021-04-29)


### Bug Fixes

* bootstrap and local dot-env instabilities ([35cc9b1](https://gitlab.com/emerald-works/corvus/commit/35cc9b1ee107eb8161998e993fb59c81cdda7a0c))
* counter realtime with effects model ([d9ab8e3](https://gitlab.com/emerald-works/corvus/commit/d9ab8e3d13a0e0bf78993ff615d17144e2db408e))
* missing licenses ([e32083d](https://gitlab.com/emerald-works/corvus/commit/e32083d2d463a6b9b3b22e70934b6b49330e21a9))
* removing dotenv/config ([f53b784](https://gitlab.com/emerald-works/corvus/commit/f53b7845a02cde68112382aedc20f69abc5f86e4))
* serverless and split stack compatibility ([330cb2a](https://gitlab.com/emerald-works/corvus/commit/330cb2a23f426716b2eb862df90a0d7b4f257b74))
* trying wildcard permissions ([3dba3bf](https://gitlab.com/emerald-works/corvus/commit/3dba3bf4ff10271a58328840fa0cd4224c19e985))
* using ondemand for counter dynamodb table ([c305400](https://gitlab.com/emerald-works/corvus/commit/c305400ba5713edd5b79ead6e16129f5032ea3f3))


### Features

* dynamoose model builder and making a true offline experience ([8109081](https://gitlab.com/emerald-works/corvus/commit/81090818b1d82ea6f61c33b7f3830dd0424ad7fe))
* fixing serverless-dotenv-plugin and normalising serverless yml variables ([d94d9de](https://gitlab.com/emerald-works/corvus/commit/d94d9def8553d5fbdbc43ce8137c3cdd0e211b4f))
* using npm instead of yarn ([6a9525c](https://gitlab.com/emerald-works/corvus/commit/6a9525cd4ebe6a0573cc57fc90c2cd07469cd229))
