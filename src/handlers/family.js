import eventBus from '@emerald-works/middleware-event-bus'
import { getUser } from '../queries/user'
import { getPatientFamily as getPatientFamilyFromQueries } from '../queries/family'
import { getPatient } from '../queries/patient'
import { FamilyModel } from '../models/family'

export const getPatientFamily = eventBus(async (event, context) => {
  const {
    body: { patient: patientPk }
  } = event
  const family = await getPatientFamilyFromQueries({ patientPk })
  return family
})

export const addToFamily = eventBus(async (event, context) => {
  const {
    body: { patient: patientPk, family }
  } = event
  const user = await getUser({ pk: family })
  const patient = await getPatient({ pk: patientPk })
  if (user && patient) {
    return await FamilyModel.create({ patient: patientPk, family })
  }
})

export const removeFromFamily = eventBus(async (event, context) => {
  const {
    body: { patient: patientPk, family }
  } = event
  return await (await FamilyModel.get({ patient: patientPk, family })).delete()
})
