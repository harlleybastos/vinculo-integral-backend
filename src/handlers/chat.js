import eventBus from '@emerald-works/middleware-event-bus'
import { ChatModel } from '../models/chat'
import { broadcastMessage } from '../services/SubscriptionService'
import { getPatientUserAndFamily, getUserFromEvent } from '../queries/user'
import { sortMessages } from '../util/messages'
import { SortOrder } from 'dynamoose-latest/dist/General'
import { getTenantKey } from '../util/tenant'
import { getMembers } from '../queries/group'

export const getMessages = eventBus(async (event, context) => {
  const tenantKey = getTenantKey(context)
  const {
    body: { patient, sk }
  } = event
  const startAt = sk ? { tenantKey, sk } : null
  const messages = (
    await ChatModel.query({ tenantKey, patient })
      .sort(SortOrder.descending)
      .limit(10)
      .startAt(startAt)
      .exec()
  ).sort(sortMessages)
  return { messages, patient }
})

export const sendMessage = eventBus(async (event, context) => {
  const tenantKey = getTenantKey(context)
  const {
    body: { patient, text, time }
  } = event
  const sender = (await getUserFromEvent(event, context)).userName
  const sk = `${time}#${patient}#${sender}`
  if (patient && sender) {
    const message = await ChatModel.create({
      tenantKey,
      sk,
      time,
      patient,
      sender,
      text
    })
    await broadcastMessage({
      context,
      eventName: 'newMessage',
      contextId: patient,
      message
    })
    return message
  }
  throw new Error(
    'Message could not be sent: ' + JSON.stringify({ sender, patient })
  )
})

export const getChatMembers = eventBus(async (event, context) => {
  const {
    body: { patient: patientPk }
  } = event
  const patientUserAndFamily = await getPatientUserAndFamily({ patientPk })
  const members = await getMembers(patientPk)
  const chatMembers = [...patientUserAndFamily, ...members]
  return chatMembers
})
