import ArqSign from '../services/ArqSign'
import moment from 'moment'
import { generateAttendanceHtml } from '../util/pdf'
import chromium from '@sparticuz/chromium'
import { Upload } from '@aws-sdk/lib-storage'
import { GetObjectCommand, S3 } from '@aws-sdk/client-s3'
import puppeteer from 'puppeteer-core'
import { SignatureDocumentModel } from '../models/signature-document'
import { base } from '../util/basemiddleware'
import http from '@emerald-works/middleware-http'
import { getUser } from '../queries/user'
import {
  ARQSIGN_DEFAULT_USER,
  DOCS_BUCKET_NAME,
  ENVIRONMENT_NAME
} from '../util/consts'
import { getPatient } from '../queries/patient'
import eventBus from '@emerald-works/middleware-event-bus'
import { getTenantKey } from '../util/tenant'
import { getSignedUrl } from '@aws-sdk/s3-request-presigner'
import { getTenant } from '../queries/tenant'
import { TenantModel } from '../models/tenant'

const S3Client = new S3()

export const create = async (event, { source = 'vi', tenantKey }) => {
  const {
    attendanceBegin,
    attendanceEnd,
    therapistName,
    supervisorName,
    patientName,
    therapistEmail,
    supervisorEmail,
    notes,
    therapistCrp,
    supervisorCrp
  } = event

  const html = generateAttendanceHtml({
    attendanceBegin,
    attendanceEnd,
    therapistName,
    supervisorName,
    patientName,
    notes,
    therapistCrp,
    supervisorCrp
  })

  const options = {
    type: 'pdf',
    format: 'A4',
    orientation: 'portrait',
    printBackground: true
  }

  const config = process.env.IS_LOCAL
    ? {
      headless: 'new',
      executablePath:
          '/Applications/Google Chrome.app/Contents/MacOS/Google Chrome'
    }
    : {
      args: chromium.args,
      defaultViewport: chromium.defaultViewport,
      executablePath: await chromium.executablePath(),
      headless: 'new',
      ignoreHTTPSErrors: true
    }

  const browser = await puppeteer.launch(config)
  const page = await browser.newPage()
  await page.setContent(html)
  const pdf = await page.pdf(options)
  await browser.close()
  const date = moment(attendanceBegin).format('YYYY-MM-DD-HH-mm')

  const fileName = `${date}-${patientName}-${therapistName}${
    supervisorName ? '-' + supervisorName : ''
  }.pdf`

  const Key = `${tenantKey}/${fileName}`
  const S3Client = new S3()
  await new Upload({
    client: S3Client,
    params: { Bucket: DOCS_BUCKET_NAME, Key, Body: pdf }
  }).done()
  const docInfo = { tenantKey, key: Key, source }
  const alreadySubmitedDoc = await SignatureDocumentModel.get(docInfo)
  if (alreadySubmitedDoc) {
    if (alreadySubmitedDoc?.arqsignData?.idProcesso) {
      const res = await ArqSign.get(alreadySubmitedDoc?.arqsignData?.idProcesso)
      return {
        message: 'This document was already submitted.',
        response: res.data
      }
    }
    return {
      message:
        'This document was already submitted however we could not get info about it.'
    }
  }
  const tenant = await getTenant(tenantKey)
  const doc = await SignatureDocumentModel.create(docInfo)
  const data = {
    idResponsavel: ARQSIGN_DEFAULT_USER,
    nomeProcesso: fileName,
    idPasta: tenant?.arqsignFolderId,
    usarOrdemAssinatura: true,
    usarPosicaoAssinaturaAutomatica: true,
    destinatarios: [
      {
        anexos: [],
        ordemAssinatura: 1,
        nome: therapistName,
        idTipoAcao: 1,
        idFormaEnvio: 1,
        email: therapistEmail,
        telefone: null,
        assinarOnline: {
          assinarComo: 1,
          idTipoAssinatura: 1,
          assinaturaEletronica: {
            tipoDocumentoAInformar: 1,
            obrigarSignatarioInformarNome: false,
            obrigarSignatarioInformarNumeroDocumento: true
          },
          seguranca: {
            idMeioEnvio: 1
          }
        }
      }
    ],
    documentos: [
      {
        nomeComExtensao: fileName,
        arquivo: pdf.toString('base64'),
        ordemDocumento: 1
      }
    ],
    renovacaoMeses: 0,
    assinarOrdemDestinatarios: true,
    obrigarLeituraDocumentos: true
  }
  if (supervisorName) {
    data.destinatarios.push({
      anexos: [],
      ordemAssinatura: 1,
      nome: supervisorName,
      idTipoAcao: 1,
      idFormaEnvio: 1,
      email: supervisorEmail,
      telefone: null,
      assinarOnline: {
        assinarComo: 1,
        idTipoAssinatura: 1,
        assinaturaEletronica: {
          tipoDocumentoAInformar: 1,
          obrigarSignatarioInformarNome: false,
          obrigarSignatarioInformarNumeroDocumento: true
        },
        seguranca: {
          idMeioEnvio: 1
        }
      }
    })
  }
  const res = await ArqSign.post('/enviar-documento-para-assinar', data)
  await SignatureDocumentModel.update(doc, { arqsignData: res.data })
  return res.data
}

export const submitHttp = http(
  async ({ body, pathParameters }) =>
    await create(body, { source: 'api', tenantKey: pathParameters.tenantKey })
)

export const submit = base(async ({ Records }) => {
  const [{ body }] = Records
  const attendance = JSON.parse(body)
  const { patient, doctor, start, end, notes, tenantKey } = attendance
  const therapist = await getUser({ pk: doctor })
  const patientUser = await getPatient({ pk: patient })
  const supervisor = {}
  if (ENVIRONMENT_NAME === 'live') {
    supervisor.name = 'Michelle Procópio'
    supervisor.email = 'sender@michelleprocopio.com.br'
    supervisor.crp = 'CRP 01/12311'
  }

  const docInfo = {
    attendanceBegin: start,
    attendanceEnd: end,
    therapistName: therapist.name,
    supervisorName: supervisor.name,
    patientName: patientUser.name,
    therapistEmail: therapist.email,
    supervisorEmail: supervisor.email,
    notes,
    therapistCrp: therapist.crp,
    supervisorCrp: supervisor.crp
  }

  await create(docInfo, { source: 'vi', tenantKey })
})

export const get = eventBus(async (event, context) => {
  const tenantKey = getTenantKey(context)
  const start = event.body?.start
    ? event.body?.start
    : moment().startOf('month').valueOf()
  const end = event.body?.end
    ? event.body?.end
    : moment().endOf('month').valueOf()
  const signatureDocuments = await Promise.all(
    (
      await SignatureDocumentModel.query({ tenantKey })
        .where('createdAt')
        .ge(start)
        .where('createdAt')
        .le(end)
        .all()
        .exec()
    ).map(async (doc) =>
      doc.signedKey
        ? {
          ...doc,
          signedUrl: await getSignedUrl(
            S3Client,
            new GetObjectCommand({
              Key: doc.signedKey,
              Bucket: DOCS_BUCKET_NAME
            })
          )
        }
        : doc
    )
  )
  return signatureDocuments
})

export const update = async () => {
  const tenants = await TenantModel.scan().all().exec()
  for (const { tenantKey } of tenants) {
    const pendingDocuments = await SignatureDocumentModel.query({
      tenantKey
    })
      .filter('status')
      .eq('Criado')
      .all()
      .exec()
    for (const doc of pendingDocuments) {
      if (doc.arqsignData?.idProcesso) {
        const {
          arqsignData: { idProcesso }
        } = doc
        const res = await ArqSign.get(`/${idProcesso}`).catch((res) => res)
        if (res.data) {
          const {
            data: { status }
          } = res
          if (status === 'Concluído') {
            const {
              data: { dataConclusao, documentos }
            } = res
            const [{ nomeDocumento, base64Documento }] = documentos
            const signedKey = `${tenantKey}/signed_${nomeDocumento}`
            await S3Client.putObject({
              Key: signedKey,
              Bucket: DOCS_BUCKET_NAME,
              Body: Buffer.from(base64Documento, 'base64')
            })
            await SignatureDocumentModel.update(doc, {
              status: 'Concluído',
              finishedAt:
                new Date(dataConclusao).valueOf() - 3 * 60 * 60 * 1000, // to GMT-3
              signedKey
            })
          }
        }
      }
    }
  }
}
