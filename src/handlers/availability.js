import eventBus from '@emerald-works/middleware-event-bus'
import { getUser, getUserFromEvent } from '../queries/user'
import { AvailabilityModel } from '../models/availability'
import moment from 'moment'
import { applyDateCondition } from '../util/dynamodb/date'
import { MeetingModel } from '../models/meeting'
import { getTenantKey } from '../util/tenant'
import uuid from 'uuid-random'

export const upsertAvailability = eventBus(async (event, context) => {
  const {
    body: { startDate, endDate, frequency, sk }
  } = event

  if (startDate > endDate) {
    throw new Error('A data final precisa ser maior que a inicial')
  }
  const user = await getUserFromEvent(event)
  const tenantKey = await getTenantKey(context)
  const availability = sk
    ? await AvailabilityModel.get({
      tenantKey,
      sk
    })
    : null
  if (availability) {
    return await AvailabilityModel.update(availability, {
      startDate,
      endDate,
      frequency
    })
  }
  return await AvailabilityModel.create({
    tenantKey,
    sk: uuid(),
    supervisor: user.pk,
    startDate,
    endDate,
    frequency,
    year: moment(startDate).year(),
    month: moment(startDate).month()
  })
})

export const removeAvailability = eventBus(async (event, context) => {
  const {
    body: { sk }
  } = event
  const user = await getUserFromEvent(event)
  const tenantKey = getTenantKey(context)
  const availability = await AvailabilityModel.get({ tenantKey, sk })
  if (availability.supervisor === user.pk) {
    return await AvailabilityModel.delete({
      tenantKey,
      sk
    })
  }
})

export const getAvailability = eventBus(async (event, context) => {
  const {
    body: { year, month }
  } = event
  const tenantKey = getTenantKey(context)
  const availability = (
    await Promise.all(
      (
        await applyDateCondition(
          AvailabilityModel.query('tenantKey').eq(tenantKey),
          { year, month }
        ).exec()
      ).map(async (availability) => {
        const { frequency } = availability
        let times =
          frequency === 'daily'
            ? 30
            : frequency === 'weekly'
              ? 4
              : frequency === 'monthly'
                ? 2
                : 1
        const unit =
          frequency === 'weekly'
            ? 'weeks'
            : frequency === 'monthly'
              ? 'months'
              : 'days'
        let filter = true
        let startDate = availability.startDate
        let endDate = availability.endDate
        while (times--) {
          const compareDate = moment(availability.startDate) // check if recurrence events has future meetings
            .add(times, unit)
            .valueOf()
          const meeting = (
            await MeetingModel.query('host')
              .eq(availability.supervisor)
              .using('hostIndex')
              .filter('startDate')
              .eq(compareDate)
              .exec()
          ).toJSON()
          if (!meeting.length || !filter) {
            filter = false
          }
          if (!meeting.length) {
            startDate = compareDate
            endDate = moment(availability.endDate).add(times, unit).valueOf()
          }
        }
        if (filter) {
          return { ...availability, filter }
        }
        const supervisor = await getUser({ pk: availability.supervisor })
        return { ...availability, startDate, endDate, supervisor }
      })
    )
  ).filter(({ filter }) => !filter)
  return availability
})
