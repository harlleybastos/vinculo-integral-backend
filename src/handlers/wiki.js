import { GetParameterCommand, SSMClient } from '@aws-sdk/client-ssm'
import eventBus from '@emerald-works/middleware-event-bus'
import { getSignedCookies } from 'aws-cloudfront-sign'

const domain = '.vinculointegral.com.br'

function getExpirationTime () {
  return new Date(new Date().valueOf() + 12 * 1000 * 60 * 60)
}

const loadParameter = async (Name, WithDecryption = false) => {
  const { Parameter } = await new SSMClient().send(
    new GetParameterCommand({ Name, WithDecryption: true })
  )
  return Parameter.Value
}

const CLOUDFRONT_WIKI_AUTHORIZATION_PRIVATE_KEY =
  process.env.CLOUDFRONT_WIKI_AUTHORIZATION_PRIVATE_KEY

export const getWikiCookies = eventBus(async (event, context) => {
  const cloudFrontUrl = 'https://wiki.vinculointegral.com.br/*'
  const signingParams = {
    keypairId:
      process.env.CLOUDFRONT_WIKI_KEY_PAIR_ID ||
      (await loadParameter('CLOUDFRONT_WIKI_KEY_PAIR_ID')),
    privateKeyString:
      CLOUDFRONT_WIKI_AUTHORIZATION_PRIVATE_KEY ||
      (await loadParameter('CLOUDFRONT_WIKI_AUTHORIZATION_PRIVATE_KEY'))
  }

  if (!signingParams.keypairId || !signingParams.privateKeyString) {
    return []
  }

  const signedCookies = getSignedCookies(cloudFrontUrl, signingParams)
  return Object.keys(signedCookies).map(
    (key) =>
      `${key}=${
        signedCookies[key]
      };Domain=${domain};Path=/;Expires='${getExpirationTime().toUTCString()}';SecureHttpOnlySameSite=Lax`
  )
})
