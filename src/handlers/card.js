import eventBus from '@emerald-works/middleware-event-bus'
import { getTenantKey } from '../util/tenant'
import { CardModel } from '../models/card'
import { Safe2PayToken } from '../services/Safe2Pay'
import { createCard, tokenizeCreditCard } from '../queries/card'

export const getCreditCards = eventBus(async (event, context) => {
  const tenantKey = getTenantKey(context)
  const cards = await CardModel.query({ tenantKey }).all().exec()
  return cards
})

export const addCreditCard = eventBus(async (event, context) => {
  const {
    body: {
      holder: Holder,
      cardNumber: CardNumber,
      expirationDate: ExpirationDate,
      securityCode: SecurityCode
    }
  } = event
  const tenantKey = getTenantKey(context)

  const res = await tokenizeCreditCard({
    CardNumber,
    ExpirationDate,
    Holder,
    SecurityCode
  })
  await createCard({
    tenantKey,
    ...res.ResponseDetail
  })
})

export const removeCreditCard = eventBus(async (event, context) => {
  const tenantKey = getTenantKey(context)
  const {
    body: { Token }
  } = event
  await CardModel.delete({ tenantKey, Token })
  await Safe2PayToken.delete(`/token/delete?cardToken=${Token}`).catch((e) => e)
})
