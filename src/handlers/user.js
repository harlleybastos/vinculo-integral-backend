import eventBus from '@emerald-works/middleware-event-bus'
import {
  queryUsers,
  getUserFromEvent,
  getUser as queryUser
} from '../queries/user'
import { UserModel } from '../models/user'
import { addUserToGroup, removeUserFromGroup } from '../services/Cognito'
import { TenantModel } from '../models/tenant'
import { getTenantKey } from '../util/tenant'
import { applyFixToName } from '../util/name'
import { isEmailValid } from '../util/email'

export const getUser = eventBus(async (event, context) => {
  try {
    const user = await getUserFromEvent(event, context)
    return user
  } catch (e) {
    console.error(`User not found: ${JSON.stringify(e)}`)
  }
})

export const getUsers = eventBus(async (event, context) => {
  const tenantKey = getTenantKey(context)
  const {
    body: { limit, startAt, search, searchBy, role }
  } = event
  const users = await queryUsers({
    tenantKey,
    limit,
    startAt,
    search,
    searchBy,
    role
  })
  return users
})

export const getDoctors = eventBus(async (event, context) => {
  const tenantKey = getTenantKey(context)
  const {
    body: { limit, startAt, search, searchBy }
  } = event
  const doctors = await queryUsers({
    tenantKey,
    limit,
    startAt,
    search,
    searchBy,
    role: 'doctor'
  })
  return doctors
})

export const getUserByEmail = eventBus(async (event, context) => {
  const tenantKey = getTenantKey(context)
  const {
    body: { email }
  } = event
  return await queryUser({ tenantKey, email })
})

export const getUserByPk = eventBus(async (event, context) => {
  const {
    body: { pk }
  } = event
  return await queryUser({ pk })
})

export const updateUser = eventBus(async (event, context) => {
  const tenantKey = getTenantKey(context)
  const {
    body: { email, role, patientDoc, name, tags, isSupervisor }
  } = event
  const user = await queryUser({ tenantKey, email })

  const update = {}

  if (patientDoc) {
    update.patientDoc = patientDoc
  }
  if (isSupervisor !== undefined && isSupervisor !== null) {
    update.isSupervisor = isSupervisor
  }
  if (name) {
    update.name = applyFixToName(name)
  }
  if (role) {
    update.role = role
    if (role !== 'family' && role !== 'patient') {
      update.patientDoc = undefined
    }
  }
  if ((isSupervisor || user.isSupervisor) && tags) {
    update.tags = tags
  }
  await handleUserGroups({ user, role, tenantKey })

  return await UserModel.update(user, update)
})

const handleUserGroups = async ({ user, role, tenantKey }) => {
  if (user.userName) {
    const [{ userPoolId }] = await TenantModel.query({ tenantKey })
      .limit(1)
      .exec()

    const groupsToRemove = []
    const groupsToAdd = []

    switch (role) {
      case 'owner':
        groupsToRemove.push('doctor')
        groupsToRemove.push('admin')
        groupsToAdd.push('owner')
        break
      case 'admin':
        groupsToRemove.push('doctor')
        groupsToRemove.push('owner')
        groupsToAdd.push('admin')
        break
      case 'doctor':
        groupsToRemove.push('admin')
        groupsToRemove.push('owner')
        groupsToAdd.push('doctor')
        break
      default:
        groupsToRemove.push('admin')
        groupsToRemove.push('doctor')
        groupsToRemove.push('owner')
    }

    for (const groupName of groupsToRemove) {
      await removeUserFromGroup({
        groupName: groupName,
        userPoolId,
        userName: user.userName
      })
    }

    for (const groupName of groupsToAdd) {
      await addUserToGroup({
        groupName: groupName,
        userPoolId,
        userName: user.userName
      })
    }
  }
}

export const createUser = eventBus(async (event, context) => {
  const tenantKey = getTenantKey(context)
  const {
    body: { role, patientDoc, name, isSupervisor, tags, ...body }
  } = event

  const email = body.email.replace(/\s/g, '')

  const pk = `${tenantKey}#${email}`

  const user = {
    tenantKey,
    pk,
    role,
    email,
    isSupervisor,
    tags: isSupervisor ? tags : [],
    name: applyFixToName(name)
  }

  if (role === 'patient') {
    user.patientDoc = patientDoc
  }

  return await UserModel.create(user)
})

export const acceptTerms = eventBus(async (event, context) => {
  const user = await getUserFromEvent(event, context)
  return UserModel.update(user, { hasAcceptedTerms: true })
})

export const getSupervisors = eventBus(async (event, context) => {
  const tenantKey = getTenantKey(context)
  const {
    body: { limit, startAt, search, searchBy }
  } = event
  const supervisors = await queryUsers({
    tenantKey,
    limit,
    startAt,
    search,
    searchBy,
    role: 'doctor',
    isSupervisor: true
  })
  return supervisors
})

export const validateUsersList = eventBus(async (event, context) => {
  const tenantKey = getTenantKey(context)
  const {
    body: { users }
  } = event
  const errors = []
  const validRoles = ['patient', 'doctor', 'family']
  for (const user of users) {
    const { email, name, role } = user
    if (!name) {
      errors.push({ user, error: 'Nome inválido' })
      continue
    }
    if (validRoles.indexOf(role) < 0) {
      errors.push({ user, error: 'Perfil inválido' })
      continue
    }
    if (!email || !isEmailValid(email)) {
      errors.push({ user, error: 'E-mail inválido' })
      continue
    }
    const userAlreadyExists = await queryUser({ email, tenantKey })
    if (userAlreadyExists) {
      errors.push({ user, error: 'Já existe um usuário com este e-mail' })
      continue
    }
  }
  return errors
})
