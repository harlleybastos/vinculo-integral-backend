import eventBus from '@emerald-works/middleware-event-bus'
import { getUser, getUserFromEvent } from '../queries/user'
import { AttendanceModel } from '../models/attendance'
import { SortOrder } from 'dynamoose-latest/dist/General'
import {
  ENVIRONMENT_NAME,
  RECORDINGS_BUCKET_NAME,
  SAVE_ATTENDANCE_RECORDING_FIRST_SLICE_QUEUE_NAME,
  SUBMIT_DOCUMENT_QUEUE_NAME
} from '../util/consts'
import { getPatient } from '../queries/patient'
import {
  Acquire,
  GenerateRtmToken,
  GenerateToken,
  QueryRecording,
  StartRecording,
  StopRecording
} from '../services/Agora'
import { getSignedUrl } from '@aws-sdk/s3-request-presigner'
import { GetObjectCommand, S3 } from '@aws-sdk/client-s3'
import { sendSQSMessage } from '../util/sqs'
import { getTenantKey } from '../util/tenant'
import uuid from 'uuid-random'
import { AttendanceCommentModel } from '../models/attendance-comment'
import { broadcastMessage } from '../services/SubscriptionService'
import sleep from 'sleep-promise'
import { base } from '../util/basemiddleware'

export const create = eventBus(async (event, context) => {
  const {
    body: { patient }
  } = event
  const [inProgress] = await AttendanceModel.query({
    patient,
    hasFinished: false
  }).exec()
  if (inProgress) {
    return 'Já existe um atendimento em andamento para este paciente.'
  }
  const { tenantKey, email } = await getUserFromEvent(event, context)
  const { doc } = await getPatient({ pk: patient })
  const attendance = await AttendanceModel.create({
    pk: uuid(),
    start: new Date().valueOf(),
    patient,
    doctor: `${tenantKey}#${email}`,
    token: GenerateToken({
      channel: doc
    }),
    tenantKey: getTenantKey(context)
  })

  return attendance
})

export const update = eventBus(async (event, context) => {
  const {
    body: { patient, start, notes }
  } = event
  const attendance = await AttendanceModel.update({ patient, start }, { notes })
  return attendance
})

const finishAttendance = async (event, context) => {
  const {
    body: { patient, start, end }
  } = event
  const [attendance] = await AttendanceModel.query({ start, patient }).exec()
  const updatedAttendance = await AttendanceModel.update(attendance, {
    end: end || new Date().valueOf(),
    hasFinished: true
  })

  if (
    attendance.uid &&
    attendance.sid &&
    attendance.resourceId &&
    attendance.patient
  ) {
    const patient = await getPatient({ pk: attendance.patient })
    await StopRecording({ channel: patient.doc, ...attendance })
  }

  await sendSQSMessage(SUBMIT_DOCUMENT_QUEUE_NAME, {
    ...attendance.toJSON(),
    tenantKey: getTenantKey(context)
  })

  return updatedAttendance
}

export const finish = eventBus(finishAttendance)

export const getAttendances = eventBus(async (event, context) => {
  const {
    body: { patient, lastKey, limit }
  } = event
  const attendances = await AttendanceModel.query({
    patient
  })
    .startAt(lastKey)
    .limit(limit || 5)
    .sort(SortOrder.descending)
    .exec()
  const attendancesWithDoctor = await Promise.all(
    attendances.map(async (attendance) => ({
      ...attendance,
      doctor: await getUser({ pk: attendance.doctor })
    }))
  )
  return attendancesWithDoctor
})

export const getAllAttendances = eventBus(async (event, context) => {
  const {
    body: { patient, hasFinished }
  } = event
  let query = AttendanceModel.query({
    patient
  }).sort(SortOrder.descending)
  if (hasFinished !== undefined) {
    query = query.where('hasFinished').eq(hasFinished)
  }
  const attendances = await query.all().exec()
  return await Promise.all(
    attendances.map(async (att) => ({
      ...att,
      doctor: await getUser({ pk: att.doctor })
    }))
  )
})

export const exportAttendance = eventBus(async (event, context) => {
  const {
    body: { patient, start }
  } = event

  const [attendance] = await AttendanceModel.query({
    patient,
    start
  }).exec()
  const updatedAttendance = await AttendanceModel.update(attendance, {
    hasExported: true
  })
  return {
    ...updatedAttendance,
    doctor: await getUser({ pk: updatedAttendance.doctor })
  }
})

export const getInProgressAttendance = eventBus(async (event, context) => {
  const {
    body: { patient }
  } = event
  const attendances = await AttendanceModel.query({ patient })
    .filter('hasFinished')
    .eq(false)
    .all()
    .exec()
  if (attendances.length) {
    const [attendance] = attendances
    return { ...attendance, doctor: await getUser({ pk: attendance.doctor }) }
  }
  return null
})

export const recordAttendance = eventBus(async (event, context) => {
  const {
    body: { patient, start }
  } = event
  const [attendance] = await AttendanceModel.query({ patient, start }).exec()
  if (!attendance) {
    return { success: false, error: 'Atendimento não encontrado.' }
  }
  if (attendance.sid && attendance.resourceId) {
    const response = await QueryRecording(attendance)
    if (response.code !== 404 && response.code !== 2) {
      // Recording has been found and resourceId is valid
      return { success: true }
    }
  }
  const user = await getUserFromEvent(event, context)
  if (user.pk !== attendance.doctor) {
    return { success: false }
  }
  const { doc, tenantKey } = await getPatient({ pk: patient })
  const { resourceId, uid } = await Acquire({
    channel: doc,
    token: attendance.token
  })
  try {
    // Generate a new token
    const updatedAttendance = await AttendanceModel.update(attendance, {
      token: GenerateToken({
        channel: doc
      })
    })
    const response = await StartRecording({
      channel: doc,
      resourceId,
      uid,
      prefix: [
        ENVIRONMENT_NAME,
        tenantKey,
        doc,
        new Date(start).valueOf().toString()
      ],
      token: updatedAttendance.token
    })
    const queryResponse = await QueryRecording({
      ...attendance,
      sid: response.sid,
      resourceId
    })
    const isRecording =
      queryResponse?.serverResponse?.status > 0 &&
      queryResponse?.serverResponse?.status < 6
    if (!isRecording) {
      return { success: false }
    }
    await AttendanceModel.update(attendance, {
      resourceId,
      sid: response.sid,
      uid
    })
    await sendSQSMessage(
      SAVE_ATTENDANCE_RECORDING_FIRST_SLICE_QUEUE_NAME,
      attendance.toJSON()
    )
    return { success: true }
  } catch (e) {
    return { success: false, error: e.toString() }
  }
})

const S3Client = new S3({
  region: 'us-east-1'
})

export const getAttendanceRecordings = eventBus(async (event, context) => {
  const {
    body: { patient, start }
  } = event
  const [attendance] = await AttendanceModel.query({
    patient,
    start
  }).exec()

  const { doc, tenantKey } = await getPatient({ pk: patient })

  const Prefix = `${ENVIRONMENT_NAME}/${tenantKey}/${doc}/${new Date(
    attendance.start
  ).valueOf()}`

  const { Contents = [] } = await S3Client.listObjects({
    Bucket: RECORDINGS_BUCKET_NAME,
    Prefix
  })

  const recordings = await Promise.all(
    Contents.filter(
      ({ Key }) =>
        Key.indexOf('.mp4') > 0 ||
        (Key.indexOf('CHUNK') >= 0 && Key.indexOf('.webm') > 0)
    )
      .sort(
        (
          { Key: KeyA, LastModified: LastModifiedA },
          { Key: KeyB, LastModified: LastModifiedB }
        ) => {
          if (KeyA.indexOf('VIDEO') >= 0) {
            return -1000000
          }
          if (KeyB.indexOf('VIDEO') >= 0) {
            return 1000000
          }
          if (KeyA.indexOf('CHUNK') >= 0 && KeyB.indexOf('CHUNK') >= 0) {
            const valueA = parseInt(KeyA.match(/_(\d+)\./)[1])
            const valueB = parseInt(KeyB.match(/_(\d+)\./)[1])
            return valueA - valueB
          }
          return LastModifiedA.valueOf() - LastModifiedB.valueOf()
        }
      )
      .map(async ({ Key }) => ({
        url: await getSignedUrl(
          S3Client,

          new GetObjectCommand({
            Key,
            Bucket: RECORDINGS_BUCKET_NAME
          }),
          { expiresIn: 5 * 60 * 60 } // 5 hours
        )
      }))
  )
  const user = await getUserFromEvent(event, context)
  const isAllowedToSeeComments =
    user.role === 'admin' || user.role === 'owner' || user.role === 'doctor'
  if (isAllowedToSeeComments && attendance?.slices?.length) {
    for (let i = 0; i < attendance?.slices?.length; i++) {
      recordings[i].comments = await Promise.all(
        (
          await AttendanceCommentModel.query({
            attendance: `${attendance.patient}#${attendance.start}`
          })
            .filter('slice')
            .eq(attendance?.slices[i])
            .all()
            .exec()
        )
          .sort(({ createdAt: a }, { createdAt: b }) => a - b)
          .map(async (att) => ({
            ...att,
            commenter: await getUser({ pk: att.commenter }, ['name', 'pk'])
          }))
      )
      recordings[i].slice = attendance?.slices[i]
    }
  }
  return recordings
})

export const getDoctorInProgressAttendance = eventBus(
  async (event, context) => {
    const user = await getUserFromEvent(event)
    const inProgressAttendances = await AttendanceModel.query({
      doctor: user.pk
    })
      .using('doctorInProgressAttendance')
      .filter('hasFinished')
      .eq(false)
      .all()
      .exec()
    if (inProgressAttendances.length) {
      const [attendance] = inProgressAttendances
      const patient = await getPatient({ pk: attendance.patient })
      return { ...attendance, patient, doctor: user }
    }
  }
)

export const generateAttendanceRtmToken = eventBus(async (event, context) => {
  const {
    body: { patient }
  } = event
  const { pk } = await getUserFromEvent(event, context)
  const { doc } = await getPatient({ pk: patient })
  return GenerateRtmToken({ channel: doc, pk })
})

export const addAttendanceComment = eventBus(async (event, context) => {
  const {
    body: { patient, start, comment }
  } = event
  const [attendance] = await AttendanceModel.query({ start, patient }).exec()
  do {
    var { serverResponse } = await QueryRecording(attendance)
    if (serverResponse?.sliceStartTime) {
      break
    }
    await sleep(2000)
  } while (!serverResponse?.sliceStartTime)
  const { pk } = await getUserFromEvent(event, context)
  const attendancePk = `${patient}#${start}`

  let moment = new Date().valueOf()
  while (
    await AttendanceCommentModel.get({ attendance: attendancePk, moment })
  ) {
    moment++
  }

  const attendanceComment = await AttendanceCommentModel.create({
    attendance: attendancePk,
    moment,
    comment,
    slice: serverResponse?.sliceStartTime,
    commenter: pk
  })
  attendanceComment.commenter = await getUser({ pk }, ['name', 'pk'])
  await broadcastMessage({
    context,
    eventName: 'newAttendanceComment',
    contextId: attendancePk,
    message: attendanceComment,
    excludeOrigin: false
  })
})

export const getAttendanceComments = eventBus(async (event, context) => {
  const {
    body: { patient, start }
  } = event
  const comments = await Promise.all(
    (
      await AttendanceCommentModel.query({ attendance: `${patient}#${start}` })
        .all()
        .sort(SortOrder.descending)
        .exec()
    ).map(async (comment) => ({
      ...comment,
      commenter: await getUser({ pk: comment.commenter }, ['name', 'pk'])
    }))
  )
  return comments
})

export const addAttendanceRecordingComment = eventBus(
  async (event, context) => {
    const {
      body: { patient, start, comment, moment, slice }
    } = event
    const { pk } = await getUserFromEvent(event, context)
    const attendancePk = `${patient}#${start}`
    let slicePlusMoment = (slice !== undefined ? slice : 0) + moment
    while (
      await AttendanceCommentModel.get({
        attendance: attendancePk,
        moment: slicePlusMoment
      })
    ) {
      slicePlusMoment++
    }
    const attendanceComment = await AttendanceCommentModel.create({
      attendance: attendancePk,
      moment: slicePlusMoment,
      comment,
      slice,
      commenter: pk
    })
    attendanceComment.commenter = await getUser({ pk }, ['name', 'pk'])
    return attendanceComment
  }
)

export const saveAttendanceRecordingFirstSlice = base(async ({ Records }) => {
  for (const { body } of Records) {
    const { patient, start } = JSON.parse(body)
    const attendance = await AttendanceModel.get({ patient, start })
    let retries = 5
    do {
      var query = await QueryRecording(attendance)
      await sleep(5000)
    } while (!query?.serverResponse?.sliceStartTime && retries--)
  }
})
