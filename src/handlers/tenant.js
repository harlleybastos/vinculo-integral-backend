import http from '@emerald-works/middleware-http'
import httpErrors from 'http-errors'
import { TenantModel } from '../models/tenant'
import { checkIfTenantIsAvailable } from '../queries/tenant'
import { Safe2PayServices } from '../services/Safe2Pay'
import eventBus from '@emerald-works/middleware-event-bus'
import { getTenantKey } from '../util/tenant'

const { ENVIRONMENT_NAME } = process.env

export const getInfo = http(
  async ({ queryStringParameters, pathParameters }) => {
    const tenantKey =
      queryStringParameters && queryStringParameters.tenantKey
        ? queryStringParameters.tenantKey
        : pathParameters.id
    const result = await getTenantInfo({
      queryStringParameters,
      pathParameters
    })
    if (
      tenantKey === 'vi' ||
      tenantKey === 'clifali' ||
      ENVIRONMENT_NAME !== 'live'
    ) {
      return result
    }
    const { data } = await Safe2PayServices.get(
      `Subscriptions/${result.idSubscription}`
    )

    if (data?.data?.statusName !== 'Ativa') {
      console.error(data.data)
      throw new httpErrors.Unauthorized('Tenant is not active')
    }
    return result
  }
)

const getTenantInfo = async ({ queryStringParameters, pathParameters }) => {
  const tenantKey =
    queryStringParameters && queryStringParameters.tenantKey
      ? queryStringParameters.tenantKey
      : pathParameters.id
  if (!tenantKey) {
    throw new httpErrors.BadRequest(
      'Tenant key is required. Either as a query string parameter (?tenantKey=www) or path parameter (/tenant-info/www)'
    )
  }
  const [result] = await TenantModel.query({ tenantKey }).limit(1).exec()
  if (!result) throw new httpErrors.NotFound('Tenant was not found')
  return result
}

export const adminGetInfo = http(getTenantInfo)

export const isTenantAvailable = http(async ({ queryStringParameters }) => {
  const tenantKey = queryStringParameters && queryStringParameters.tenantKey
  if (!tenantKey) throw new httpErrors.BadRequest('Tenant key is required.')
  return checkIfTenantIsAvailable(tenantKey)
})

export const getTenant = eventBus(async (event, context) => {
  const tenantKey = getTenantKey(context)
  const [result] = await TenantModel.query({ tenantKey }).limit(1).exec()
  if (!result) throw new httpErrors.NotFound('Tenant was not found')
  return result
})

export const updateTenant = eventBus(async (event, context) => {
  const tenantKey = getTenantKey(context)
  const {
    body: {
      name,
      identity,
      ownerEmails,
      email,
      city,
      district,
      street,
      number,
      complement,
      zipcode,
      ownerEmail
    }
  } = event

  return await TenantModel.update(
    { tenantKey },
    {
      name,
      identity,
      ownerEmails,
      email,
      city,
      district,
      street,
      number,
      complement,
      zipcode,
      ownerEmail
    }
  )
})
