import { CognitoIdentityProvider } from '@aws-sdk/client-cognito-identity-provider'
import { S3 } from '@aws-sdk/client-s3'
import { GroupModel } from '../models/group'
import { PatientModel } from '../models/patient'
import { TenantModel } from '../models/tenant'
import { UserModel } from '../models/user'
import { read, utils } from 'xlsx'
import { cpf } from 'cpf-cnpj-validator'
import { getPatient } from '../queries/patient'
import { fixCpfString } from '../util/cpf'
import { FamilyModel } from '../models/family'
import { getUser } from '../queries/user'
import { applyFixToName } from '../util/name'
import { fixEmailString } from '../util/email'

export const createFakeUsers = async () => {
  if (process.env.ENVIRONMENT_NAME === 'live') return
  const tenantKey = 'vi'
  const patientsNumber = 5
  const doctorsNumber = 3
  const familyNumber = 2
  const doctorsPerPatientNumber = 3

  for (let i = 1; i <= doctorsNumber; i++) {
    const email = `doctor-${i}@email.com`
    const name = `Doctor Fake ${i}`
    const pk = `${tenantKey}#${email}`
    await UserModel.create({
      tenantKey,
      pk,
      role: 'doctor',
      email,
      name: applyFixToName(name)
    })
  }

  for (let i = 1; i <= patientsNumber; i++) {
    const n = i <= 9 ? `0${i}` : i
    const doc = `${n}${n}${n}${n}${n}${n}`.slice(0, 11)
    const email = `fake-${n}@email.com`
    const name = `Fake ${n}`
    const pk = `${tenantKey}#${doc}`
    await PatientModel.create({
      tenantKey,
      doc,
      name: applyFixToName(name),
      pk
    })
    await UserModel.create({
      tenantKey,
      pk,
      role: 'patient',
      email,
      name: applyFixToName(name)
    })

    for (let j = 1; j <= doctorsPerPatientNumber; j++) {
      const randomId = Math.floor(Math.random() * doctorsNumber) + 1
      const group = {
        patient: pk,
        professional: `${tenantKey}#doctor-${randomId}@email.com`
      }
      try {
        await GroupModel.create(group)
      } catch (e) {}
    }

    for (let j = 1; j <= familyNumber; j++) {
      const familyEmail = `fake-family${j}-patient-${n}@email.com`
      const family = {
        tenantKey,
        patientDoc: doc,
        name: `Family ${j} Patient ${n}`,
        role: 'family',
        pk: `${tenantKey}#${familyEmail}`,
        email: familyEmail
      }
      await UserModel.create(family)
    }
  }
}

export const createUsersFromCognito = async () => {
  const getAttr = (attrs, key) => attrs.find(({ Name }) => Name === key)?.Value
  const cognito = new CognitoIdentityProvider()
  const tenants = await TenantModel.scan().all().exec()
  for (const tenant of tenants) {
    const { userPoolId: UserPoolId } = tenant
    const { Users } = await cognito.listUsers({ UserPoolId, Limit: 60 })
    for (const user of Users) {
      const { Attributes, Username } = user
      const { tenantKey } = tenant
      const email = getAttr(Attributes, 'email')
      await UserModel.create({
        tenantKey,
        name: email,
        userName: Username,
        email,
        pk: `${tenantKey}#${email}`,
        role: email.indexOf('corvus') >= 0 ? 'admin' : 'patient'
      })
    }
  }
}
const ImportUsersBucketName = 'vi-api-import-users'
const S3Client = new S3({
  // The key signatureVersion is no longer supported in v3, and can be removed.
  // @deprecated SDK v3 only supports signature v4.
  signatureVersion: 'v4',

  region: 'sa-east-1'
})
export const createPatientsFromXls = async (event, context) => {
  const { fileName, tenantKey } = event
  const file = await S3Client.getObject({
    Bucket: ImportUsersBucketName,
    Key: fileName
  })
  const xls = read(file.Body, { type: 'buffer' })
  const sheet = xls.Sheets[xls.SheetNames[0]]
  const patients = utils.sheet_to_json(sheet)
  const invalids = []
  for (const patient of patients) {
    if (cpf.isValid(patient.CPF)) {
      try {
        const { Nome: name, CPF } = patient
        const doc = fixCpfString(CPF)
        const pk = `${tenantKey}#${doc}`
        await PatientModel.create({
          tenantKey,
          name: applyFixToName(name),
          doc,
          pk
        })
      } catch (e) {
        invalids.push({ ...patient, error: e.toString() })
      }
    } else {
      invalids.push({ ...patient, error: `CPF Inválido: ${patient.CPF}` })
    }
  }
  return invalids
}

export const createUsersFromXls = async (event, context) => {
  const { fileName, role, tenantKey } = event
  const file = await S3Client.getObject({
    Bucket: ImportUsersBucketName,
    Key: fileName
  })
  const xls = read(file.Body, { type: 'buffer' })
  const sheet = xls.Sheets[xls.SheetNames[0]]
  const users = utils.sheet_to_json(sheet).map((user) => {
    if (user.Email) {
      user.Email = fixEmailString(user.Email)
    }
    return user
  })
  const invalids = []
  for (const user of users) {
    try {
      switch (role) {
        case 'doctor':
          await insertProfessional({ user, tenantKey })
          break
        case 'family':
        case 'patient':
          await insertPatientOrFamily({ user, tenantKey, role })
          break
      }
    } catch (e) {
      invalids.push({ ...user, error: e.toString() })
    }
  }
  return invalids
}

const insertProfessional = async ({ tenantKey, user }) => {
  const { Nome: name, 'CPF do Paciente Vinculado': CPF, Email: email } = user
  const pk = `${tenantKey}#${email}`
  const userInfo = {
    tenantKey,
    role: 'doctor',
    name: applyFixToName(name),
    email,
    pk
  }

  const doctor = await getUser({ pk })
  if (!doctor) {
    await UserModel.create(userInfo)
  } else {
    if (CPF) {
      console.log(
        `O terapeuta ${name} - ${email} já existe. No entanto, o paciente ${CPF} será vinculado a ele.`
      )
    }
  }

  const patientDoc = fixCpfString(CPF || '')
  if (cpf.isValid(patientDoc)) {
    const patientPk = `${tenantKey}#${patientDoc}`
    const patient = await getPatient({ pk: patientPk })
    if (!patient) {
      throw new Error(`Paciente vinculado não encontrado: ${patientDoc}`)
    }
    try {
      await GroupModel.create({ patient: patientPk, professional: pk })
    } catch (e) {
      throw new Error(
        `Falha ao adicionar ao grupo.\nPaciente: ${patientDoc} - Profissional: ${pk}`
      )
    }
  }
}
const insertPatientOrFamily = async ({ tenantKey, user, role }) => {
  const { Nome: name, 'CPF do Paciente Vinculado': CPF, Email: email } = user
  const patientDoc = fixCpfString(CPF || '')
  const patientPk = `${tenantKey}#${patientDoc}`
  if (patientDoc.length >= 11) {
    const patient = await getPatient({ pk: patientPk })
    if (!patient) {
      throw new Error(`Paciente vinculado não encontrado: ${patientDoc}`)
    }
  } else if (role === 'patient') {
    throw new Error(`Paciente vinculado não encontrado: ${patientDoc}`)
  }

  const pk = `${tenantKey}#${email}`

  const foundUser = await getUser({ pk })
  if (!foundUser) {
    await UserModel.create({
      tenantKey,
      role,
      name: applyFixToName(name),
      email,
      patientDoc: role === 'patient' ? patientDoc : undefined,
      pk
    })
  }

  await FamilyModel.create({ patient: patientPk, family: pk })
}

export const fixUserAndPatientNames = async ({ patientPk, userPk }) => {
  if (patientPk) {
    const patient = await getPatient({ pk: patientPk })
    if (patient) {
      return await PatientModel.update(patient, {
        name: applyFixToName(patient.name)
      })
    }
    return
  }
  if (userPk) {
    const user = await getUser({ pk: userPk })
    if (user) {
      return await UserModel.update(user, {
        name: applyFixToName(user.name)
      })
    }
    return
  }

  const patients = await PatientModel.scan().all().exec()
  for (const patient of patients) {
    await fixUserAndPatientNames({ patientPk: patient.pk })
  }
  const users = await UserModel.scan().all().exec()
  for (const user of users) {
    await fixUserAndPatientNames({ userPk: user.pk })
  }
}
