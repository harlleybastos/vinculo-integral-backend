import eventBus from '@emerald-works/middleware-event-bus'
import { getUser, getUserFromEvent } from '../queries/user'
import uuid from 'uuid-random'
import { MeetingModel } from '../models/meeting'
import moment from 'moment'
import { hour } from '../util/time'
import { GenerateRtmToken, GenerateToken } from '../services/Agora'
import { applyDateCondition } from '../util/dynamodb/date'
import { getTenantKey } from '../util/tenant'

export const create = eventBus(async (event, context) => {
  const {
    body: {
      startDate,
      endDate,
      title,
      frequency,
      participants = [],
      type = 'meeting',
      supervisor
    }
  } = event
  const startDateValue = new Date(startDate).valueOf()
  const endDateValue =
    startDate >= new Date(endDate).valueOf()
      ? startDateValue + hour
      : new Date(endDate).valueOf()
  const user = await getUserFromEvent(event, context)
  const tenantKey = getTenantKey(context)
  const meetingEvent = {
    pk: `${tenantKey}#meeting`,
    sk: uuid(),
    host:
      type === 'meeting' ? user.pk : type === 'mentoring' ? supervisor : null,
    title,
    frequency,
    year: moment(startDate).year(),
    month: moment(startDate).month(),
    startDate: startDateValue,
    endDate: endDateValue,
    type
  }

  if (type === 'mentoring' && user.pk !== supervisor) {
    participants.push(user)
  }

  const meeting = await MeetingModel.create(meetingEvent)
  for (const { pk } of participants) {
    await MeetingModel.create({ sk: meeting?.sk, pk, type })
  }
  return meeting
})

export const getMeetings = eventBus(async (event, context) => {
  const user = await getUserFromEvent(event, context)
  const {
    body: { month, year }
  } = event
  const invites = await MeetingModel.query({ pk: user.pk }).all().exec()
  const tenantKey = getTenantKey(context)
  const meetings = (
    await Promise.all(
      invites.map(
        async ({ sk }) =>
          (
            await applyDateCondition(
              MeetingModel.query('pk')
                .eq(`${tenantKey}#meeting`)
                .filter('sk')
                .eq(sk),
              {
                month,
                year
              }
            ).exec()
          )?.[0]
      )
    )
  ).filter((m) => m)
  const hostMeetings = await applyDateCondition(
    MeetingModel.query('host').eq(user.pk).using('hostIndex'),
    {
      month,
      year
    }
  ).exec()
  const allMeetings = await Promise.all(
    [...meetings, ...hostMeetings].map(async (meeting) =>
      meeting.type === 'mentoring'
        ? { ...meeting, supervisor: await getUser({ pk: meeting.host }) }
        : meeting
    )
  )
  return allMeetings
})

export const deleteMeeting = eventBus(async (event, context) => {
  const {
    body: { sk }
  } = event
  const tenantKey = getTenantKey(context)
  const meeting = await MeetingModel.get({ pk: `${tenantKey}#meeting`, sk })
  if (meeting) {
    const user = await getUserFromEvent(event, context)
    if (user.pk === meeting.host) {
      // remove invites
      await Promise.all(
        (
          await MeetingModel.query('sk')
            .eq(meeting.sk)
            .using('meetingSkIndex')
            .all()
            .exec()
        ).map((invite) => MeetingModel.delete(invite))
      )
      await MeetingModel.delete(meeting)
      return 'Reunião removida com sucesso.'
    } else {
      return 'Apenas o host pode remover uma reunião.'
    }
  } else {
    return 'Reunião não encontrada.'
  }
})

export const removeInvite = eventBus(async (event, context) => {
  const {
    body: { sk, userPk }
  } = event

  const invite = await MeetingModel.get({ pk: userPk, sk })
  if (invite) {
    await MeetingModel.delete(invite)
    return 'Convite removido com sucesso.'
  } else {
    throw new Error('Convite não encontrado.')
  }
})

export const sendInvite = eventBus(async (event, context) => {
  const {
    body: { sk, userPk }
  } = event
  return MeetingModel.create({ sk: sk, pk: userPk })
})

export const getMeeting = eventBus(async (event, context) => {
  const tenantKey = getTenantKey(context)
  const user = await getUserFromEvent(event, context)
  const {
    body: { sk }
  } = event
  const invite = await MeetingModel.get({ pk: user.pk, sk })
  const [host] = await MeetingModel.query({ host: user.pk, sk })
    .using('hostIndex')
    .exec()
  if (invite || host) {
    let meeting = await MeetingModel.get({ pk: `${tenantKey}#meeting`, sk })
    if (host) {
      meeting = await MeetingModel.update(meeting, {
        token: GenerateToken({ channel: sk })
      })
    }
    return meeting
  } else {
    return {
      statusCode: 500,
      error:
        'Você não convidado para esta reunião. Entre em contato com o host.'
    }
  }
})

export const getMeetingParticipants = eventBus(async (event, context) => {
  const {
    body: { sk }
  } = event
  const tenantKey = getTenantKey(context)
  const meeting = await MeetingModel.get({ pk: `${tenantKey}#meeting`, sk })
  const participants = await Promise.all(
    [
      { pk: meeting.host },
      ...(
        await MeetingModel.query({ sk }).using('meetingSkIndex').all().exec()
      ).filter(({ pk }) => pk !== `${tenantKey}#meeting`)
    ].map(async ({ pk }) => await getUser({ pk }, ['name', 'pk', 'email']))
  )
  return participants
})

export const update = eventBus(async (event, context) => {
  const {
    body: { sk, title, startDate, endDate, frequency }
  } = event
  const tenantKey = getTenantKey(context)
  const meeting = await MeetingModel.get({ pk: `${tenantKey}#meeting`, sk })
  const update = {}
  if (title && title !== meeting?.title) {
    update.title = title
  }

  if (startDate && startDate !== meeting?.startDate) {
    update.startDate = startDate
    update.year = moment(startDate).year()
    update.month = moment(startDate).month()
  }

  if (endDate && endDate !== meeting?.endDate) {
    update.endDate = endDate
  }

  if (frequency !== meeting?.frequency) {
    update.frequency = frequency
  }

  return await MeetingModel.update(meeting, update)
})

export const generateMeetingRtmToken = eventBus(async (event, context) => {
  const {
    body: { sk }
  } = event
  const { pk } = await getUserFromEvent(event, context)
  return GenerateRtmToken({ channel: sk, pk })
})
