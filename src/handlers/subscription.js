import { createCard, tokenizeCreditCard } from '../queries/card'
import { Safe2PayServices } from '../services/Safe2Pay'
import http from '@emerald-works/middleware-http'
import { sendSQSMessage } from '../util/sqs'
import {
  checkIfTenantIsAvailable,
  getTenantByIdSubscription,
  getTenant
} from '../queries/tenant'
import { TenantRegisterModel } from '../models/tenant-register'
import eventBus from '@emerald-works/middleware-event-bus'
import { getTenantKey } from '../util/tenant'
import { TenantModel } from '../models/tenant'

const { ENVIRONMENT_NAME, PROJECT_NAME } = process.env

const PLANS = [
  14780, // Plano Inicial R$99
  14781, // Plano teste R$5
  14822 // Teste R$1
]
const CREATE_TENANT_QUEUE_NAME = `${PROJECT_NAME}-create-tenant-${ENVIRONMENT_NAME}`

const getPlansFn = async (event) => {
  const { data: plan } = await Safe2PayServices.get('Plans?IsEnabled=true')
  return plan.data.objects.filter(({ idPlan }) => PLANS.indexOf(idPlan) >= 0)
}

export const getPlansHttp = http(getPlansFn)

export const getPlans = eventBus(getPlansFn)

export const updateSubscriptionStatus = http(async (event, context) => {
  console.log({ sourceIp: event?.requestContext?.identity?.sourceIp })
  const {
    body: {
      TransactionStatus: { Name: status },
      Origin
    }
  } = event

  if (Origin) {
    const {
      Plan: {
        Subscription: { IdSubscription }
      }
    } = Origin
    console.log({ IdSubscription })
    const tenant = await getTenantByIdSubscription(IdSubscription)
    // the tenant creation should happen if tenant doesn't exist yet
    if (!tenant) {
      const tenantRegister = await TenantRegisterModel.get(IdSubscription)
      await TenantRegisterModel.update(tenantRegister, { status })

      console.log(tenantRegister)
      console.log({ status })
      await sendSQSMessage(CREATE_TENANT_QUEUE_NAME, tenantRegister)
    }
  }
  return {}
})

export const create = http(async (event, context) => {
  const {
    body: {
      name: Name,
      tenantKey,
      holder: Holder,
      cardNumber: CardNumber,
      expirationDate: ExpirationDate,
      securityCode: SecurityCode,
      email: Email,
      paymentMethod: PaymentMethod,
      identity: Identity,
      phone: Phone,
      city: CodeIBGE,
      district: District,
      street: Street,
      number: Number,
      complement: Complement,
      zipcode: ZipCode,
      plan
    }
  } = event

  const isCCPaymentMethod = PaymentMethod === 2

  const { available } = await checkIfTenantIsAvailable(tenantKey)
  if (!available) {
    return {
      success: false,
      Errors: [
        {
          Message: 'Esse id não está mais disponível.',
          ErrorName: 'Erro de validação.'
        }
      ]
    }
  }
  try {
    let ccInfo
    // CC
    if (isCCPaymentMethod) {
      const res = await tokenizeCreditCard({
        CardNumber,
        ExpirationDate,
        Holder,
        SecurityCode
      })
      if (res.HasError) {
        return {
          success: false,
          Errors: [
            {
              Message: `Erro ${res.ErrorCode}: ${res.Error}`,
              ErrorName: 'Erro nos dados de pagamento.'
            }
          ]
        }
      } else {
        ccInfo = res.ResponseDetail
      }
    }

    const params = {
      PaymentMethod,
      Customer: {
        Name,
        Email,
        Identity,
        Phone,
        Address: {
          City: { CodeIBGE },
          District,
          Street,
          Number: Number.toString(),
          Complement,
          ZipCode
        }
      },
      Emails: [Email],
      Token: isCCPaymentMethod ? ccInfo.Token : undefined
    }

    console.log(JSON.stringify(params))

    const res = await Safe2PayServices.post(
      `Plans/${plan}/subscriptions`,
      params
    ).catch(({ response }) => response)

    // FAIL
    if (!res.data.success) {
      console.error(res.data)
      return res.data
    }

    const {
      data: { idSubscription }
    } = res.data

    await TenantRegisterModel.create({
      name: Name,
      tenantKey,
      ownerEmails: [Email],
      idSubscription,
      identity: Identity,
      city: CodeIBGE,
      district: District,
      street: Street,
      number: Number,
      complement: Complement,
      zipcode: ZipCode
    })

    if (PaymentMethod === 2) {
      // Add the card to cards list
      await createCard({
        tenantKey,
        ...ccInfo
      })
    }
    return { success: true }
  } catch (e) {
    return {
      success: false,
      Errors: [
        {
          Message: e.toString(),
          ErrorName: 'Erro interno.'
        }
      ]
    }
  }
})

export const addCreditCard = async (event, context) => {
  const { Holder, CardNumber, ExpirationDate, SecurityCode } = event
  const body = { Holder, CardNumber, ExpirationDate, SecurityCode }
  return await tokenizeCreditCard(body)
}

export const getSubscription = eventBus(async (event, context) => {
  const tenantKey = getTenantKey(context)
  const tenant = await getTenant(tenantKey)
  const { idSubscription } = tenant
  if (!idSubscription) {
    return {}
  }
  const {
    data: { data: subscription }
  } = await Safe2PayServices.get(`Subscriptions/${idSubscription}`)

  const { idPlan, statusName, paymentMethodName, emails, createdDate } =
    subscription
  const {
    data: { data: plan }
  } = await Safe2PayServices.get(`Plans/${idPlan}`)

  const { planFrequence, name, description, amount } = plan

  return {
    subscription: { statusName, paymentMethodName, emails, createdDate },
    plan: { planFrequence, name, description, amount, idPlan }
  }
})

export const cancelSubscription = eventBus(async (event, context) => {
  const tenantKey = getTenantKey(context)
  const tenant = await getTenant(tenantKey)
  const { idSubscription } = tenant
  await Safe2PayServices.patch(`Subscriptions/${idSubscription}/Disable`)
  await TenantModel.update(tenant, { idSubscription: undefined })
})

export const newSubscription = eventBus(async (event, context) => {
  const tenantKey = getTenantKey(context)
  const tenant = await getTenant(tenantKey)
  const {
    body: { idPlan, paymentMethod: PaymentMethod, Token }
  } = event
  const isCCPaymentMethod = PaymentMethod === 2
  const params = {
    PaymentMethod,
    Customer: {
      Name: tenant.name,
      Email: tenant.ownerEmail,
      Identity: tenant.identity,
      Address: {
        City: { CodeIBGE: tenant.city },
        District: tenant.district,
        Street: tenant.street,
        Number: tenant.number.toString(),
        Complement: tenant.complement,
        ZipCode: tenant.zipcode
      }
    },
    Emails: tenant.ownerEmails,
    Token: isCCPaymentMethod ? Token : undefined
  }

  console.log(JSON.stringify(params))

  const res = await Safe2PayServices.post(
    `Plans/${idPlan}/subscriptions`,
    params
  ).catch(({ response }) => response)

  // FAIL
  if (!res.data.success) {
    console.error(res.data)
    return res.data
  }

  const {
    data: { idSubscription }
  } = res.data

  await TenantModel.update(
    { tenantKey },
    {
      idSubscription,
      subscriptionHistory: [...tenant.subscriptionHistory, idSubscription]
    }
  )
  return res
})

export const getCharges = eventBus(async (event, context) => {
  const tenantKey = getTenantKey(context)
  const tenant = await getTenant(tenantKey)
  const { subscriptionHistory } = tenant
  const history = {}
  if (subscriptionHistory) {
    for (const sub of subscriptionHistory) {
      const res = await Safe2PayServices.get(
        `/Subscriptions/${sub}/Charges`
      ).catch((e) => e)
      history[sub] = res?.data?.data
    }
    return history
  }
  return {}
})
