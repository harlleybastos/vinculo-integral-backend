import { GroupModel } from '../models/group'
import { getMembers } from '../queries/group'
import { getPatient } from '../queries/patient'
import { getUser, getUserFromEvent } from '../queries/user'
import eventBus from '@emerald-works/middleware-event-bus'
import { getTenantKey } from '../util/tenant'

export const getUserGroups = eventBus(async (event, context) => {
  const user = await getUserFromEvent(event, context)
  switch (user.role) {
    case 'doctor':
      return await Promise.all(
        (
          await GroupModel.query({ professional: user.pk })
            .using('groupMembersIndex')
            .all()
            .exec()
        )
          .toJSON()
          .map(async (group) => ({
            ...group,
            groupInfo: await getPatient({ pk: group.patient })
          }))
      )
  }
  return []
})

export const getPatientDoctors = eventBus(async (event, context) => {
  const {
    body: { patient: doc }
  } = event
  const tenantKey = getTenantKey(context)
  const patientPk = `${tenantKey}#${doc}`
  const doctors = await getMembers(patientPk)
  return doctors
})

export const addDoctorToGroup = eventBus(async (event, context) => {
  const tenantKey = getTenantKey(context)
  const {
    body: { patient: patientDoc, professional }
  } = event
  const doctor = await getUser({ pk: professional })
  const patient = await getPatient({ tenantKey, doc: patientDoc })
  const patientPk = `${tenantKey}#${patientDoc}`
  if (doctor && patient) {
    return await GroupModel.create({ patient: patientPk, professional })
  }
})

export const removeDoctorFromGroup = eventBus(async (event, context) => {
  const tenantKey = getTenantKey(context)
  const {
    body: { patient, professional }
  } = event
  const patientPk = `${tenantKey}#${patient}`
  return await (
    await GroupModel.get({ patient: patientPk, professional })
  ).delete()
})

export const setIsSupervisor = eventBus(async (event, context) => {
  const {
    body: { patient, professional, isSupervisor }
  } = event
  return await GroupModel.update({ patient, professional }, { isSupervisor })
})
