import eventBus from '@emerald-works/middleware-event-bus'
import { FileModel } from '../models/file'
import {
  deleteFileFromBucket,
  generateUploadPresignedUrl
} from '../services/S3'
import uuid from 'uuid-random'
import { getUserFromEvent } from '../queries/user'
import { getSignedUrl } from '@aws-sdk/s3-request-presigner'
import { GetObjectCommand, S3Client } from '@aws-sdk/client-s3'
import { USER_FILES_BUCKET_NAME } from '../util/consts'

export const getFiles = eventBus(async (event, context) => {
  const {
    body: { pk }
  } = event
  const files = await FileModel.query('pk').eq(pk).exec()
  return await Promise.all(
    files.map(async (file) => ({
      ...file,
      url: await getSignedUrl(
        new S3Client(),

        new GetObjectCommand({
          Key: `${pk}/${file.sk}`,
          Bucket: USER_FILES_BUCKET_NAME
        }),
        { expiresIn: 5 * 60 * 60 } // 5 hours
      )
    }))
  )
})

export const uploadFiles = eventBus(async (event, context) => {
  const {
    body: { files }
  } = event
  const me = await getUserFromEvent(event)
  return await Promise.all(
    files.map(async ({ fileType }) => {
      const sk = uuid()
      return {
        url: await generateUploadPresignedUrl({
          Key: `${me.pk}/${sk}`,
          ContentType: fileType
        }),
        sk
      }
    })
  )
})

export const finishUploadFiles = eventBus(async (event, context) => {
  const {
    body: { files }
  } = event
  const me = await getUserFromEvent(event)
  await Promise.all(
    files.map(
      async ({ fileType, sk, fileName, fileSize }) =>
        await FileModel.create({ pk: me.pk, sk, fileType, fileName, fileSize })
    )
  )
})

export const deleteFile = eventBus(async (event, context) => {
  const {
    body: { sk }
  } = event
  const me = await getUserFromEvent(event)
  await deleteFileFromBucket({ Key: `${me.pk}/${sk}` })
  return await FileModel.delete({ pk: me.pk, sk })
})
