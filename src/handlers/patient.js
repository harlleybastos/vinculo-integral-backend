import eventBus from '@emerald-works/middleware-event-bus'
import { getUserFromEvent } from '../queries/user'
import {
  getAllPatients,
  getMyPatients,
  getPatient as getPatientFromQueries
} from '../queries/patient'
import { getTenantKey } from '../util/tenant'
import { PatientModel } from '../models/patient'
import { applyFixToName } from '../util/name'
import { cpf } from 'cpf-cnpj-validator'
import { fixCpfString } from '../util/cpf'

export const getPatients = eventBus(async (event, context) => {
  const tenantKey = getTenantKey(context)
  const {
    body: { limit, startAt, search, searchBy, active }
  } = event
  const user = await getUserFromEvent(event, context)
  switch (user.role) {
    case 'owner':
    case 'admin':
      return getAllPatients({
        tenantKey,
        limit,
        startAt,
        search,
        searchBy,
        active
      })
    case 'doctor':
      return getMyPatients({
        tenantKey,
        limit,
        startAt,
        search,
        searchBy,
        user,
        active
      })
  }
})

export const getPatient = eventBus(async (event, context) => {
  const tenantKey = getTenantKey(context)
  const {
    body: { patientDoc }
  } = event
  return await getPatientFromQueries({ tenantKey, doc: patientDoc })
})

export const createPatient = eventBus(async (event, context) => {
  const tenantKey = getTenantKey(context)
  const {
    body: { doc, name }
  } = event

  const pk = `${tenantKey}#${doc}`

  const patient = {
    tenantKey,
    doc,
    name: applyFixToName(name),
    pk
  }

  return await PatientModel.create(patient)
})

export const updatePatient = eventBus(async (event, context) => {
  const tenantKey = getTenantKey(context)
  const {
    body: { doc, name }
  } = event

  const patient = await PatientModel.get({ tenantKey, doc })
  if (patient) {
    const update = {}

    if (name) {
      update.name = applyFixToName(name)
    }

    return await PatientModel.update(patient, update)
  }
})

export const tooglePatientActive = eventBus(async (event, context) => {
  const tenantKey = getTenantKey(context)
  const {
    body: { doc }
  } = event
  const patient = await PatientModel.get({ tenantKey, doc })
  const { active } = patient
  if (active === false) {
    return await PatientModel.update(patient, { active: true })
  }
  return await PatientModel.update(patient, { active: false })
})

export const validatePatientsList = eventBus(async (event, context) => {
  const tenantKey = getTenantKey(context)
  const {
    body: { patients }
  } = event
  const errors = []
  for (const patient of patients) {
    const { doc, name } = patient
    if (!name) {
      errors.push({ patient, error: 'Nome inválido' })
      continue
    }
    if (!doc || !cpf.isValid(fixCpfString(doc))) {
      errors.push({ patient, error: 'CPF inválido' })
      continue
    }
    const patientAlreadyExists = await getPatientFromQueries({ doc, tenantKey })
    if (patientAlreadyExists) {
      errors.push({
        patient,
        error: 'Já existe um paciente com este cpf'
      })
      continue
    }
  }
  return errors
})
