import eventBus from '@emerald-works/middleware-event-bus'
import { getUser, getUserFromEvent } from '../queries/user'
import { AgendaModel } from '../models/agenda'
import { recreateAgendaEvent } from '../queries/agenda'
import moment from 'moment'
import { getPatient } from '../queries/patient'
import dynamoose from 'dynamoose-latest'
import { hour } from '../util/time'
import {
  applyDateCondition,
  getNextAndPreviousMonthNumbers
} from '../util/dynamodb/date'

export const getPatientAgenda = eventBus(async (event, context) => {
  const {
    body: { month, year, pk }
  } = event
  const query = AgendaModel.query('patient').eq(pk).using('patientAgendaIndex')

  const events = (
    await applyDateCondition(query, { month, year }).exec()
  ).toJSON()
  const agenda = await Promise.all(
    events.map(async (event) => ({
      ...event,
      doctor: await getUser({ pk: event.doctor }),
      patient: await getPatient({ pk: event.patient })
    }))
  )
  return agenda
})

export const getAgenda = eventBus(async (event, context) => {
  let query
  const user = await getUserFromEvent(event, context)
  const {
    body: { month, year, pk, searchBy }
  } = event
  const months = getNextAndPreviousMonthNumbers({ month, year })
  switch (user.role) {
    case 'doctor':
      query = AgendaModel.query('doctor').eq(pk)
      break
    case 'patient': // return patient agenda
    case 'family': // return it's patient agenda
      query = AgendaModel.query('patient').eq(pk).using('patientAgendaIndex')
      break
    case 'owner':
    case 'admin':
      if (searchBy === 'doctor') {
        query = AgendaModel.query('doctor').eq(pk)
      } else if (searchBy === 'patient') {
        query = AgendaModel.query('patient').eq(pk).using('patientAgendaIndex')
      }
      break
  }

  const events = (
    await query
      .and()
      .parenthesis(
        new dynamoose.Condition()
          .where('month')
          .eq(month)
          .and()
          .where('year')
          .eq(year)
          .or()
          .where('month')
          .eq(months.next.month)
          .and()
          .where('year')
          .eq(months.next.year)
          .or()
          .where('month')
          .eq(months.previous.month)
          .and()
          .where('year')
          .eq(months.previous.year)
          .or()
          .where('frequency')
          .exists()
      )
      .all()
      .exec()
  ).toJSON()
  const agenda = await Promise.all(
    events.map(async (event) => ({
      ...event,
      doctor: await getUser({ pk: event.doctor }),
      patient: await getPatient({ pk: event.patient })
    }))
  )
  return agenda
})

export const addEventToAgenda = eventBus(async (event, context) => {
  const {
    body: { patient: patientPk, startDate, endDate, title, notes, frequency }
  } = event

  if (!patientPk) {
    throw new Error('Paciente inválido.')
  }

  const startDateValue = new Date(startDate).valueOf()
  const endDateValue =
    startDate >= new Date(endDate).valueOf()
      ? startDateValue + hour
      : new Date(endDate).valueOf()

  const user = await getUserFromEvent(event, context)
  const agendaEvent = {
    doctor: user.pk,
    patient: patientPk,
    title,
    notes,
    frequency,
    year: moment(startDate).year(),
    month: moment(startDate).month(),
    startDate: startDateValue,
    endDate: endDateValue
  }

  return await AgendaModel.create(agendaEvent)
})

export const updateEventOnAgenda = eventBus(async (event, context) => {
  const {
    body: { startDate, newStartDate, patient, frequency, ...body }
  } = event
  const user = await getUserFromEvent(event, context)

  const agendaEvent = {
    doctor: user.pk,
    startDate: new Date(startDate).valueOf()
  }

  if (newStartDate) {
    await recreateAgendaEvent(
      { doctor: user.pk, startDate },
      { newStartDate: new Date(newStartDate).valueOf() }
    )
    agendaEvent.startDate = new Date(newStartDate).valueOf()
  }

  if (patient) {
    agendaEvent.patient = patient
  }

  const fields = ['title', 'notes']
  const update = {}
  for (const field of fields) {
    if (body[field]) {
      update[field] = body[field]
    }
  }

  if (body.endDate) {
    update.endDate = new Date(body.endDate).valueOf()
    if (agendaEvent.startDate >= update.endDate) {
      update.endDate = agendaEvent.startDate + hour
    }
  } else {
    const eventItem = await AgendaModel.get(agendaEvent)
    if (agendaEvent.startDate >= eventItem.endDate) {
      update.endDate = agendaEvent.startDate + hour
    }
  }

  if (frequency) {
    update.frequency = frequency
  } else {
    update.frequency = undefined
  }
  return await AgendaModel.update(agendaEvent, update)
})

export const removeEventFromAgenda = eventBus(async (event, context) => {
  const {
    body: { startDate }
  } = event
  const user = await getUserFromEvent(event, context)

  const agendaEvent = {
    doctor: user.pk,
    startDate: new Date(startDate).valueOf()
  }

  return await AgendaModel.delete(agendaEvent)
})
