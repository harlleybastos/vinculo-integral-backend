import { TenantModel } from '../models/tenant'
import { UserModel } from '../models/user'
import { getUser } from '../queries/user'
import { addUserToGroup } from '../services/Cognito'

export const postUserConfirmation = async (event, context, callback) => {
  try {
    if (event && event.source === 'serverless-plugin-warmup') {
      return 'Ok.'
    }
    const {
      userName,
      request: {
        userAttributes: { email }
      },
      userPoolId
    } = event
    const [{ tenantKey }] = await TenantModel.query({ userPoolId })
      .limit(1)
      .using('userPoolIdIndex')
      .exec()
    const user = await getUser({ tenantKey, email })

    if (user.role === 'owner' || user.role === 'admin' || user.role === 'doctor') {
      await addUserToGroup({ groupName: user.role, userPoolId, userName })
    }

    await UserModel.update(user, { userName })
    return callback(null, event)
  } catch (error) {
    return callback(error, event)
  }
}

export const preSignUp = async (event, context, callback) => {
  try {
    if (event && event.source === 'serverless-plugin-warmup') {
      return 'Ok.'
    }
    const {
      request: {
        userAttributes: { email }
      },
      userPoolId
    } = event

    const [{ tenantKey }] = await TenantModel.query({ userPoolId })
      .limit(1)
      .using('userPoolIdIndex')
      .exec()
    const user = await getUser({ tenantKey, email })
    if (user) {
      return callback(null, event)
    } else {
      throw new Error('User not found')
    }
  } catch (error) {
    return callback(error, event)
  }
}
