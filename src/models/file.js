import dynamoose from 'dynamoose-latest'
import * as DynamoDBUtil from '../util/dynamodb'
import ENV from '@emerald-works/utils-env'

export const BASE_TABLE_NAME = 'file'
const fullTableName = `${ENV.projectName}-${BASE_TABLE_NAME}-${ENV.stage}`

const dynamooseSchema = new dynamoose.Schema(
  {
    pk: {
      // tenantKey#email
      type: String,
      hashKey: true
    },
    sk: {
      type: String,
      rangeKey: true
    },
    fileName: String,
    fileType: String,
    fileSize: Number
  },
  {
    timestamps: false,
    saveUnknown: false,
    throughput: 'ON_DEMAND'
  }
)

export const FileModel = dynamoose.model(fullTableName, dynamooseSchema)
export const FileTable = DynamoDBUtil.Table.buildTable({
  tableName: fullTableName,
  models: [FileModel]
})
