import dynamoose from 'dynamoose-latest'
import * as DynamoDBUtil from '../util/dynamodb'
import ENV from '@emerald-works/utils-env'

export const BASE_TABLE_NAME = 'user'
const fullTableName = `${ENV.projectName}-${BASE_TABLE_NAME}-${ENV.stage}`

const dynamooseSchema = new dynamoose.Schema(
  {
    tenantKey: {
      type: String,
      hashKey: true
    },
    email: {
      type: String,
      rangeKey: true
    },
    patientDoc: {
      // only for patients
      type: String,
      index: {
        global: true,
        name: 'patientIndex'
      }
    },
    pk: String, // tenantKey#email
    role: String, // doctor, admin, patient, family
    name: String,
    isSupervisor: Boolean,
    userName: {
      type: String,
      index: {
        global: true,
        name: 'cognitoUsernameIndex'
      }
    },
    tags: {
      type: Array,
      schema: [String]
    },
    hasAcceptedTerms: {
      type: Boolean,
      required: true,
      default: false
    }
  },
  {
    timestamps: true,
    saveUnknown: false,
    throughput: 'ON_DEMAND'
  }
)

export const UserModel = dynamoose.model(fullTableName, dynamooseSchema)
export const UserTable = DynamoDBUtil.Table.buildTable({
  tableName: fullTableName,
  models: [UserModel]
})
