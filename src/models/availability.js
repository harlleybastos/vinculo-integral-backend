import dynamoose from 'dynamoose-latest'
import * as DynamoDBUtil from '../util/dynamodb'
import ENV from '@emerald-works/utils-env'

export const BASE_TABLE_NAME = 'availability'
const fullTableName = `${ENV.projectName}-${BASE_TABLE_NAME}-${ENV.stage}`

const dynamooseSchema = new dynamoose.Schema(
  {
    tenantKey: {
      // tenantKey
      type: String,
      hashKey: true
    },
    sk: {
      type: String,
      rangeKey: true,
      required: true
    },
    supervisor: {
      // tenantKey#email
      type: String,
      required: true
    },
    startDate: {
      type: Number,
      required: true
    },
    endDate: {
      type: Number,
      required: true
    },
    month: {
      type: Number,
      required: true
    },
    year: { type: Number, required: true },
    frequency: String
  },
  {
    timestamps: true,
    saveUnknown: false,
    throughput: 'ON_DEMAND'
  }
)

export const AvailabilityModel = dynamoose.model(fullTableName, dynamooseSchema)
export const AvailabilityTable = DynamoDBUtil.Table.buildTable({
  tableName: fullTableName,
  models: [AvailabilityModel]
})
