import dynamoose from 'dynamoose-latest'
import * as DynamoDBUtil from '../util/dynamodb'
import ENV from '@emerald-works/utils-env'

export const BASE_TABLE_NAME = 'signature-document'
const fullTableName = `${ENV.projectName}-${BASE_TABLE_NAME}-${ENV.stage}`

const dynamooseSchema = new dynamoose.Schema(
  {
    tenantKey: {
      type: String,
      hashKey: true
    },
    key: {
      // document key on S3
      type: String,
      rangeKey: true
    },
    signedKey: {
      // signed document key on S3
      type: String
    },
    source: String, // vi or api
    status: {
      type: String,
      default: 'Criado'
    },
    arqsignData: {
      type: Object,
      schema: {
        idProcesso: String,
        documentos: {
          type: Array,
          schema: [{ type: Object, schema: { id: String, nome: String } }]
        }
      }
    },
    finishedAt: Number
  },
  {
    timestamps: true,
    saveUnknown: false,
    throughput: 'ON_DEMAND'
  }
)

export const SignatureDocumentModel = dynamoose.model(
  fullTableName,
  dynamooseSchema
)
export const SignatureDocumentTable = DynamoDBUtil.Table.buildTable({
  tableName: fullTableName,
  models: [SignatureDocumentModel]
})
