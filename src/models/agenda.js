import dynamoose from 'dynamoose-latest'
import * as DynamoDBUtil from '../util/dynamodb'
import ENV from '@emerald-works/utils-env'

export const BASE_TABLE_NAME = 'agenda'
const fullTableName = `${ENV.projectName}-${BASE_TABLE_NAME}-${ENV.stage}`

const dynamooseSchema = new dynamoose.Schema(
  {
    doctor: {
      // tenantKey#doctorPk
      type: String,
      hashKey: true
    },
    patient: {
      // tenantKey#patientPk
      type: String,
      index: {
        global: true,
        name: 'patientAgendaIndex'
      }
    },
    startDate: {
      type: Number,
      rangeKey: true,
      required: true
    },
    endDate: {
      type: Number,
      required: true
    },
    title: String,
    notes: String,
    month: Number,
    year: Number,
    frequency: String
  },
  {
    timestamps: true,
    saveUnknown: false,
    throughput: 'ON_DEMAND'
  }
)

export const AgendaModel = dynamoose.model(fullTableName, dynamooseSchema)
export const AgendaTable = DynamoDBUtil.Table.buildTable({
  tableName: fullTableName,
  models: [AgendaModel]
})
