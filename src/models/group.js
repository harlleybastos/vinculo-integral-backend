import dynamoose from 'dynamoose-latest'
import * as DynamoDBUtil from '../util/dynamodb'
import ENV from '@emerald-works/utils-env'

export const BASE_TABLE_NAME = 'group'
const fullTableName = `${ENV.projectName}-${BASE_TABLE_NAME}-${ENV.stage}`

// this table only stores members who are not patient or it's family
const dynamooseSchema = new dynamoose.Schema({
  patient: { //  tenantKey#patientPk
    type: String,
    hashKey: true
  },
  professional: { // tenantKey#doctorPk
    type: String,
    rangeKey: true,
    index: {
      global: true,
      name: 'groupMembersIndex'
    }
  },
  isSupervisor: Boolean
},
{
  timestamps: true,
  saveUnknown: false,
  throughput: 'ON_DEMAND'
})

export const GroupModel = dynamoose.model(fullTableName, dynamooseSchema)
export const GroupTable = DynamoDBUtil.Table.buildTable({
  tableName: fullTableName,
  models: [GroupModel]
})
