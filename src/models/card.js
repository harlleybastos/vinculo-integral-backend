import dynamoose from 'dynamoose-latest'
import * as DynamoDBUtil from '../util/dynamodb'
import ENV from '@emerald-works/utils-env'

export const BASE_TABLE_NAME = 'card'
const fullTableName = `${ENV.projectName}-${BASE_TABLE_NAME}-${ENV.stage}`

const dynamooseSchema = new dynamoose.Schema(
  {
    tenantKey: {
      type: String,
      hashKey: true
    },
    Token: {
      type: String,
      rangeKey: true
    },
    Holder: String,
    CardNumber: String,
    Expiration: String,
    Brand: String,
    CardType: String
  },
  {
    timestamps: false,
    saveUnknown: false,
    throughput: 'ON_DEMAND'
  }
)

export const CardModel = dynamoose.model(fullTableName, dynamooseSchema)
export const CardTable = DynamoDBUtil.Table.buildTable({
  tableName: fullTableName,
  models: [CardModel]
})
