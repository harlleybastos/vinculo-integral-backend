import dynamoose from 'dynamoose-latest'
import * as DynamoDBUtil from '../util/dynamodb'
import ENV from '@emerald-works/utils-env'

export const BASE_TABLE_NAME = 'attendance'
const fullTableName = `${ENV.projectName}-${BASE_TABLE_NAME}-${ENV.stage}`

const dynamooseSchema = new dynamoose.Schema(
  {
    patient: {
      // tenantKey#patientPk
      type: String,
      hashKey: true
    },
    start: {
      type: Number,
      rangeKey: true
    },
    end: Number,
    hasFinished: {
      type: Boolean,
      default: false
    },
    hasExported: {
      type: Boolean,
      default: false
    },
    notes: {
      type: String,
      default: ''
    },
    // tenantKey#email
    doctor: {
      type: String,
      required: true,
      index: {
        name: 'doctorInProgressAttendance',
        rangeKey: 'start',
        type: 'global'
      }
    },
    resourceId: String,
    sid: String,
    uid: String,
    token: String,
    slices: {
      type: Array,
      default: [],
      schema: [{ type: Number }]
    },
    tenantKey: String
  },
  {
    timestamps: true,
    saveUnknown: false,
    throughput: 'ON_DEMAND'
  }
)

export const AttendanceModel = dynamoose.model(fullTableName, dynamooseSchema)
export const AttendanceTable = DynamoDBUtil.Table.buildTable({
  tableName: fullTableName,
  models: [AttendanceModel]
})
