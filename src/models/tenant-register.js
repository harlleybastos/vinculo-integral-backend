import dynamoose from 'dynamoose-latest'
import * as DynamoDBUtil from '../util/dynamodb'
import ENV from '@emerald-works/utils-env'

export const BASE_TABLE_NAME = 'tenant-register'
const fullTableName = `${ENV.projectName}-${BASE_TABLE_NAME}-${ENV.stage}`

const dynamooseSchema = new dynamoose.Schema(
  {
    idSubscription: {
      type: Number,
      hashKey: true
    },
    tenantKey: String,
    name: {
      type: String,
      required: true
    },
    identity: String,
    city: String,
    district: String,
    street: String,
    number: String,
    complement: String,
    zipcode: String,
    ownerEmails: {
      type: Array,
      schema: [{ type: String }]
    },
    status: String
  },
  {
    timestamps: false,
    saveUnknown: false,
    throughput: 'ON_DEMAND'
  }
)

export const TenantRegisterModel = dynamoose.model(
  fullTableName,
  dynamooseSchema
)
export const TenantRegisterTable = DynamoDBUtil.Table.buildTable({
  tableName: fullTableName,
  models: [TenantRegisterModel]
})
