import dynamoose from 'dynamoose-latest'
import * as DynamoDBUtil from '../util/dynamodb'
import ENV from '@emerald-works/utils-env'

export const BASE_TABLE_NAME = 'attendance-comment'
const fullTableName = `${ENV.projectName}-${BASE_TABLE_NAME}-${ENV.stage}`

const dynamooseSchema = new dynamoose.Schema(
  {
    attendance: {
      // tenantKey#patientPk#start
      type: String,
      hashKey: true
    },
    moment: {
      type: Number,
      rangeKey: true
    },
    slice: Number,
    comment: {
      type: String,
      default: ''
    },
    commenter: {
      // userPk
      type: String,
      required: true
    }
  },
  {
    timestamps: true,
    saveUnknown: false,
    throughput: 'ON_DEMAND'
  }
)

export const AttendanceCommentModel = dynamoose.model(fullTableName, dynamooseSchema)
export const AttendanceCommentTable = DynamoDBUtil.Table.buildTable({
  tableName: fullTableName,
  models: [AttendanceCommentModel]
})
