import dynamoose from 'dynamoose-latest'
import * as DynamoDBUtil from '../util/dynamodb'
import ENV from '@emerald-works/utils-env'

export const BASE_TABLE_NAME = 'family'
const fullTableName = `${ENV.projectName}-${BASE_TABLE_NAME}-${ENV.stage}`

const dynamooseSchema = new dynamoose.Schema(
  {
    patient: {
      //  tenantKey#patientPk
      type: String,
      hashKey: true
    },
    family: {
      // tenantKey#userPk
      type: String,
      rangeKey: true,
      index: {
        global: true,
        name: 'familyMembersIndex'
      }
    }
  },
  {
    timestamps: true,
    saveUnknown: false,
    throughput: 'ON_DEMAND'
  }
)

export const FamilyModel = dynamoose.model(fullTableName, dynamooseSchema)
export const FamilyTable = DynamoDBUtil.Table.buildTable({
  tableName: fullTableName,
  models: [FamilyModel]
})
