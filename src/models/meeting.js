import dynamoose from 'dynamoose-latest'
import * as DynamoDBUtil from '../util/dynamodb'
import ENV from '@emerald-works/utils-env'

export const BASE_TABLE_NAME = 'meeting'
const fullTableName = `${ENV.projectName}-${BASE_TABLE_NAME}-${ENV.stage}`

const dynamooseSchema = new dynamoose.Schema(
  {
    pk: {
      // 'tenantKey#meeting' or
      // tenantKey#userPk
      type: String,
      hashKey: true
    },
    sk: {
      // random uuid
      rangeKey: true,
      type: String,
      index: [{ name: 'meetingSkIndex', rangeKey: 'pk', type: 'global' }]
    },
    title: String,
    host: {
      type: String,
      index: [{ name: 'hostIndex', rangeKey: 'sk', type: 'global' }]
    },
    startDate: Number,
    endDate: Number,
    frequency: String,
    month: Number,
    year: Number,
    token: String,
    type: {
      type: String,
      enum: ['meeting', 'mentoring'],
      required: true,
      default: 'meeting'
    }
  },
  {
    timestamps: true,
    saveUnknown: false,
    throughput: 'ON_DEMAND'
  }
)

export const MeetingModel = dynamoose.model(fullTableName, dynamooseSchema)
export const MeetingTable = DynamoDBUtil.Table.buildTable({
  tableName: fullTableName,
  models: [MeetingModel]
})
