import dynamoose from 'dynamoose-latest'
import * as DynamoDBUtil from '../util/dynamodb'
import ENV from '@emerald-works/utils-env'

export const BASE_TABLE_NAME = 'patient'
const fullTableName = `${ENV.projectName}-${BASE_TABLE_NAME}-${ENV.stage}`

const dynamooseSchema = new dynamoose.Schema(
  {
    tenantKey: {
      type: String,
      hashKey: true
    },
    doc: {
      type: String,
      required: true,
      rangeKey: true
    },
    name: String,
    pk: {
      type: String,
      index: {
        global: true,
        name: 'patientPkIndex'
      }
    },
    active: {
      type: Boolean,
      required: true,
      default: true
    }
  },
  {
    timestamps: false,
    saveUnknown: false,
    throughput: 'ON_DEMAND'
  }
)

export const PatientModel = dynamoose.model(fullTableName, dynamooseSchema)
export const PatientTable = DynamoDBUtil.Table.buildTable({
  tableName: fullTableName,
  models: [PatientModel]
})
