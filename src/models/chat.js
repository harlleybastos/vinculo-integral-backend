import dynamoose from 'dynamoose-latest'
import * as DynamoDBUtil from '../util/dynamodb'
import ENV from '@emerald-works/utils-env'

export const BASE_TABLE_NAME = 'chat'
const fullTableName = `${ENV.projectName}-${BASE_TABLE_NAME}-${ENV.stage}`

const dynamooseSchema = new dynamoose.Schema({
  tenantKey: {
    type: String,
    hashKey: true
  },
  sk: { // patientPk#userName#time
    type: String,
    rangeKey: true
  },
  time: Number,
  patient: String,
  sender: String,
  text: String
},
{
  timestamps: false,
  saveUnknown: false,
  throughput: 'ON_DEMAND'
})

export const ChatModel = dynamoose.model(fullTableName, dynamooseSchema)
export const ChatTable = DynamoDBUtil.Table.buildTable({
  tableName: fullTableName,
  models: [ChatModel]
})
