import dynamoose from 'dynamoose-latest'
import * as DynamoDBUtil from '../util/dynamodb'
import ENV from '@emerald-works/utils-env'

export const BASE_TABLE_NAME = 'tenant'
const fullTableName = `${ENV.projectName}-${BASE_TABLE_NAME}-${ENV.stage}`

const dynamooseSchema = new dynamoose.Schema(
  {
    tenantKey: {
      type: String,
      hashKey: true
    },
    name: {
      type: String,
      required: true
    },
    identity: String,
    ownerEmails: {
      type: Array,
      schema: [{ type: String }],
      required: true,
      default: []
    },
    subscriptionHistory: {
      type: Array,
      schema: [{ type: Number }]
    },
    city: String,
    district: String,
    street: String,
    number: Number,
    complement: String,
    zipcode: String,
    ownerEmail: String,
    cognitoSettings: {
      type: Object,
      schema: {
        stackId: String,
        stackName: String,
        userPoolId: String,
        userPoolWebClientId: String,
        userPoolDomain: String,
        identityPoolId: String,
        region: String,
        allowedOAuthScopes: {
          type: Array,
          schema: [String]
        }
      }
    },
    userPoolId: {
      type: String,
      index: {
        global: true,
        name: 'userPoolIdIndex'
      }
    },
    idSubscription: {
      type: Number,
      index: {
        global: true,
        name: 'idSubscriptionIndex'
      }
    },
    arqsignFolderId: {
      type: String
    }
  },
  {
    timestamps: false,
    saveUnknown: false,
    throughput: 'ON_DEMAND'
  }
)

export const TenantModel = dynamoose.model(fullTableName, dynamooseSchema)
export const TenantTable = DynamoDBUtil.Table.buildTable({
  tableName: fullTableName,
  models: [TenantModel]
})
