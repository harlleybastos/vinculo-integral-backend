import { TenantModel } from '../models/tenant'

export const checkIfTenantIsAvailable = async (tenantKey) => {
  const tenant = await getTenant(tenantKey)
  if (tenant) {
    return { available: false }
  }
  return { available: true }
}

export const getTenant = async (tenantKey) => {
  const [result] = await TenantModel.query({ tenantKey }).limit(1).exec()
  return result
}

export const getTenantByIdSubscription = async (idSubscription) => {
  try {
    const [result] = await TenantModel.query({ idSubscription })
      .using('idSubscriptionIndex')
      .limit(1)
      .exec()
    return result
  } catch (e) {}
}
