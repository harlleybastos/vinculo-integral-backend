import { CardModel } from '../models/card'
import { Safe2PayPayment } from '../services/Safe2Pay'
import { getOnlyNumbers } from '../util/string'

export const createCard = async (card) => await CardModel.create(card)

export const tokenizeCreditCard = async ({
  Holder,
  CardNumber,
  ExpirationDate,
  SecurityCode
}) => {
  const body = {
    Holder,
    CardNumber: getOnlyNumbers(CardNumber),
    ExpirationDate,
    SecurityCode: getOnlyNumbers(SecurityCode)
  }
  const res = await Safe2PayPayment.post('/token', body)
  return res.data
}
