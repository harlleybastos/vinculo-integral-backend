import { GroupModel } from '../models/group'
import { PatientModel } from '../models/patient'
import { getPatientFamily } from './family'
import { getUser } from './user'

export const getAllPatients = async ({
  tenantKey,
  limit,
  startAt,
  search,
  searchBy,
  active
}) => {
  const patients = await queryPatients({
    tenantKey,
    limit,
    startAt,
    search,
    searchBy,
    active
  })
  const patientsWithDoctorsAndFamily = await Promise.all(
    patients.map(async (patient) => {
      const doctors = await Promise.all(
        await GroupModel.query({ patient: `${tenantKey}#${patient.doc}` })
          .all()
          .exec()
      )
      const supervisors = await Promise.all(
        await GroupModel.query({
          patient: `${tenantKey}#${patient.doc}`,
          isSupervisor: true
        })
          .all()
          .exec()
      )
      const patientWithDoctorsAndFamily = {
        ...patient,
        doctors: await Promise.all(
          doctors.map(
            async (doctor) => await getUser({ pk: doctor.professional })
          )
        ),
        supervisors: await Promise.all(
          supervisors.map(
            async (doctor) => await getUser({ pk: doctor.professional })
          )
        ),
        family: await getPatientFamily({
          patientPk: `${tenantKey}#${patient.doc}`
        })
      }
      return patientWithDoctorsAndFamily
    })
  )
  return patientsWithDoctorsAndFamily
}

export const getMyPatients = async ({
  search,
  searchBy,
  user,
  tenantKey,
  active
}) => {
  const groups = await GroupModel.query({ professional: user.pk })
    .using('groupMembersIndex')
    .all()
    .exec()
  let patients = [
    ...(await Promise.all(
      groups.map(async (group) => {
        return await getPatient({ pk: group.patient })
      })
    ))
  ]

  if (active === false) {
    patients = patients.filter((patient) => patient.active === false)
  }

  if (searchBy && search) {
    patients = patients.filter(
      (patient) => patient[searchBy].indexOf(search) >= 0
    )
  }
  const patientsWithDoctorsAndFamily = await Promise.all(
    patients.map(async (patient) => {
      const doctors = await Promise.all(
        await GroupModel.query({ patient: `${tenantKey}#${patient.doc}` })
          .all()
          .exec()
      )
      const supervisors = await Promise.all(
        await GroupModel.query({
          patient: `${tenantKey}#${patient.doc}`,
          isSupervisor: true
        })
          .all()
          .exec()
      )
      const patientWithDoctorsAndFamily = {
        ...patient,
        doctors: await Promise.all(
          doctors.map(
            async (doctor) => await getUser({ pk: doctor.professional })
          )
        ),
        supervisors: await Promise.all(
          supervisors.map(
            async (doctor) => await getUser({ pk: doctor.professional })
          )
        ),
        family: await getPatientFamily({
          patientPk: `${tenantKey}#${patient.doc}`
        })
      }
      return patientWithDoctorsAndFamily
    })
  )
  return patientsWithDoctorsAndFamily
}

export const queryPatients = async (params) => {
  const { tenantKey, limit, startAt, search, searchBy, active } = params
  let query = PatientModel.query({ tenantKey })
  if (search) {
    switch (searchBy) {
      case 'name':
        query = query.filter(searchBy).contains(search)
        break
      case 'doc':
        query = query.where(searchBy).beginsWith(search)
        break
    }
  }

  if (active === false) {
    query = query.filter('active').eq(false)
  } else {
    query = query.filter('active').not().eq(false)
  }

  if (limit) {
    query = query.limit(limit)
  }
  if (startAt) {
    query = query.startAt(startAt)
  }

  let patients = []
  let lastKey

  do {
    const response = await query.exec()
    lastKey = response.lastKey
    patients = [...patients, ...response.toJSON()]
    query = query.startAt(lastKey)
    query = query.limit(limit - patients.length)
  } while (patients.length < limit && lastKey) // query until find $limit results

  const usersWithDoctorsAndFamily = await Promise.all(
    patients.map(async (patient) => {
      const doctors = await Promise.all(
        await GroupModel.query({ patient: `${tenantKey}#${patient.doc}` })
          .all()
          .exec()
      )
      const supervisors = await Promise.all(
        await GroupModel.query({
          patient: `${tenantKey}#${patient.doc}`,
          isSupervisor: true
        })
          .all()
          .exec()
      )
      const patientWithDoctorsAndFamily = {
        ...patient,
        doctors: await Promise.all(
          doctors.map(
            async (doctor) => await getUser({ pk: doctor.professional })
          )
        ),
        supervisors: await Promise.all(
          supervisors.map(
            async (doctor) => await getUser({ pk: doctor.professional })
          )
        ),
        family: await getPatientFamily({
          patientPk: `${tenantKey}#${patient.doc}`
        })
      }
      return patientWithDoctorsAndFamily
    })
  )
  return usersWithDoctorsAndFamily
}

export const getPatient = async ({ doc, tenantKey, pk }) =>
  pk
    ? (
        await PatientModel.query({ pk }).using('patientPkIndex').all().exec()
      )?.[0]
    : await PatientModel.get({ tenantKey, doc })
