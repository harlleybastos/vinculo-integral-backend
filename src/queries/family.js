import { FamilyModel } from '../models/family'
import { getPatient } from './patient'
import { getUser } from './user'

export const getPatients = async ({ userPk }) => {
  const familyMembership = await FamilyModel.query({ family: userPk })
    .using('familyMembersIndex')
    .all()
    .exec()
  const patients = await Promise.all(
    familyMembership.map(
      async ({ patient }) => await getPatient({ pk: patient })
    )
  )

  return patients
}

export const getPatientFamily = async ({ patientPk }) => {
  const familyMembership = await FamilyModel.query({ patient: patientPk })
    .all()
    .exec()
  const family = await Promise.all(
    familyMembership.map(async ({ family }) => await getUser({ pk: family }))
  )

  return family
}
