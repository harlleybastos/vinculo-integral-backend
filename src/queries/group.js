import { GroupModel } from '../models/group'
import { getUser } from './user'

export const getMembers = async (patient) => {
  const members = await Promise.all(
    (
      await GroupModel.query({ patient }).all().exec()
    ).map(async (member) => ({
      ...(await getUser({ pk: member.professional })),
      ...member
    }))
  )
  return members
}
