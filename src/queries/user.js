import { UserModel } from '../models/user'
import { getTenantKey } from '../util/tenant'
import { getPatientFamily, getPatients } from './family'
import { getMyPatients, getPatient } from './patient'

export const getUserFromEvent = async (event, context) => {
  if (!event?.detail?.context?.connection?.userAttributes) {
    console.error('User not found')
    return
  }
  const { Username: userName } = event.detail.context.connection.userAttributes

  const [user] = await UserModel.query({ userName })
    .using('cognitoUsernameIndex')
    .limit(1)
    .exec()
  if (user.role === 'family') {
    user.patients = await getPatients({ userPk: user.pk })
  } else if (user.role === 'patient') {
    const tenantKey = getTenantKey(context)
    user.patient = await getPatient({ tenantKey, doc: user.patientDoc })
  }
  return user
}

export const getUserEmailFromEvent = (event) =>
  event.detail.context.connection.userAttributes.Attributes.email

export const getUser = async ({ pk, email, tenantKey }, attributes = []) => {
  if (pk) {
    ;[tenantKey, email] = pk.split('#')
  }
  let query = UserModel.query({ email, tenantKey })
  if (attributes.length) {
    query = query.attributes(attributes)
  }
  const [user] = await query.exec()
  return user
}

export const getPatientUserAndFamily = async ({
  patientDoc,
  tenantKey,
  patientPk
}) => {
  if (patientPk) {
    ;[tenantKey, patientDoc] = patientPk.split('#')
  } else {
    patientPk = `${tenantKey}#${patientDoc}`
  }

  const [patient] = await UserModel.query({ tenantKey, patientDoc })
    .where('role')
    .eq('patient')
    .all()
    .exec()

  const family = await getPatientFamily({ patientPk })
  if (patient) {
    return [patient, ...family]
  } else {
    return family
  }
}

export const queryUsers = async (params) => {
  const { tenantKey, limit, startAt, search, searchBy, role, isSupervisor } =
    params
  let query = UserModel.query({ tenantKey })

  if (search) {
    switch (searchBy) {
      case 'name':
        query = query.filter(searchBy).contains(search)
        break
      case 'email':
        query = query.where(searchBy).beginsWith(search)
        break
    }
  }

  if (limit) {
    query = query.limit(limit)
  }
  if (startAt) {
    query = query.startAt(startAt)
  }

  if (role) {
    query = query.where('role').eq(role)
  }

  if (isSupervisor) {
    query = query.where('isSupervisor').eq(true)
  }

  let users = []
  let lastKey

  do {
    const response = await query.exec()
    lastKey = response.lastKey
    users = [...users, ...response.toJSON()]
    query = query.startAt(lastKey)
    query = query.limit(limit - users.length)
  } while (users.length < limit && lastKey) // query until find $limit results

  const usersWithDoctorsAndFamily = await Promise.all(
    users.map(async (user) => {
      if (user.role === 'family') {
        const familyWithPatientInfo = {
          ...user,
          patients: await getPatients({ userPk: user.pk })
        }
        return familyWithPatientInfo
      } else if (user.role === 'doctor') {
        const doctorWithSupervised = {
          ...user,
          supervised: await getSupervised({ pk: user.pk }),
          patients: await getMyPatients({ user, tenantKey })
        }
        return doctorWithSupervised
      }
      return user
    })
  )
  return usersWithDoctorsAndFamily
}

const getSupervised = async ({ pk }) => []
