import moment from 'moment'
import { AgendaModel } from '../models/agenda'
import { transaction } from 'dynamoose-latest'

export const recreateAgendaEvent = async (
  { startDate, doctor },
  { newStartDate }
) => {
  const eventToDelete = { startDate, doctor }
  const [newAgendaEvent] = (
    await AgendaModel.query({ startDate, doctor }).limit(1).exec()
  ).toJSON()
  newAgendaEvent.startDate = newStartDate
  newAgendaEvent.year = moment(newStartDate).year()
  newAgendaEvent.month = moment(newStartDate).month()
  return await transaction([
    AgendaModel.transaction.delete(eventToDelete),
    AgendaModel.transaction.create(newAgendaEvent)
  ])
}
