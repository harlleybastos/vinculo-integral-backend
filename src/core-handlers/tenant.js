/* eslint-disable no-template-curly-in-string */
import debug from 'debug'
import {
  CloudFormationClient,
  CreateStackCommand,
  DescribeStackResourcesCommand,
  waitUntilStackCreateComplete
} from '@aws-sdk/client-cloudformation'
import {
  AdminSetUserPasswordCommand,
  CognitoIdentityProviderClient,
  CreateGroupCommand,
  GetGroupCommand,
  ListUsersCommand
} from '@aws-sdk/client-cognito-identity-provider'
import { TenantModel } from '../models/tenant'
import { UserModel } from '../models/user'
import { ARQSIGN_DEFAULT_FOLDER } from '../util/consts'

const { ENVIRONMENT_NAME, AWS_ACCOUNT_ID, AWS_REGION, PROJECT_NAME } =
  process.env

const getCloudFormation = (region) => new CloudFormationClient({ region })

const getIdentityProvider = () => new CognitoIdentityProviderClient({})

const logger = (handler) =>
  debug(`${process.env.PROJECT_NAME}:client:${handler}`)

export const createTenant = async (event) => {
  const { Records } = event
  const {
    tenantKey,
    name,
    identity,
    city,
    district,
    street,
    number,
    complement,
    zipcode,
    region = 'sa-east-1',
    ownerEmails,
    idSubscription,
    identityProviders = {}
  } = Records?.length ? JSON.parse(Records[0].body) : event
  const allowedOAuthScopes = ['phone', 'email', 'openid', 'profile']
  const { stackId, stackName, resources } = await createTenantCognitoStack(
    tenantKey,
    region,
    allowedOAuthScopes,
    identityProviders
  )

  const awsToModelAttrMap = {
    CognitoUserPool: 'userPoolId',
    CognitoUserPoolClient: 'userPoolWebClientId',
    UserPoolDomain: 'userPoolDomain',
    CognitoIdentityPool: 'identityPoolId'
  }

  const cognitoSettings = resources.reduce(
    (result, { LogicalResourceId, PhysicalResourceId }) => {
      return {
        ...result,
        [awsToModelAttrMap[LogicalResourceId]]: PhysicalResourceId
      }
    },
    { stackId, stackName, allowedOAuthScopes, region }
  )

  const client = await TenantModel.create({
    tenantKey,
    name,
    identity,
    city,
    district,
    street,
    number,
    complement,
    zipcode,
    cognitoSettings,
    ownerEmails,
    ownerEmail: ownerEmails?.length > 0 ? ownerEmails[0] : undefined,
    userPoolId: cognitoSettings.userPoolId,
    subscriptionHistory: idSubscription ? [idSubscription] : [],
    idSubscription,
    arqsignFolderId: ARQSIGN_DEFAULT_FOLDER
  })
  await createCognitoGroupsAndBaseUser(client, ownerEmails)

  return cognitoSettings
}

const createTenantCognitoStack = async (
  tenantKey,
  region = AWS_REGION,
  allowedOAuthScopes,
  identityProviders = {}
) => {
  const cloudFormation = getCloudFormation(region)
  const stackName = `tenant-${tenantKey}-${ENVIRONMENT_NAME}`
  const TemplateBody = createTenantCognitoTemplateBody(
    tenantKey,
    stackName,
    region,
    allowedOAuthScopes,
    identityProviders
  )
  logger('createTenantCognitoStack TemplateBody', stackName, TemplateBody)
  const params = {
    StackName: stackName,
    Capabilities: [],
    ClientRequestToken: `${stackName}`,
    EnableTerminationProtection: false,
    OnFailure: 'DELETE',
    Parameters: [],
    ResourceTypes: ['AWS::*'],
    TemplateBody
  }
  const createStackCommand = new CreateStackCommand(params)
  const createStackResponse = await cloudFormation.send(createStackCommand)
  const stackId = createStackResponse.StackId

  // Esperar até que a criação da stack seja concluída
  const waiterParams = { client: cloudFormation, maxWaitTime: 300 } // Configuração opcional do waiter
  await waitUntilStackCreateComplete(waiterParams, { StackName: stackId })

  // Obter recursos da stack
  const describeCommand = new DescribeStackResourcesCommand({
    StackName: stackId
  })
  const describeResponse = await cloudFormation.send(describeCommand)

  const resources = describeResponse.StackResources

  return { stackId, stackName, resources }
}

const createTenantCognitoTemplateBody = (
  tenantKey,
  stackName,
  region,
  AllowedOAuthScopes,
  identityProviders
) => {
  const domain = `${tenantKey}-${ENVIRONMENT_NAME}`

  let CallbackURLs = [
    `${generateCognitoDomain(tenantKey, region)}/signin`,
    GenerateAppCallbackUrl(tenantKey, true)
  ]

  let LogoutURLs = [
    `${generateCognitoDomain(tenantKey, region)}/signout`,
    GenerateAppCallbackUrl(tenantKey, false)
  ]
  if (ENVIRONMENT_NAME !== 'live') {
    CallbackURLs = [
      ...CallbackURLs,
      'https://localhost:3000/signin',
      `https://localhost:3000/${tenantKey}/sso/signin`
    ]
    LogoutURLs = [
      ...LogoutURLs,
      'https://localhost:3000/signout',
      `https://localhost:3000/${tenantKey}/signout`
    ]
  }

  const SupportedIdentityProviders = ['COGNITO']
  const PostConfirmation = `arn:aws:lambda:${AWS_REGION}:${AWS_ACCOUNT_ID}:function:${PROJECT_NAME}-${ENVIRONMENT_NAME}-cognitoPostUserConfirmation`
  const PreSignUp = `arn:aws:lambda:${AWS_REGION}:${AWS_ACCOUNT_ID}:function:${PROJECT_NAME}-${ENVIRONMENT_NAME}-cognitoPreSignUp`
  const template = {
    AWSTemplateFormatVersion: '2010-09-09',
    Resources: {
      CognitoUserPool: {
        Type: 'AWS::Cognito::UserPool',
        Properties: {
          UserPoolName: stackName,
          Policies: {
            PasswordPolicy: {
              MinimumLength: 8,
              RequireUppercase: true,
              RequireLowercase: true,
              RequireNumbers: true,
              RequireSymbols: true,
              TemporaryPasswordValidityDays: 7
            }
          },
          LambdaConfig: { PostConfirmation, PreSignUp },
          Schema: [
            {
              Name: 'given_name',
              AttributeDataType: 'String',
              DeveloperOnlyAttribute: false,
              Mutable: true,
              Required: false,
              StringAttributeConstraints: {
                MinLength: '0',
                MaxLength: '2048'
              }
            },
            {
              Name: 'family_name',
              AttributeDataType: 'String',
              DeveloperOnlyAttribute: false,
              Mutable: true,
              Required: false,
              StringAttributeConstraints: {
                MinLength: '0',
                MaxLength: '2048'
              }
            },
            {
              Name: 'email',
              AttributeDataType: 'String',
              DeveloperOnlyAttribute: false,
              Mutable: true,
              Required: false,
              StringAttributeConstraints: {
                MinLength: '0',
                MaxLength: '2048'
              }
            },
            {
              Name: 'email_verified',
              AttributeDataType: 'Boolean',
              DeveloperOnlyAttribute: false,
              Mutable: true,
              Required: false
            },
            {
              Name: 'experience',
              AttributeDataType: 'String',
              DeveloperOnlyAttribute: false,
              Mutable: true,
              Required: false
            },
            {
              Name: 'name',
              AttributeDataType: 'String',
              DeveloperOnlyAttribute: false,
              Mutable: true,
              Required: false
            }
          ],
          MfaConfiguration: 'OFF',
          UsernameAttributes: ['email'],
          EmailConfiguration: {
            EmailSendingAccount: 'COGNITO_DEFAULT'
          },
          UserPoolTags: {},
          AutoVerifiedAttributes: ['email'],
          AccountRecoverySetting: {
            RecoveryMechanisms: [
              {
                Priority: 1,
                Name: 'verified_email'
              }
            ]
          },
          UsernameConfiguration: {
            CaseSensitive: false
          },
          VerificationMessageTemplate: {
            DefaultEmailOption: 'CONFIRM_WITH_CODE'
          }
        },
        DependsOn: []
      },
      CognitoUserPoolClient: {
        Type: 'AWS::Cognito::UserPoolClient',
        Properties: {
          ClientName: stackName,
          UserPoolId: {
            'Fn::Sub': [
              '${CognitoUserPool}',
              {
                CognitoUserPool: {
                  Ref: 'CognitoUserPool'
                }
              }
            ]
          },
          AllowedOAuthFlows: ['code', 'implicit'],
          AllowedOAuthScopes,
          CallbackURLs,
          LogoutURLs,
          SupportedIdentityProviders,
          AllowedOAuthFlowsUserPoolClient: true,
          RefreshTokenValidity: 30,
          ExplicitAuthFlows: [
            'ALLOW_ADMIN_USER_PASSWORD_AUTH',
            'ALLOW_CUSTOM_AUTH',
            'ALLOW_REFRESH_TOKEN_AUTH',
            'ALLOW_USER_PASSWORD_AUTH',
            'ALLOW_USER_SRP_AUTH'
          ],
          PreventUserExistenceErrors: 'LEGACY'
        },
        DependsOn: []
      },
      UserPoolDomain: {
        Type: 'AWS::Cognito::UserPoolDomain',
        Properties: {
          UserPoolId: {
            'Fn::Sub': [
              '${CognitoUserPool}',
              {
                CognitoUserPool: {
                  Ref: 'CognitoUserPool'
                }
              }
            ]
          },
          Domain: domain
        },
        DependsOn: []
      },
      CognitoIdentityPool: {
        Type: 'AWS::Cognito::IdentityPool',
        Properties: {
          IdentityPoolName: stackName,
          AllowUnauthenticatedIdentities: true,
          CognitoIdentityProviders: [
            {
              ProviderName: {
                'Fn::Sub': [
                  'cognito-idp.${AWS::Region}.amazonaws.com/${CognitoUserPool}',
                  {
                    CognitoUserPool: {
                      Ref: 'CognitoUserPool'
                    }
                  }
                ]
              },
              ClientId: {
                'Fn::Sub': [
                  '${CognitoUserPoolClient}',
                  {
                    CognitoUserPoolClient: {
                      Ref: 'CognitoUserPoolClient'
                    }
                  }
                ]
              },
              ServerSideTokenCheck: false
            }
          ]
        },
        DependsOn: []
      },
      CognitoIdentityPoolRoleAttachment: {
        Type: 'AWS::Cognito::IdentityPoolRoleAttachment',
        Properties: {
          IdentityPoolId: {
            'Fn::Sub': [
              '${CognitoIdentityPool}',
              {
                CognitoIdentityPool: {
                  Ref: 'CognitoIdentityPool'
                }
              }
            ]
          },
          Roles: {
            authenticated: GetAuthenticatedUserRoleArn(),
            unauthenticated: GetUnauthenticatedUserRoleArn()
          },
          RoleMappings: {
            userPool: {
              IdentityProvider: {
                'Fn::Sub': [
                  'cognito-idp.${AWS::Region}.amazonaws.com/${CognitoUserPool}:${CognitoUserPoolClient}',
                  {
                    CognitoUserPool: {
                      Ref: 'CognitoUserPool'
                    },
                    CognitoUserPoolClient: {
                      Ref: 'CognitoUserPoolClient'
                    }
                  }
                ]
              },
              Type: 'Token',
              AmbiguousRoleResolution: 'AuthenticatedRole'
            }
          }
        },
        DependsOn: []
      },
      CognitoPostUserConfirmationLambdaPermission: {
        Type: 'AWS::Lambda::Permission',
        Properties: {
          Action: 'lambda:InvokeFunction',
          FunctionName: PostConfirmation,
          Principal: 'cognito-idp.amazonaws.com',
          SourceArn: {
            'Fn::GetAtt': ['CognitoUserPool', 'Arn']
          }
        },
        DependsOn: []
      },
      CognitoPreSignUpLambdaPermission: {
        Type: 'AWS::Lambda::Permission',
        Properties: {
          Action: 'lambda:InvokeFunction',
          FunctionName: PreSignUp,
          Principal: 'cognito-idp.amazonaws.com',
          SourceArn: {
            'Fn::GetAtt': ['CognitoUserPool', 'Arn']
          }
        },
        DependsOn: []
      }
    }
  }

  return JSON.stringify(template)
}

const generateCognitoDomain = (tenantKey, region = AWS_REGION) => {
  const domain = `vi-${tenantKey}-${ENVIRONMENT_NAME}`
  return `https://${domain}.auth.${region}.amazoncognito.com`
}

const GenerateAppCallbackUrl = (tenantKey, isSigninIn = true) => {
  const operation = isSigninIn ? 'sso/signin' : 'signout'
  if (ENVIRONMENT_NAME !== 'live') {
    return `https://${ENVIRONMENT_NAME}-vi-frontend.dev.vinculointegral.com.br/${tenantKey}/${operation}`
  } else {
    return `https://vinculointegral.com.br/${tenantKey}/${operation}`
  }
}

export const GetAuthenticatedUserRoleArn = () => {
  return GetUserRoleArn('authenticated')
}

export const GetUnauthenticatedUserRoleArn = () => {
  return GetUserRoleArn('unauthenticated')
}

const GetUserRoleArn = (role) => {
  return `arn:aws:iam::${AWS_ACCOUNT_ID}:role/custom-user-roles/${GetUserRoleName(
    role
  )}`
}

export const GetUserRoleName = (role) => {
  return `${role}_User_Role_${PROJECT_NAME}_${ENVIRONMENT_NAME}_${AWS_REGION}`
}

const createCognitoGroupsAndBaseUser = async (client, ownerEmails = []) => {
  const groupNames = ['owner', 'admin', 'doctor']
  for (const groupName of groupNames) {
    const data = {
      groupName,
      roleArn: GetUserRoleArn(groupName),
      userPoolId: client.cognitoSettings.userPoolId,
      precedence: groupNames.findIndex((i) => i === groupName) + 1
    }
    const group = await createUserGroup(data)
    console.log(`postDeployBackend - created group ${group}`)
  }
  console.log('Creating default corvus user')

  const user = {
    email: 'corvus@corvus.com',
    password: 'Corvus@123',
    givenName: 'user',
    familyName: 'corvus',
    roleName: 'admin',
    tenantKey: client.tenantKey,
    userPoolId: client.cognitoSettings.userPoolId,
    identityPoolId: client.cognitoSettings.identityPoolId
  }

  await UserModel.create({
    email: user.email,
    role: user.roleName,
    name: 'Corvus',
    tenantKey: client.tenantKey,
    pk: `${client.tenantKey}#${user.email}`
  })
  await insert(user)

  for (const ownerEmail of ownerEmails) {
    await UserModel.create({
      email: ownerEmail.replace(/\s/g, ''),
      role: 'owner',
      name: 'Owner ' + client.name,
      tenantKey: client.tenantKey,
      pk: `${client.tenantKey}#${ownerEmail}`
    })
  }

  const cognito = getIdentityProvider()

  const listUserCommand = new ListUsersCommand({
    UserPoolId: client.cognitoSettings.userPoolId,
    Filter: 'email="corvus@corvus.com"'
  })
  const {
    Users: [cognitoUser]
  } = await cognito.send(listUserCommand)

  await UserModel.update(
    { tenantKey: client.tenantKey, email: user.email },
    { userName: cognitoUser.Username }
  )

  console.log('Default corvus user created')
}

const insert = async (userData) => {
  const {
    userPoolId,
    email,
    password,
    givenName,
    familyName,
    roleName,
    attributes,
    region = process.env.AWS_REGION,
    tenantKey
  } = userData

  const { stackId, stackName } = await createUserCognitoStack({
    tenantKey,
    userPoolId,
    email,
    password,
    givenName,
    familyName,
    attributes: {
      given_name: givenName,
      family_name: familyName,
      ...attributes
    },
    roleName,
    region
  })

  console.log('Created', stackId, stackName)

  logger('setting user default password')

  await setUserPassword(userPoolId, email, password)

  logger('saving user in DB')
}

const createUserCognitoStack = async ({
  tenantKey,
  userPoolId,
  email,
  givenName,
  familyName,
  attributes,
  roleName,
  description,
  region = process.env.AWS_REGION
}) => {
  const cloudFormation = getCloudFormation(region)
  const stackName = `user-${tenantKey}-${familyName}-${givenName}-${process.env.ENVIRONMENT_NAME}`
  const params = {
    StackName: stackName,
    Capabilities: [],
    ClientRequestToken: `${stackName}`,
    EnableTerminationProtection: false,
    OnFailure: 'DELETE',
    Parameters: [],
    ResourceTypes: ['AWS::*'],
    TemplateBody: createUserCognitoTemplateBody({
      userPoolId,
      email,
      attributes,
      roleName,
      description
    })
  }
  const { StackId: stackId } = await cloudFormation.send(
    new CreateStackCommand(params)
  )

  const waiterParams = { client: cloudFormation, maxWaitTime: 300 }
  await waitUntilStackCreateComplete(waiterParams, { StackName: stackId })

  const describeCommand = new DescribeStackResourcesCommand({
    StackName: stackId
  })
  const describeResponse = await cloudFormation.send(describeCommand)

  const resources = describeResponse.StackResources

  return { stackId, stackName, resources }
}

export const createUserCognitoTemplateBody = ({
  userPoolId,
  email,
  attributes = {},
  roleName,
  description
}) => {
  return JSON.stringify({
    AWSTemplateFormatVersion: '2010-09-09',
    Resources: {
      CognitoUserPoolUserCreate: {
        Type: 'AWS::Cognito::UserPoolUser',
        Properties: {
          UserPoolId: userPoolId,
          Username: email,
          DesiredDeliveryMediums: ['EMAIL'],
          ForceAliasCreation: false,
          MessageAction: 'SUPPRESS',
          UserAttributes: [
            { Name: 'email', Value: email },
            { Name: 'email_verified', Value: 'true' },
            // add all attributes
            ...Object.entries(attributes).map(([Name, Value]) => ({
              Name,
              Value
            }))
          ]
        },
        DependsOn: []
      },
      CognitoUserPoolUserToGroupAttachment: {
        Type: 'AWS::Cognito::UserPoolUserToGroupAttachment',
        Properties: {
          GroupName: roleName,
          UserPoolId: userPoolId,
          Username: email
        },
        DependsOn: ['CognitoUserPoolUserCreate']
      }
    }
  })
}

const createUserGroup = async ({
  groupName,
  userPoolId,
  roleArn,
  description = '',
  precedence = null
}) => {
  const cognito = getIdentityProvider()
  const isGroupExist = await cognitoGetGroup(groupName, userPoolId).catch()
  if (!isGroupExist) {
    const params = {
      GroupName: groupName,
      UserPoolId: userPoolId,
      Description: description,
      Precedence: precedence,
      RoleArn: roleArn
    }

    const createGroupCommand = new CreateGroupCommand(params)
    const { Group } = await cognito.send(createGroupCommand)
    return Group
  }
  return 'Group already exist'
}

const cognitoGetGroup = async (groupName, userPoolId) => {
  const cognito = getIdentityProvider()
  const params = {
    GroupName: groupName,
    UserPoolId: userPoolId
  }
  try {
    const getGroupCommand = new GetGroupCommand(params)
    const { Group } = cognito.send(getGroupCommand).catch((e) => e)
    console.log(Group)
    return Group
  } catch (error) {
    if (error.code === 'ResourceNotFoundException') return false
  }
}

const setUserPassword = async (userPoolId, username, password) => {
  const cognito = getIdentityProvider()
  const params = {
    UserPoolId: userPoolId,
    Username: username,
    Password: password,
    Permanent: true
  }
  const adminSetUserPasswordCommand = new AdminSetUserPasswordCommand(params)
  const result = await cognito.send(adminSetUserPasswordCommand)
  return result
}
