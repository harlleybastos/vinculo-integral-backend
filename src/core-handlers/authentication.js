import http from '@emerald-works/middleware-http'

export const generateCognitoUserMock = http(
  async () => {
    return {
      Username: 'fc032101-147f-42bd-acc6-ad48457add0a',
      Attributes: [{
        Name: 'sub',
        Value: 'fc032101-147f-42bd-acc6-ad48457add0a'
      },
      {
        Name: 'email_verified',
        Value: 'true'
      },
      {
        Name: 'email',
        Value: 'gmail@gmail.com'
      }
      ],
      UserCreateDate: new Date('2020-07-10T04:21:54.910Z'),
      UserLastModifiedDate: new Date('2020-07-10T04:22:06.509Z'),
      Enabled: true,
      UserStatus: 'CONFIRMED'
    }
  }
)

export const getUserProfile = http(
  async () => {
    return {
      name: 'jonny',
      data: 'data',
      why: 'this is the application level user profile.'
    }
  }
)

export const generateSecureParams = http(
  async () => {
    return {
      tenantKey: 'corvus'
    }
  }
)

export const allowConnection = http(
  async () => {
    // if you want to block the connection throw an error.
    // throw new Error('Trying to connect to a tenant you are not part of')
    return true
  }
)
