import http from '@emerald-works/middleware-http'
import { CognitoIdentityProvider } from '@aws-sdk/client-cognito-identity-provider'
import { Lambda } from '@aws-sdk/client-lambda'
import { SQS } from '@aws-sdk/client-sqs'
import { TenantModel } from '../models/tenant'
import {
  CREATE_TENANT_LAMBDA_NAME,
  CREATE_TENANT_QUEUE_NAME,
  POST_CONFIRMATION,
  PRE_SIGNUP,
  SAVE_ATTENDANCE_RECORDING_FIRST_SLICE_LAMBDA_NAME,
  SAVE_ATTENDANCE_RECORDING_FIRST_SLICE_QUEUE_NAME,
  SUBMIT_DOCUMENT_LAMBDA_NAME,
  SUBMIT_DOCUMENT_QUEUE_NAME
} from '../util/consts'

const { AWS_DEFAULT_REGION } = process.env

const LambdaClient = new Lambda()
const SQSClient = new SQS({
  region: AWS_DEFAULT_REGION
})
const CognitoClient = new CognitoIdentityProvider()

export const postDeployFrontend = http(async () => {
  console.log('postDeployFrontend lambda was executed')
  return true
})

export const postDeployBackend = http(async () => {
  await addLambdaTriggers()
  await addCognitoLambdaTriggers()
  console.log('postDeployBackend lambda was executed')
  return true
})

const addLambdaTriggers = async () => {
  await addLambdaTrigger(
    CREATE_TENANT_LAMBDA_NAME,
    await getSqs(CREATE_TENANT_QUEUE_NAME)
  )
  await addLambdaTrigger(
    SUBMIT_DOCUMENT_LAMBDA_NAME,
    await getSqs(SUBMIT_DOCUMENT_QUEUE_NAME)
  )
  await addLambdaTrigger(
    SAVE_ATTENDANCE_RECORDING_FIRST_SLICE_LAMBDA_NAME,
    await getSqs(SAVE_ATTENDANCE_RECORDING_FIRST_SLICE_QUEUE_NAME)
  )
}

const getSqs = async (QueueNamePrefix) => {
  try {
    const sqs = await SQSClient.listQueues({
      QueueNamePrefix
    })
    if (sqs.QueueUrls.length) {
      const QueueUrl = sqs.QueueUrls[0]
      const { QueueArn } = (
        await SQSClient.getQueueAttributes({
          QueueUrl,
          AttributeNames: ['QueueArn']
        })
      ).Attributes
      return QueueArn
    }
  } catch (e) {}
}

const addLambdaTrigger = async (FunctionName, EventSourceArn) => {
  try {
    await LambdaClient.createEventSourceMapping({
      FunctionName,
      EventSourceArn
    })
  } catch (e) {
    // console.log(e)
  }
}

const addCognitoLambdaTriggers = async () => {
  const tenants = await TenantModel.scan().all().exec()
  for (const tenant of tenants) {
    const {
      cognitoSettings: { userPoolId: UserPoolId }
    } = tenant
    const userPoolInfo = await CognitoClient.describeUserPool({
      UserPoolId
    })

    await CognitoClient.updateUserPool({
      UserPoolId,
      UserAttributeUpdateSettings: {
        AttributesRequireVerificationBeforeUpdate: ['email']
      },
      AutoVerifiedAttributes: ['email'],
      LambdaConfig: {
        PostConfirmation: POST_CONFIRMATION,
        PreSignUp: PRE_SIGNUP,
        ...userPoolInfo.LambdaConfig
      }
    })
  }
}
