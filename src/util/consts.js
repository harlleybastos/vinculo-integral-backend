export const {
  ENVIRONMENT_NAME,
  PROJECT_NAME,
  AWS_DEFAULT_REGION,
  AWS_ACCOUNT_ID
} = process.env

// SQS
export const CREATE_TENANT_QUEUE_NAME = `${PROJECT_NAME}-create-tenant-${ENVIRONMENT_NAME}`
export const SUBMIT_DOCUMENT_QUEUE_NAME = `${PROJECT_NAME}-submit-document-for-sign-${ENVIRONMENT_NAME}`
export const SAVE_ATTENDANCE_RECORDING_FIRST_SLICE_QUEUE_NAME = `${PROJECT_NAME}-save-attendance-recording-first-slice-${ENVIRONMENT_NAME}`

// Lambda
export const CREATE_TENANT_LAMBDA_NAME = `${process.env.PROJECT_NAME}-${process.env.ENVIRONMENT_NAME}-createTenant`
export const SUBMIT_DOCUMENT_LAMBDA_NAME = `${process.env.PROJECT_NAME}-signature-document-${process.env.ENVIRONMENT_NAME}-submitDocumentForSign`
export const SAVE_ATTENDANCE_RECORDING_FIRST_SLICE_LAMBDA_NAME = `${process.env.PROJECT_NAME}-attendance-${process.env.ENVIRONMENT_NAME}-saveAttendanceRecordingFirstSlice`

// Cognito Lambda Arn
export const POST_CONFIRMATION = `arn:aws:lambda:${AWS_DEFAULT_REGION}:${AWS_ACCOUNT_ID}:function:${PROJECT_NAME}-${ENVIRONMENT_NAME}-cognitoPostUserConfirmation`
export const PRE_SIGNUP = `arn:aws:lambda:${AWS_DEFAULT_REGION}:${AWS_ACCOUNT_ID}:function:${PROJECT_NAME}-${ENVIRONMENT_NAME}-cognitoPreSignUp`

// Recording
export const RECORDINGS_BUCKET_NAME =
  process.env.RECORDINGS_BUCKET_NAME || 'vi-api-stream-recordings-dev'

// Signature
export const DOCS_BUCKET_NAME =
  process.env.DOCS_BUCKET_NAME || 'vi-api-docs-dev'
export const ARQSIGN_DEFAULT_USER = 'da0d3d2e-5c86-4025-afb5-acb05867de09' // Michelle
export const ARQSIGN_DEFAULT_FOLDER =
  process.env.ARQSIGN_DEFAULT_FOLDER || '5b7dafca-6e96-47aa-9c92-729d96ebabd8' // Michelle/Development

// Agora.io
export const AGORA_IO_APP_ID = process.env.REACT_APP_AGORA_IO_APP_ID
export const AGORA_IO_API_KEY = process.env.AGORA_IO_API_KEY
export const AGORA_IO_API_SECRET = process.env.AGORA_IO_API_SECRET
export const AGORA_IO_APP_CERTIFICATE = process.env.AGORA_IO_APP_CERTIFICATE
export const RECORDINGS_BUCKET_ACCESS_KEY =
  process.env.RECORDINGS_BUCKET_ACCESS_KEY
export const RECORDINGS_BUCKET_SECRET_ACCESS_KEY =
  process.env.RECORDINGS_BUCKET_SECRET_ACCESS_KEY

// Files
export const USER_FILES_BUCKET_NAME =
  process.env.USER_FILES_BUCKET_NAME || 'vi-api-user-files-dev'
