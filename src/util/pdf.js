import moment from 'moment'
import attendanceTemplate from '../assets/pdf/attendance'
import { compile } from 'handlebars'

export const generateAttendanceHtml = ({
  attendanceBegin,
  attendanceEnd,
  therapistName,
  supervisorName = '',
  patientName,
  notes,
  therapistCrp,
  supervisorCrp = ''
}) => {
  const date = moment(attendanceBegin).format('DD/MM/YYYY')
  const beginTime = moment(attendanceBegin).format('HH:mm')
  const endTime = moment(attendanceEnd).format('HH:mm')
  const template = compile(attendanceTemplate)
  return template({
    date,
    beginTime,
    endTime,
    therapistName,
    supervisorName,
    patientName,
    notes,
    therapistCrp,
    supervisorCrp
  })
}
