import middy from '@middy/core'
import warmup from 'middy-middleware-warmup'

/**
 * Just a basic middleware intended for events that won't be called through
 * the websocket from a frontend app
 */
export const base = (handler, context, callback) =>
  middy(handler, context, callback)
    .use(warmup())
