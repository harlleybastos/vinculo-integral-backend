const { ENVIRONMENT_NAME, PROJECT_NAME } = process.env

export const GetUserRoleName = (role) => {
  return `${role}_User_Role_${PROJECT_NAME}_${ENVIRONMENT_NAME}`
}

export const GetUserRoleArn = (role) => {
  return `arn:aws:iam::${
    process.env.AWS_ACCOUNT_ID
  }:role/custom-user-roles/${GetUserRoleName(role)}`
}
