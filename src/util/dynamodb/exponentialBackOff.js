import ENV from '@emerald-works/utils-env'
import debug from 'debug'
import { random } from 'lodash'

const logger = debug(`${ENV.projectName}:util:dynamodb:exponentialBackOff`)

// https://github.com/dynamoose/dynamoose/issues/209
const baseBackOff = 1000
export const handleThroughput = async (data, method, backoff = baseBackOff, attempt = 1) => {
  try {
    logger('[handleThroughput] method %j data %j', method, data)
    return await method(data)
  } catch (e) {
    if (e.code === 'ProvisionedThroughputExceededException') {
      logger('[handleThroughput] setTimeout backoff %s ms, attemps: %s', backoff, attempt)
      await new Promise(resolve => setTimeout(resolve, backoff))
      const maxRetries = process.env.DDB_MAX_RETRIES || 10
      const tempt = Math.min(maxRetries, baseBackOff * Math.pow(2, attempt))
      backoff = tempt / 2 + random(0, tempt / 2, true)
      console.log('[handleThroughput] Backing off for ', backoff)
      logger('[handleThroughput] setting backoff to %s ms', backoff)
      return await handleThroughput(data, method, backoff, ++attempt)
    } else { throw e }
  }
}
