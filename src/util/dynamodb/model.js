const normalizeTimestamp = (model) => {
  // dynamoose doesn't format timestamp attributes to ISO format
  // when saving the document. It returns timestamp as it is.
  // Format to ISO to keep consistence between get and insert
  const normalizedContent = model.toJSON ? model.toJSON() : { ...model }

  return {
    ...normalizedContent,
    updatedAt: (new Date(model.updatedAt)).toISOString(),
    createdAt: (new Date(model.createdAt)).toISOString()
  }
}

export {
  normalizeTimestamp
}
