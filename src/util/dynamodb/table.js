import dynamoose from 'dynamoose-latest'

/**
 * This function is a helper for creating a dynamodb table.
 * It's important to notice that DynamoDB doesn't have a connection
 * state, so this function creates a wrapper for the configuration
 * needed to made the request
 * @param {Object} options
 * @param {String} options.tableName
 * @param {String} options.region
 * @param {String} options.schema
 * @param {Boolean} options.autoCreate
 */

export const buildTable = ({ tableName, models, options = {} }) => {
  const table = new dynamoose.Table(tableName, models, {
    create: false,
    waitForActive: false,
    ...options
  })

  return table
}
