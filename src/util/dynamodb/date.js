import dynamoose from 'dynamoose-latest'

export const getNextAndPreviousMonthNumbers = ({ month, year }) => {
  const previous = { month: (month - 1 + 12) % 12, year }
  const next = { month: (month + 1) % 12, year }
  return {
    previous: month === 0 ? { month: 11, year: year - 1 } : previous,
    next: month === 11 ? { month: 0, year: year + 1 } : next
  }
}

export const applyDateCondition = (query, { month, year }) => {
  const months = getNextAndPreviousMonthNumbers({ month, year })
  return query
    .and()
    .parenthesis(
      new dynamoose.Condition()
        .where('month')
        .eq(month)
        .and()
        .where('year')
        .eq(year)
        .or()
        .where('month')
        .eq(months.next.month)
        .and()
        .where('year')
        .eq(months.next.year)
        .or()
        .where('month')
        .eq(months.previous.month)
        .and()
        .where('year')
        .eq(months.previous.year)
        .or()
        .where('frequency')
        .exists()
    )
    .all()
}
