// STREAMS
import { unmarshall } from '@aws-sdk/util-dynamodb'

export const DYNAMO_STREAM_EVENTS = {
  INSERT: 'INSERT',
  MODIFY: 'MODIFY',
  REMOVE: 'REMOVE'
}
export const DYNAMO_STREAM_TYPE = {
  KEYS_ONLY: 'KEYS_ONLY',
  NEW_IMAGE: 'NEW_IMAGE',
  OLD_IMAGE: 'OLD_IMAGE',
  NEW_AND_OLD_IMAGE: 'NEW_AND_OLD_IMAGE'
}

export const formatStreamEvent = (event) => {
  return event.Records.map((record) => ({
    eventName: record.eventName,
    newImage: record.dynamodb.NewImage
      ? unmarshall(record.dynamodb.NewImage)
      : undefined,
    oldImage: record.dynamodb.OldImage
      ? unmarshall(record.dynamodb.OldImage)
      : undefined,
    keys: unmarshall(record.dynamodb.Keys)
  }))
}
