import { DynamoDBClient, GetItemCommand, QueryCommand } from '@aws-sdk/client-dynamodb'
import { marshall, unmarshall } from '@aws-sdk/util-dynamodb'
import ENV from '@emerald-works/utils-env'
import debug from 'debug'
import { random } from 'lodash'

const logger = debug(`${ENV.projectName}:util:dynamodb:ddb`)

/**
 * Get item from DDB
 * @param {String} tableName DDB table name
 * @param {Object} key DDB key
 * @param {String} key.pk DDB pk
 * @param {String} key.sk DDB sk
 * @param {String} region AWS region where the table is located
 * @returns {Object} Item
 */
export const getItem = async (
  tableName,
  { pk, sk },
  region = process.env.AWS_REGION
) => {
  logger(
    'getItem - tableName %s pk %s sk %s region %s',
    tableName,
    pk,
    sk,
    region
  )
  const dynamodbClient = new DynamoDBClient({ region })
  const params = {
    TableName: `${ENV.projectName}-${tableName}-${ENV.stage}`,
    Key: marshall({
      pk,
      sk: sk || undefined
    })
  }
  logger('getItem - params %j', params)
  const command = new GetItemCommand(params)
  const { Item } = await dynamodbClient.send(command)
  logger('getItem - Item.pk %s Item.sk %s', Item?.pk, Item?.sk)
  const unmarshalled = Item ? unmarshall(Item) : null
  logger('getItem - unmarshalled.pk %s unmarshalled.sk %s', unmarshalled?.pk, unmarshalled?.sk)
  return unmarshalled
}

/**
 * Query items by pk
 * @param {String} tableName DDB table name
 * @param {String} pk DDB table pk
 * @param {String} region AWS region where the table is located
 * @returns {Array} Array of items
 */
export const queryItemsByPk = async (tableName, pk, region) => {
  logger('queryItemsByPk - tableName %s pk %s region %s', tableName, pk, region)
  const items = await queryItems({
    region,
    tableName: `${ENV.projectName}-${tableName}-${ENV.stage}`,
    keyConditions: 'pk = :pk',
    expressionAttrs: { pk }
  })

  return items
}

/**
 * Query items by condition
 * @param {String} tableName DDB table name
 * @param {String} region AWS region where the table is located
 * @param {String} keyConditions DDB key conditions
 * @param {Object} expressionAttrs DDB expression attributes
 * @param {Boolean} queryAll Flag to query all items
 * @param {Object} lastKey Last key to start the query
 * @returns {Array} Array of items
 */
const queryItems = async ({
  tableName,
  region = process.env.AWS_REGION || 'eu-west-2',
  keyConditions,
  expressionAttrs,
  queryAll = false,
  lastKey,
  backoff,
  attempt
}) => {
  try {
    const dynamodbClient = new DynamoDBClient({ region })

    // marshall expression attributes
    const expressionAttributeValues = marshall(Object.keys(expressionAttrs).reduce((acc, attr) => {
      acc[`:${attr}`] = expressionAttrs[attr]
      return acc
    }, {}))

    const params = {
      TableName: tableName,
      KeyConditionExpression: keyConditions,
      ExpressionAttributeValues: expressionAttributeValues,
      ExclusiveStartKey: lastKey
    }
    logger('queryItems - params %j', params)
    const command = new QueryCommand(params)
    const { Items, LastEvaluatedKey } = await dynamodbClient.send(command)
    logger('queryItems - LastEvaluatedKey %j Items %s first item %j', LastEvaluatedKey, Items.length, Items[0])
    // unmarshall items from DDB format to standard JSON format
    const unmarshalled = Items ? Items.map(item => unmarshall(item)) : null
    logger('queryItems - unmarshalled %s first item %j', unmarshalled.length, unmarshalled[0])
    // if queryAll is true and there is a LastEvaluatedKey, query the next items
    if (queryAll && LastEvaluatedKey) {
      const nextItems = await queryItems({ region, tableName, keyConditions, expressionAttrs, queryAll, lastKey: LastEvaluatedKey })
      return [...unmarshalled, ...nextItems]
    }
    return unmarshalled
  } catch (e) {
    // if the error is ProvisionedThroughputExceededException, retry the query
    if (e.name === 'ProvisionedThroughputExceededException') {
      // wait for backoff
      const expBackOff = await exponentialBackOff(backoff, attempt)
      // retry the query
      return queryItems({
        region,
        tableName,
        keyConditions,
        expressionAttrs,
        queryAll,
        lastKey,
        backoff: expBackOff.backoff,
        attempt: expBackOff.attempt
      })
    } else throw e
  }
}

const baseBackOff = 1000

/**
 * Exponential backoff
 * @param {Number} backoff Backoff time
 * @param {Number} attempt Attempt number
 */
const exponentialBackOff = async (backoff = baseBackOff, attempt = 1) => {
  logger('[exponentialBackOff] setTimeout backoff %s ms, attemps: %s', backoff, attempt)
  console.log('[exponentialBackOff] Backing off for ' + backoff)
  await new Promise(resolve => setTimeout(resolve, backoff))
  const maxRetries = process.env.DDB_MAX_RETRIES || 30
  const tempt = Math.min(maxRetries, baseBackOff * Math.pow(2, attempt))
  backoff = tempt / 2 + random(0, tempt / 2, true)
  logger('[exponentialBackOff] setting backoff to %s ms', backoff)
  return { backoff, attempt }
}
