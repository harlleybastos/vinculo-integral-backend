export const applyFixToName = (name) =>
  name
    ?.toLowerCase()
    .replace(/(\s\w|^\w)([^\s]{2,})/g, (s) =>
      s.replace(/\s\w|^\w/g, (ss) => ss.toUpperCase())
    )
    .replace(/\s\w\s/g, (s) => s.toUpperCase())
