import { SQS } from '@aws-sdk/client-sqs'

const SQSClient = new SQS({
  region: process.env.AWS_DEFAULT_REGION
})

export const sendSQSMessage = async (QueueName, message) => {
  const { QueueUrl } = await SQSClient.getQueueUrl({ QueueName })
  return await SQSClient.sendMessage({
    QueueUrl,
    MessageBody: JSON.stringify(message)
  })
}
