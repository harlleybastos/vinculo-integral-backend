export const getOnlyNumbers = (string) => string.replace(/\D/g, '')
