import { CognitoIdentityProvider } from '@aws-sdk/client-cognito-identity-provider'
import { GetUserRoleArn } from '../util/iam'

const cognitoIdentityProvider = new CognitoIdentityProvider({
  // The key maxRetries is renamed to maxAttempts.
  // The value of maxAttempts needs to be maxRetries + 1.
  maxAttempts: 10,

  // Reference: https://www.npmjs.com/package/@smithy/util-retry
  // The key retryDelayOptions is no longer supported in v3, and can be removed.
  // @deprecated The SDK supports more flexible retry strategies in retryStrategy option.
  retryDelayOptions: { base: 300 }
})

const createUserGroup = async (
  groupName,
  userPoolId,
  roleArn,
  description = ''
) => {
  const params = {
    GroupName: groupName,
    UserPoolId: userPoolId,
    Description: description,
    Precedence: null,
    RoleArn: roleArn
  }
  const { Group } = await cognitoIdentityProvider.createGroup(params)
  return Group
}

const getUserGroup = async (groupName, userPoolId) => {
  const params = {
    GroupName: groupName,
    UserPoolId: userPoolId
  }
  const { Group } = await cognitoIdentityProvider.getGroup(params)
  return Group
}

const getOrCreateGroup = async (
  userGroupName,
  userPoolId,
  description = 'Default user group'
) => {
  let group = null
  try {
    // AWS return a 404 error if the group doesn't exists.
    group = await getUserGroup(userGroupName, userPoolId)
  } catch (err) {
    group = await createUserGroup(
      userGroupName,
      userPoolId,
      GetUserRoleArn(userGroupName),
      description
    )
  }

  return group
}

export const addUserToGroup = async ({ groupName, userPoolId, userName }) => {
  const group = await getOrCreateGroup(groupName, userPoolId)
  const params = {
    GroupName: group.GroupName,
    UserPoolId: userPoolId,
    Username: userName
  }
  const { Group } = await cognitoIdentityProvider.adminAddUserToGroup(params)
  return Group
}

export const removeUserFromGroup = async ({
  groupName,
  userPoolId,
  userName
} = {}) => {
  try {
    if (!userPoolId) throw new Error('The userPoolId is required')
    if (!userName) throw new Error('The userName is required')
    if (!groupName) throw new Error('The newGroupName is required')

    const params = {
      GroupName: groupName,
      Username: userName,
      UserPoolId: userPoolId
    }

    const result = await cognitoIdentityProvider.adminRemoveUserFromGroup(
      params
    )

    return !result
  } catch (error) {
    console.error(error)
    throw error
  }
}
