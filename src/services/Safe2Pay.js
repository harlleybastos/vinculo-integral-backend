import axios from 'axios'

export const Safe2PayServices = axios.create({
  baseURL: 'https://services.safe2pay.com.br/Recurrence/V1',
  headers: {
    'X-API-KEY': process.env.SAFE2PAY_TOKEN,
    'Content-Type': 'application/json'
  }
})
export const Safe2PayPayment = axios.create({
  baseURL: 'https://payment.safe2pay.com.br/v2',
  headers: {
    'X-API-KEY': process.env.SAFE2PAY_TOKEN,
    'Content-Type': 'application/json'
  }
})
export const Safe2PayToken = axios.create({
  baseURL: 'https://api.safe2pay.com.br/v2',
  headers: {
    'X-API-KEY': process.env.SAFE2PAY_TOKEN,
    'Content-Type': 'application/json'
  }
})
