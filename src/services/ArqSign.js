import axios from 'axios'

const ArqSign = axios.create({
  baseURL: 'https://api-rest.arqsign.com/api/v2/processo',
  headers: {
    AppKey: process.env.ARQSIGN_APPKEY,
    SubscriptionKey: process.env.ARQSIGN_SUBSCRIPTIONKEY1,
    'Content-Type': 'application/json'
  },
  timeout: 120000
})

export default ArqSign
