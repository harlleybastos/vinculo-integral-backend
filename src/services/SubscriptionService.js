import EventBridgeService from '@emerald-works/service-event-bus'
import SubscriptionService from '@emerald-works/service-subscription'
import Constants from '@emerald-works/constants'

const { broadcastMany } = EventBridgeService
const { subscriptions } = SubscriptionService

export const broadcastMessage = async ({ contextId, eventName, context, message, excludeOrigin = true }) => {
  const { eventBus: { connectionId } } = context
  const subscriptionList = await subscriptions({
    contextId,
    eventName,
    excludeOrigin: excludeOrigin ? [connectionId] : []
  })
  await broadcastMany({
    payload: message,
    subscriptionList,
    eventName,
    type: Constants.MESSAGE_TYPE_RESPONSE
  })
}
