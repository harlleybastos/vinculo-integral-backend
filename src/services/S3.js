import {
  DeleteObjectCommand,
  GetObjectCommand,
  PutObjectCommand,
  S3Client
} from '@aws-sdk/client-s3'
import { getSignedUrl } from '@aws-sdk/s3-request-presigner'
import { USER_FILES_BUCKET_NAME } from '../util/consts'

export async function generateUploadPresignedUrl ({ Key, ContentType }) {
  const command = new PutObjectCommand({
    Bucket: USER_FILES_BUCKET_NAME,
    Key,
    ContentType
  })
  const signedUrl = await getSignedUrl(new S3Client(), command, {
    expiresIn: 3600
  })
  return signedUrl
}

export async function deleteFileFromBucket ({ Key }) {
  const command = new DeleteObjectCommand({
    Bucket: USER_FILES_BUCKET_NAME,
    Key
  })
  return await new S3Client().send(command)
}

export async function generateGetPresignedUrl ({ Bucket, Key }) {
  const command = new GetObjectCommand({ Bucket, Key })
  const signedUrl = await getSignedUrl(new S3Client(), command, {
    expiresIn: 86400
  })
  return signedUrl
}
