import axios from 'axios'
import {
  AGORA_IO_API_KEY,
  AGORA_IO_API_SECRET,
  AGORA_IO_APP_CERTIFICATE,
  AGORA_IO_APP_ID,
  RECORDINGS_BUCKET_ACCESS_KEY,
  RECORDINGS_BUCKET_NAME,
  RECORDINGS_BUCKET_SECRET_ACCESS_KEY
} from '../util/consts'
import { RtcTokenBuilder, RtmTokenBuilder } from 'agora-token'
import { AttendanceModel } from '../models/attendance'

const AgoraCloudRecording = axios.create({
  baseURL: `https://api.agora.io/v1/apps/${AGORA_IO_APP_ID}/cloud_recording`,
  headers: {
    Authorization: `Basic ${Buffer.from(
      `${AGORA_IO_API_KEY}:${AGORA_IO_API_SECRET}`
    ).toString('base64')}`,
    'Content-Type': 'application/json'
  }
})

export const Acquire = async ({ channel, token }) => {
  const uid = new Date().valueOf().toString().slice(4)
  const data = {
    cname: channel,
    uid,
    clientRequest: {}
  }

  const res = await AgoraCloudRecording.post('/acquire', data)
  return { resourceId: res.data.resourceId, uid }
}

const error = ({ response }) => {
  console.error(response.data)
  return response
}

export const StartRecording = async ({
  channel,
  resourceId,
  uid,
  prefix,
  token
}) => {
  const data = {
    cname: channel,
    uid,
    clientRequest: {
      token,
      storageConfig: {
        accessKey: RECORDINGS_BUCKET_ACCESS_KEY,
        secretKey: RECORDINGS_BUCKET_SECRET_ACCESS_KEY,
        region: 0, // us-east-1
        vendor: 1, // Amazon S3
        bucket: RECORDINGS_BUCKET_NAME,
        fileNamePrefix: prefix
      },
      recordingFileConfig: {
        avFileType: ['hls', 'mp4']
      },
      recordingConfig: {
        maxIdleTime: 300,
        videoStreamType: 0, // High-quality video stream refers to high-resolution and high-bitrate video stream
        transcodingConfig: {
          width: 1280,
          height: 720,
          fps: 15,
          bitrate: 600
        }
      }
    }
  }

  const res = await AgoraCloudRecording.post(
    `/resourceid/${resourceId}/mode/mix/start`,
    data
  ).catch(error)
  return res.data
}

export const QueryRecording = async ({
  resourceId,
  sid,
  start,
  patient,
  slices = []
}) => {
  const res = await AgoraCloudRecording.get(
    `/resourceid/${resourceId}/sid/${sid}/mode/mix/query`
  ).catch(error)
  if (
    res.data?.serverResponse?.sliceStartTime &&
    slices.indexOf(res.data?.serverResponse?.sliceStartTime) < 0
  ) {
    slices.push(res.data?.serverResponse?.sliceStartTime)
    await AttendanceModel.update({ patient, start }, { slices })
  }
  return res.data
}

export const StopRecording = async ({ resourceId, sid, channel, uid }) => {
  const data = {
    cname: channel,
    uid,
    clientRequest: {
      async_stop: false
    }
  }
  const res = await AgoraCloudRecording.post(
    `/resourceid/${resourceId}/sid/${sid}/mode/mix/stop`,
    data
  ).catch(error)
  return res.data
}

export const GenerateToken = ({ channel }) => {
  const token = RtcTokenBuilder.buildTokenWithUidAndPrivilege(
    AGORA_IO_APP_ID,
    AGORA_IO_APP_CERTIFICATE,
    channel,
    0,
    604800, // 1 Week
    604800, // 1 Week
    604800, // 1 Week
    604800, // 1 Week
    604800 // 1 Week
  )
  return token
}

export const GenerateRtmToken = ({ channel, pk }) => {
  const token = RtmTokenBuilder.buildToken(
    AGORA_IO_APP_ID,
    AGORA_IO_APP_CERTIFICATE,
    pk,
    86400 // 1 Day
  )
  return token
}
