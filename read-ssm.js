const fs = require('fs')
const { SSM } = require('@aws-sdk/client-ssm')
const ssm = new SSM()
const backendPrefix = `/${process.env.TLD}/backend`
const domainPrefix = `/${process.env.TLD}/GLOBAL`
const envPrefix = `/${process.env.TLD}/${process.env.CI_COMMIT_REF_SLUG}/backend`
console.log(backendPrefix, domainPrefix, envPrefix)

const paginateParams = async (params) => {
  let lastPage
  let parameters = []
  do {
    lastPage = await ssm.getParametersByPath({
      ...params,
      NextToken: lastPage ? lastPage.NextToken : undefined
    })
    parameters = parameters.concat(lastPage.Parameters)
  } while (!lastPage || lastPage.NextToken)
  return parameters
}

const retrieveParams = async () => {
  const backendParams = await paginateParams({
    Path: backendPrefix
  })
  const domainParams = await paginateParams({
    Path: domainPrefix
  })
  const environmentParams = await paginateParams({
    Path: envPrefix
  })
  console.log(
    `Parameters found ${backendPrefix}(${backendParams.length}) ${domainPrefix}(${domainParams.length}) ${envPrefix}(${environmentParams.length})`
  )
  const map = backendParams
    .concat(domainParams.concat(environmentParams))
    .reduce((c, p) => {
      c[
        `${p.Name.replace(`${envPrefix}/`, '')
          .replace(`${domainPrefix}/`, '')
          .replace(`${backendPrefix}/`, '')}`
      ] = p.Value
      return c
    }, {})
  const keyValues = []
  const keys = []
  for (const key in map) {
    if (Object.hasOwnProperty.call(map, key)) {
      const element = map[key]
      keyValues.push(`${key}=${element}`)
      keys.push(key)
    }
  }
  console.log(`Parameters reduced: ${keyValues.length} - ${keys.join(', ')}`)
  fs.writeFileSync('.env.ssm', '\n' + keyValues.join('\n'))
  if (!keys.includes('BASE_URL')) {
    fs.appendFileSync(
      '.env.ssm',
      `\nBASE_URL=https://${process.env.CI_COMMIT_REF_SLUG}.${process.env.TLD}\n`
    )
  }
}

retrieveParams()
